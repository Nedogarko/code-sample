<?php
/* @var $this TimePlanningController */
/* @var $developers LDeveloper[] */
?>
<style>
	.project {
		width: 150px;
	}

	.developerRateAvg, .projectRate, .planRecalculatedForAvg, .planHours, .factHours, .planHoursRecalculatedForAvg, .factHoursRecalculatedForAvg {
		width: 50px;
	}

	.planHours, .workedOutUnderCounterTotal, .factHoursRecalculatedForAvgTotal, .planHoursRecalculatedForAvgTotal, .underload {
		width: 60px;
	}

	.day {
		width: 57px;
	}

	.weekSplitter {
		background-color: #8699a4 !important;
		width: 10px;
	}

	tr.total, td.total {
		background-color: #eeeeee !important;
	}

	.planLabel, .timeLabel {
		cursor: pointer;
		color: #06c;
	}

	.planSetContainer, .timeSetContainer {
		display: none;
	}

	.planSetContainer img, .timeSetContainer img {
		width: 12px;
		height: 12px;
		cursor: pointer;
	}

	.planSet, .timeSet {
		width: 35px;
		height: 9px;
	}
</style>
<?php
$planSetUrl = $this->createUrl('setPlan');
$timeSetUrl = $this->createUrl('setActuallyWorkedOutTime');
Yii::app()->clientScript->registerScript('planSet', <<<JS
$(".planLabel").click(function() {
  $(this).hide().parent().children('.planSetContainer').show().find('input.planSet').focus();
});
$(".btnPlanSet").click(function() {
    var e = $(this).parent().children('input'); 
  $.post('$planSetUrl', {
      'year':e.data('year'),
      'weekOfYear':e.data('week_of_year'),
      'projectId':e.data('project_id'),
      'projectType':e.data('project_type'),
      'developerId':e.data('developer_id'),
      'time':e.val(),
  }, function(response){
      response = JSON.parse(response);
      e.attr('title', response.message).tooltip().tooltip('open');
      if (response.success == true) {
      	location.reload();
      } else {
        e.addClass('red');
      }
  });
});
$(".timeLabel").click(function() {
  $(this).hide().parent().children('.timeSetContainer').show().find('input.timeSet').focus();
});
$(".btnTimeSet").click(function() {
    var e = $(this).parent().children('input'); 
  $.post('$timeSetUrl', {
      'date':e.data('date'),
      'developerId':e.data('developer_id'),
      'time':e.val(),
  }, function(response){
      response = JSON.parse(response);
      e.attr('title', response.message).tooltip().tooltip('open');
      if (response.success == true) {
      	location.reload();
      } else {
        e.addClass('red');
      }
  });
});
JS
);
?>
<?php Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl . '/css/table.css', ''); ?>
<span style="display:none;">
<?php $this->widget('zii.widgets.jui.CJuiDatePicker', [
    'name' => 'tmp',
]);
?>
</span>

<?php echo CHtml::beginForm($this->createUrl('index'), 'get'); ?>

<div class="row">
    <?php echo CHtml::label(Yii::t('app', 'Год') . ':', 'year'); ?>
    <?php echo CHtml::dropDownList('year', (empty($year)
        ? null
        : $year), AppHelper::prepareList(LTimePlanning::getYears(), 'id', 'id', false, true), array('submit' => '')); ?>
</div>
<div class="row">
    <?php echo CHtml::label(Yii::t('app', 'Месяц') . ':', 'month'); ?>
    <?php echo CHtml::dropDownList('month', (empty($month)
        ? null
        : $month), AppHelper::prepareList(LTimePlanning::getMonths(), 'id', 'name', false, true),
        array('submit' => '')); ?>
</div>
<div class="row">
    <?php echo CHtml::label(Yii::t('app', 'Работник') . ':', 'employee'); ?>
    <?php echo CHtml::dropDownList('employee', $employee, AppHelper::prepareList(LManager::getSubordinates() , 'id', 'shortName', true, true),
        array('submit' => '')); ?>
</div>

<?php echo CHtml::endForm(); ?>

<div class="row" style="width: 6000px;">
    <?php foreach ($developers as $developer) { ?>
		<h3><?= $developer['vars']['name'] ?></h3>
		<table class="simple-table">
			<thead>
			<tr>
                <?php foreach ($developer['vars']['monthWeeks'] as $weekNumber => $monthWeek) { ?>
                    <?php if (count(array_intersect(array_values($monthWeek['days']),
                            array_merge(array_values($developer['vars']['monthWorkingDays']),
                                array_values($developer['vars']['monthWorkedDays'])))) > 0
                    ) { ?>
                        <?php foreach ($developer['vars']['header']['begin']['a'] as $columnKey => $columnName) { ?>
							<th class="<?= $columnKey ?>">
                                <?= $columnName ?>
							</th>
                        <?php } ?>
                        <?php foreach ($monthWeek['days'] as $day) { ?>
                            <?php if (in_array($day, $developer['vars']['monthWorkedDays'])
                                || in_array($day, $developer['vars']['monthWorkingDays'])
                            ) { ?>
								<th class="day">
                                    <?= $day ?>
								</th>
                            <?php } ?>
                        <?php } ?>
                        <?php foreach ($developer['vars']['header']['end'] as $columnKey => $columnName) { ?>
							<th class="<?= $columnKey ?> total">
                                <?= $columnName ?>
							</th>
                        <?php } ?>
						<th class="weekSplitter"></th>
                    <?php } ?>
                <?php } ?>
                <?php foreach ($developer['vars']['header']['total'] as $columnKey => $columnName) { ?>
					<th class="<?= $columnKey ?>">
                        <?= $columnName ?>
					</th>
                <?php } ?>
			</tr>
			</thead>
            <?php foreach ($developer['table'] as $projectId => $projectData) {
                $explodedProjectId = explode('.', $projectId);
                $theOnlyProjectId = $explodedProjectId[0];
                $milestoneId = null;
                if (count($explodedProjectId) > 1) {
                	$milestoneId = $explodedProjectId[1];
                }
                ?>
				<tr>
                    <?php foreach ($developer['vars']['monthWeeks'] as $weekNumber => $monthWeek) { ?>
						<td>
                            <?= CHtml::link($projectData['constants']['projectContractedName'],
                                $this->createUrl('projects/update', ['id' => $theOnlyProjectId]), [
                                    'target' => '_blank',
                                    'title' => $projectData['constants']['projectFullNameWithExtraInfo']
                                ]) ?>
						</td>
						<td>
                            <?= CHtml::link(round($projectData['constants']['developerRateAvg'], 2),
                                $this->createUrl('developerSaleRate/index',
                                    ['employeeId' => $developer['vars']['developerId']]), ['target' => '_blank']) ?>
						</td>
                        <?php if (count(array_intersect(array_values($monthWeek['days']),
                                array_merge(array_values($developer['vars']['monthWorkingDays']),
                                    array_values($developer['vars']['monthWorkedDays'])))) > 0
                        ) { ?>
							<td>
                                <?= round($developer['vars']['projectWeekRate'][$projectId][$weekNumber], 2) ?>
							</td>
							<td>
                                <?= round($projectData['constants']['projectPlannedHoursRecalculatedToAvg'], 2) ?>
							</td>
							<td>
								<span class="planLabel">
									<?= round($developer['vars']['projectWeekPlannedTime'][$projectId][$weekNumber],
                                        2) ?>
								</span>
								<span class="planSetContainer">
									<input data-project_id="<?= $projectId ?>"
									       data-project_type="<?= $projectData['constants']['projectType'] ?>"
									       data-developer_id="<?= $developer['vars']['developerId'] ?>"
									       data-year="<?= $year ?>"
									       data-week_of_year="<?= $monthWeek['weekOfYear'] ?>"
									       value="<?= round($developer['vars']['projectWeekPlannedTime'][$projectId][$weekNumber],
                                               2) ?>" class="planSet">
									<img src="/img/small_update.png" title="Сохранить" class="btnPlanSet">
								</span>
							</td>
                            <?php
                            $beginOfWekProjectRate = round($projectData['days'][$monthWeek['days'][0]]['rate'], 2);
                            foreach ($monthWeek['days'] as $day) { ?>
                                <?php if (in_array($day, $developer['vars']['monthWorkedDays'])
                                    || in_array($day, $developer['vars']['monthWorkingDays'])
                                ) { ?>
									<td>
                                        <?php
                                        $params = [
                                            'uid' => $developer['vars']['developerId'],
                                            'date' => $projectData['days'][$day]['date'],
                                            'returl' => Yii::app()->request->url,
                                            'project_id' => $theOnlyProjectId,
                                        ];
                                        if ($milestoneId) {
                                        	$params['milestone_id'] = $milestoneId;
                                        }
                                        echo CHtml::link(round($projectData['days'][$day]['workedOutTime'], 2),
                                            $this->createUrl('payroll/addinterval', $params)) ?>
                                        <?php if (($dayOfWekProjectRate = round($projectData['days'][$day]['rate'], 2))
                                            != $beginOfWekProjectRate
                                        ) {
                                            echo '(', $dayOfWekProjectRate, ')';
                                        } ?>
									</td>
                                <?php } ?>
                            <?php } ?>
							<td class="total">
                                <?= round($developer['vars']['projectWeekTotalTime'][$projectId][$weekNumber], 2) ?>
							</td>
							<td class="total">
                                <?= round($developer['vars']['planHoursRecalculatedForAvg'][$projectId][$weekNumber],
                                    2) ?>
							</td>
							<td class="total">
                                <?= round($developer['vars']['factHoursRecalculatedForAvg'][$projectId][$weekNumber],
                                    2) ?>
							</td>
							<td class="weekSplitter"></td>
                        <?php } ?>
                    <?php } ?>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
            <?php } ?>
			<tr class="total">
                <?php foreach ($developer['vars']['monthWeeks'] as $weekNumber => $monthWeek) { ?>
                    <?php if (count(array_intersect(array_values($monthWeek['days']),
                            array_merge(array_values($developer['vars']['monthWorkingDays']),
                                array_values($developer['vars']['monthWorkedDays'])))) > 0
                    ) { ?>
						<td colspan="<?= count($developer['vars']['header']['begin']['a']) ?>">
							Недельная норма - <?= round($developer['vars']['effectiveWeekHours'][$weekNumber], 2) ?> ч
						</td>
                        <?php foreach ($monthWeek['days'] as $day) { ?>
                            <?php if (in_array($day, $developer['vars']['monthWorkedDays'])
                                || in_array($day, $developer['vars']['monthWorkingDays'])
                            ) { ?>
								<td class="day">
                                    <?= $developer['vars']['dayTotalTime'][$day] ?>
								</td>
                            <?php } ?>
                        <?php } ?>
						<td class=""><?= round($developer['vars']['weekTotalTime'][$weekNumber], 2) ?></td>
						<td class=""><?= round($developer['vars']['planHoursRecalculatedForAvgWeekTotal'][$weekNumber],
                                2) ?></td>
						<td class=""><?= round($developer['vars']['factHoursRecalculatedForAvgWeekTotal'][$weekNumber],
                                2) ?></td>
						<td class="weekSplitter"></td>
                    <?php } ?>
                <?php } ?>
				<td class=""></td>
				<td class=""><?= round($developer['vars']['factHoursRecalculatedForAvgTotal'], 2) ?></td>
				<td class=""><?= round($developer['vars']['planHoursRecalculatedForAvgTotal'], 2) ?></td>
				<td class=""><?= round($developer['vars']['deficiencyByExternalProjects'], 2) ?></td>
				<td class=""><?= round($developer['vars']['underload'], 2) ?></td>
			</tr>
			<tr class="total">
                <?php foreach ($developer['vars']['monthWeeks'] as $weekNumber => $monthWeek) { ?>
                    <?php if (count(array_intersect(array_values($monthWeek['days']),
                            array_merge(array_values($developer['vars']['monthWorkingDays']),
                                array_values($developer['vars']['monthWorkedDays'])))) > 0
                    ) { ?>
						<td colspan="<?= count($developer['vars']['header']['begin']['a']) ?>">
							По счетчику
						</td>
                        <?php foreach ($monthWeek['days'] as $day) { ?>
                            <?php if (in_array($day, $developer['vars']['monthWorkedDays'])
                                || in_array($day, $developer['vars']['monthWorkingDays'])
                            ) { ?>
								<td>
				                <span class="timeLabel">
									<?= $developer['vars']['dayTotalTimeUnderCounter'][$day]['value'] ?>
								</span>
									<span class="timeSetContainer">
									<input data-developer_id="<?= $developer['vars']['developerId'] ?>"
									       data-date="<?= $developer['vars']['dayTotalTimeUnderCounter'][$day]['date'] ?>"
									       value="<?= round($developer['vars']['dayTotalTimeUnderCounter'][$day]['value'],
                                               2) ?>"
									       class="timeSet">
									<img src="/img/small_update.png" title="Сохранить" class="btnTimeSet">
								</span>
								</td>
                            <?php } ?>
                        <?php } ?>
						<td class=""><?= round($developer['vars']['weekWorkedOutUnderCounterTotal'][$weekNumber],
                                2) ?></td>
						<td class=""></td>
						<td class=""></td>
						<td class="weekSplitter"></td>
                    <?php } ?>
                <?php } ?>
				<td class=""><?= round($developer['vars']['workedOutUnderCounterTotal'], 2) ?></td>
				<td class=""></td>
				<td class=""></td>
				<td class=""></td>
				<td class=""></td>
			</tr>
		</table>
    <?php } ?>
</div>
