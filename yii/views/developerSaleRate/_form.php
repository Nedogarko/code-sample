<?php
/* @var $this DeveloperSaleRateController */
/* @var $model LDeveloperSaleRate */
/* @var $form CActiveForm */
?>
<div class="form">
    <?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'ldeveloper-sale-rate-form',
        'enableAjaxValidation' => false,
    )); ?>

    <?php echo $form->errorSummary($model); ?>

	<div class="row">
        <?php echo $form->labelEx($model, 'user_id'); ?>
        <?php if ($model->isNewRecord) { ?>
            <?php echo $form->dropDownList($model, 'user_id',
                AppHelper::prepareList(LManager::getSubordinates(), 'id', function ($model) {
                    return $model->getLongName();
                }, false, true)); ?>
            <?php echo $form->error($model, 'user_id'); ?>
        <?php } else { ?>
        <?php echo $model->user->getShortName(); ?>
        <?php } ?>
	</div>

	<div class="row">
        <?php echo $form->labelEx($model, 'currency'); ?>
        <?php echo $form->dropDownList($model, 'currency',
            AppHelper::prepareList([Currency::getDefaultSystemCurrency()], 'id', 'name')); ?>
        <?php echo $form->error($model, 'currency'); ?>
	</div>

	<div class="row">
        <?php echo $form->labelEx($model, 'rate_low'); ?>
        <?php echo $form->textField($model, 'rate_low', array('size' => 10, 'maxlength' => 10)); ?>
        <?php echo $form->error($model, 'rate_low'); ?>
	</div>

	<div class="row">
        <?php echo $form->labelEx($model, 'rate_avg'); ?>
        <?php echo $form->textField($model, 'rate_avg', array('size' => 10, 'maxlength' => 10)); ?>
        <?php echo $form->error($model, 'rate_avg'); ?>
	</div>

	<div class="row">
        <?php echo $form->labelEx($model, 'rate_high'); ?>
        <?php echo $form->textField($model, 'rate_high', array('size' => 10, 'maxlength' => 10)); ?>
        <?php echo $form->error($model, 'rate_high'); ?>
	</div>

	<div class="row">
        <?php echo $form->labelEx($model, 'planned_day_time'); ?>
        <?php echo $form->textField($model, 'planned_day_time', array('size' => 10, 'maxlength' => 10)); ?>
        <?php echo $form->error($model, 'planned_day_time'); ?>
	</div>

	<div class="row">
        <?php echo $form->labelEx($model, 'date'); ?>
        <?php
        $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'model' => $model,
            'attribute' => 'date',
            'htmlOptions' => [
                'value' => AppHelper::getDateApplication($model->date),
            ]
        ));
        ?>
        <?php echo $form->error($model, 'date'); ?>
	</div>

	<div class="row buttons">
        <?php echo CHtml::submitButton($model->isNewRecord
            ? 'Создать'
            : 'Сохранить'); ?>
	</div>

    <?php $this->endWidget(); ?>

</div><!-- form -->
