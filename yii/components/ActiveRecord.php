<?php

class ActiveRecord extends CActiveRecord
{
    use DateFormattedTrait;

    /** @var static */
    public $oldRecord;

    protected function afterFind()
    {
        $this->oldRecord = clone $this;

        return parent::afterFind();
    }

    public static function tblName()
    {
        return (new static())->tableName();
    }
}
