<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Contact Us';
?>

<h1>Contact Us</h1>
<div class="reachus">
    <h3>You can reach us at</h3>
    <p><strong>KeepMore, LLC</strong></p>
    <p>P.O. Box: 2312,</p>
    <p>Daphne, AL 36526,</p>
    <p>United States </p>
</div>
<div class="map"><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3432.309235321838!2d-87.90160255!3d30.65341955!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x889a4306f557db83%3A0xd53c5db2c8712e66!2sJubilee+Mall+Shopping+Center%2C+Daphne%2C+AL+36526%2C+USA!5e0!3m2!1sen!2sin!4v1429182456375" width="626" height="255" frameborder="0" style="border:0"></iframe></div>
<div class="contact-info">
    <div class="col-1">
        <h3>Sales and Orders Questions</h3>
        <p>For sales or business development questions,<br>
            email us at: <a href="mailto:sales@keepmore.net">sales@keepmore.net</a></p>
    </div>

    <div class="col-2">
        <h3>Have a Billing or Login Question or forgot your Username?</h3>
        <p>Please send us an email to <a href="mailto:sales@keepmore.net">sales@keepmore.net</a> and we will get back to
            you promptly. </p>
    </div>

    <div class="col-1">
        <h3>Need Help using the KeepMore.net service?</h3>
        <p>Need help using KeepMore.net? Check out the comprehensive Help Viewer feature in the software by clicking on Get Help (the '?' button) on the App Homepage or top navigation bar.</p> <br>
        <p>For additional support questions on how to use KeepMore.net, we offer phone-based support for a nominal fee ($25.00 for the first hour, and $25 each hour thereafter). Just send us an email to <a href="mailto:sales@keepmore.net">sales@keepmore.net</a> and we will respond to schedule a time most convenient for you. </p>
    </div>

    <div class="col-2">
        <h3>Need "Business Coaching" or have general questions about Small Business Money Management?</h3>
        <p>If your question is more along the lines of "can I expense this", or "how do I record this item?", we have help for you. We are proud to be affiliated with Ask Guy, a leading expert in small business money management who has been helping entrepreneurs like you for over twenty-five years keep more of their net income. For a free discovery consultation with Ask Guy, please email <a href="mailto:sales@keepmore.net">sales@keepmore.net</a> . The consultation is free and the invaluable advice is custom-priced specifically to your needs.</p>
    </div>

</div>
</div>