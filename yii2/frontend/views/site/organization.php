<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'No Sweat.';
?>
<h1>No Sweat.</h1>
<div class="container-left1 live-demo1" id="contentPromoLeft">
    <div class="col-det1"><img src="/images/org_boxes.jpg" class="imageLeftWrap" alt="image" /></div>
    <div class="right-sec" id="pageContent"><p>KeepMore<sup>TM</sup> organizes your accounts, contacts, receipts, photos and other documents so that you can spend more time making money. Plus, it's backed up every day, so you won't lose that important information.</p><p>Features include:</p><h4>Opportunity Tracker</h4><p>It is your simple, online contact management tool. You can't close the sale... if you forget to make the call.</p>
    <div class="inner-bx">
        <ul>
            <li>Track leads and customers</li>
            <li>Create ToDo lists</li>
            <li>Set reminders to call prospects</li>
            <li>Contact information is always close at hand</li>
        </ul>
    </div>
     <p>No opportunity will ever fall through the cracks again.</p>
     <h4>eDocument Storage</h4>
     <p>No more wading through piles of paperwork. With eDocument Storage, you can electronically store receipts, photos, resumes, or any other important document pertinent to your business.</p>
     <p>
        <?php echo Html::a( "Want to learn more?", ['site/learnmore'],  ["title"=>"Want to learn more?", "class"=>"link_cls"]); ?>
    </div>
</div>
    <div class="container-right" id="contentPromoRight">
        <?= $this->render( 'right_banners'); ?>
    </div>
