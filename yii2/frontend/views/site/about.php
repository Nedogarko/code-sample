<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'About';
?>
    <h1>About us</h1>
<div class="container-left live-demo" id="contentPromoLeft">
        <div class="inner-bx">
            <h3>We are an Web-based accounting/bookkeeping software company designed specifically for:</h3>
            <ul>
                <li>Real Estate Agents</li>
                <li>Self Employed</li>
                <li>Churches</li>
                <li>Insurance Agents</li>
                <li>Home Based Business</li>
                <li>Non-Profit Organizations</li>
                <li>Contractors</li>
                <li>Independent Sales</li>
            </ul>
        </div>
</div>
<div class="container-right" id="contentPromoRight">
    <?= $this->render( 'right_banners'); ?>
</div>

