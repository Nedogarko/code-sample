<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Training';
?>
<h1> Training.</h1>
<div class="container-left">
    <h2 class="style2">Welcome to KeepMore.net's Free Online Training Sessions</h2>

    <p>Now you can access KeepMore.net training anytime - when it is convenient for you. To watch the online training sessions you will need Flash and please be sure your pop-up blocker is turned off. Just select the training session link below you wish to watch and the session will begin automatically. </p>

    <div class="bottom-bx1">
        <h2>Available Training Sessions</h2>
        <h3>General Small Business Topics</h3>
        <!--<ul>
        <li><a href="<?//=VIDEO_ROOT?>video/GSBT_Business_Fundamentals_Successful%20Financial_Mgmt/lib/playback.html" title="Business Fundamentals and Successful Financial Management" target=_blank>#1 - Business Fundamentals and Successful Financial Management</a></li>
        <li><a href="<?//=VIDEO_ROOT?>video/keepmore/GSBT2-Accounting_Made_Simple_and_Easy/lib/playback.html" title="'Accounting' - Made Simple and Easy" target=_blank>#2 - "Accounting" - Made Simple and Easy</a></li>
      </ul>-->
        <ul>
            <li>#1 - Business Fundamentals and Successful Financial Management</li>
            <li>#2 - "Accounting" - Made Simple and Easy</li>
        </ul>
        <h3>KeepMore.net Training</h3>
        <!--<ul>
        <li><a href="<?//=VIDEO_ROOT?>video/keepmore/KM_1-Navigating_KeepMore_net/lib/playback.html" title="Navigating KeepMore.net" target=_blank>#1 - Navigating KeepMore.net</a></li>
        <li><a href="<?//=VIDEO_ROOT?>video/keepmore/KM_2-Get_Help_with_KeepMore_net/lib/playback.html" title="How To Get Help in KeepMore.net" target=_blank>#2 - How To Get Help in KeepMore.net</a></li>
        <li><a href="<?//=VIDEO_ROOT?>video/keepmore/KM_3-Setting_up_Bank_Credit_Card_Accts_and_Manage_To_From_List/lib/playback.html" title="Setting Up Bank and Credit Card Accounts and Manage To/Froms" target=_blank>#3 - Setting Up Bank and Credit Card Accounts and Manage To/Froms</a></li>
        <li><a href="<?//=VIDEO_ROOT?>video/keepmore/KM_4-To_From_Categories_Custom_Tags/lib/playback.html" title="To/Froms, Categories, and Custom Tags" target=_blank>#4 - To/Froms, Categories, and Custom Tags</a></li>
        <li><a href="<?//=VIDEO_ROOT?>video/keepmore/KM_5-How_to_record_Income/lib/playback.html" title="How To Record Income in KeepMore.net" target=_blank>#5 - How To Record Income in KeepMore.net</a></li>
        <li><a href="<?//=VIDEO_ROOT?>video/keepmore/KM_6-How_to_record_Expenses/lib/playback.html" title="How To Record Expenses in KeepMore.net" target=_blank>#6 - How To Record Expenses in KeepMore.net</a></li>
      </ul>-->
        <ul>
            <li>#1 - Navigating KeepMore.net</li>
            <li>#2 - How To Get Help in KeepMore.net</li>
            <li>#3 - Setting Up Bank and Credit Card Accounts and Manage To/Froms</li>
            <li>#4 - To/Froms, Categories, and Custom Tags</li>
            <li>#5 - How To Record Income in KeepMore.net</li>
            <li>#6 - How To Record Expenses in KeepMore.net</li>
        </ul>
        <h3>Other Training</h3>
        <!--<ul>
        <li><a href="<?//=VIDEO_ROOT?>video/Keepmore_Barkbusters_BiWeekly/lib/playback.html" title="Using KeepMore to file your Bark Busters BiWeekly Report" target=_blank>Using KeepMore to file your Bark Busters BiWeekly Report</a></li>
        <li><a href="<?//=VIDEO_ROOT?>video/BizFundamentals_FinancialManagement/lib/playback.html" title="Business Fundamentals - Successful Financial Management" target=_blank>Business Fundamentals - Successful Financial Management</a></li>
      </ul>-->
        <ul>
            <li>Using KeepMore to file your Bark Busters BiWeekly Report</li>
            <li>Business Fundamentals - Successful Financial Management</li>
        </ul>
    </div>
    <div>
    <p>More sessions will be added when they are available. If you have a specific topic you would like to see added, please let us know by sending an email to <a href="mailto:help@keepmore.net">help@keepmore.net</a> with your suggestion.</p>
    </div>
</div>
<div class="container-right" id="contentPromoRight">
    <?= $this->render( 'right_banners'); ?>
</div>
