<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Testimonials';
?>
<h1>Testimonials</h1>

<div class="textimonials-col1">
    <div class="pic"><img src="/images/user-pic.png" alt=""></div>
    <div class="info">
        <h2>I Have To Know My Profit To Be Successful</h2>
        <p><em>"To be a successful eBay seller, I have to know my profit on each product I sell, take advantage of every deduction, and consistently manage my sources of inventory. KeepMore.net enables me to do it all!"</em></p>
        <br>
        <p class="style1"><em>Steve M. <br>- eBay PowerSeller</em></p>
    </div>
</div>

<div class="textimonials-col1">
    <div class="pic"><img src="/images/user-pic.png" alt=""></div>
    <div class="info">
        <h2>I Needed Easy Accounting</h2>
        <p><em>"I bought KeepMore because it was simple online accounting. I know I have to do it, and now I do because it is easy."</em></p>
        <br>
        <p class="style1"><em>Rick Wilson <br>- eBay PowerSeller</em></p>
    </div>
</div>

<div class="textimonials-col1">
    <div class="pic"><img src="/images/user-pic.png" alt=""></div>
    <div class="info">
        <h2>Threw Quickbooks Away</h2>
        <p><em>"I love KeepMore.net! I even threw my Quickbooks disk and the 3-lb manual away."</em></p>
        <br>
        <p class="style1"><em>Jayson Waits <br>- Prepaid Legal Services, Inc.</em></p>
    </div>
</div>

<div class="textimonials-col1">
    <div class="pic"><img src="/images/user-pic.png" alt=""></div>
    <div class="info">
        <h2>Helped Me Find More Tax Deductions</h2>
        <p><em>"I have been an avid Quicken user for the past 20 years. I am very detailed and meticulous when it comes to tax filings. However,what I liked most about your program
                <a style="cursor:pointer;" class="tooltip" title="I have been an avid Quicken user for the past 20 years. I am very detailed and meticulous when it comes to tax filings. However,what I liked most about your program when I switched was that you suggested that I input everyday before I check my e-mail, which helps me keep up with my data input. Your program also categorizes the expenses in areas that are IRS approved (smile)! There were some areas that I was not counting that were actually tax-deductible. Your sheet which details the common areas of tax deductions specifically for Pre-Paid has been invaluable.">VIEW MORE</a></em></p>
        <br>
        <p class="style1"><em>Annette Hamilton <br>- Platinum Executive Director, Pre-Paid Legal Services, Inc.</em></p>
    </div>
</div>


<div class="textimonials-col1">
    <div class="pic"><img src="/images/user-pic.png" alt=""></div>
    <div class="info">
        <h2>Got a Virus - Didn't Lose My Data</h2>
        <p><em>"Somehow I had Spyware on my computer... it wiped out a bunch of stuff, so I had to go in and reload software and I lost a lot of data. If I had been on traditional accounting software, I would have lost all my financial data.
                <a style="cursor:pointer;" class="tooltip" title="Somehow I had Spyware on my computer... it wiped out a bunch of stuff, so I had to go in and reload software and I lost a lot of data. If I had been on traditional accounting software, I would have lost all my financial data. This is a huge benefit; with your program, all I did was go back on the website and all my sales and expense history was there!">VIEW MORE</a></em></p>
        <br>
        <p class="style1"><em>Mr. P. Bretz - Denver, CO</em></p>
    </div>
</div>


<div class="textimonials-col1">
    <div class="pic"><img src="/images/user-pic.png" alt=""></div>
    <div class="info">
        <h2>Saved $2300 More on Mileage</h2>
        <p><em>"It is only August and I am on track to save $2,300 more in deductions on mileage alone compared to last year. This program saves me money every day."</em></p>
        <br>
        <p class="style1"><em>Ms. R. Gobin <br>- Sunnyvale, CA</em></p>
    </div>
</div>

<div class="textimonials-col1">
    <div class="pic"><img src="/images/user-pic.png" alt=""></div>
    <div class="info">
        <h2>Customer Support and Training is Great</h2>
        <p><em>"I have been very impressed with how user friendly the program is. However, there is something that is even more important to me - the Customer Support. Customer Support has been great to me.
                <a style="cursor:pointer;" class="tooltip" title="I have been very impressed with how user friendly the program is. However, there is something that is even more important to me - the Customer Support. Customer Support has been great to me. Whenever I have a question I just call. The Customer Support alone is definitely worth the cost of the monthly fee.">VIEW MORE</a></em></p>
        <br>
        <p class="style1"><em>Rosalia Sepulveda <br>- San Antonio, TX</em></p>
    </div>
</div>

<div class="textimonials-col1">
    <div class="pic"><img src="/images/user-pic.png" alt=""></div>
    <div class="info">
        <h2>Quick and Secure Way to Manage My Business</h2>
        <p><em>"To be a successful Farmers Insurance Agent, I need to stay 100% focused on sales. I also have a business to manage and your program lets me simply and quickly manage and record all my business expenses.
                <a style="cursor:pointer;" class="tooltip" title="To be a successful Farmers Insurance Agent, I need to stay 100% focused on sales. I also have a business to manage and your program lets me simply and quickly manage and record all my business expenses. Because I have the peace of mind that my records are organized, secure, and backed up, I can stay focused on building my business.">VIEW MORE</a></em></p>
        <br>
        <p class="style1"><em>Mr. E. Johnson <br>- Farmers Insurance Agent</em></p>
    </div>
</div>


<div class="textimonials-col1">
    <div class="pic"><img src="/images/user-pic.png" alt=""></div>
    <div class="info">
        <h2>Received $1400 More - And Paid My Accountant Less</h2>
        <p><em>"My 2004 tax refund increased $1400 with KeepMore.net. And because my tax information was automatically ready, I paid my accountant less money."</em></p>
        <br>
        <p class="style1"><em>Anonymous</em></p>
    </div>
</div>


<div class="textimonials-col1">
    <div class="pic"><img src="/images/user-pic.png" alt=""></div>
    <div class="info">
        <h2>You Can Close More Sales</h2>
        <p><em>"Opportunity Tracker automatically reminds you to make each follow-up call. Nothing falls through the cracks, and you can close more sales."</em></p>
        <br>
        <p class="style1"><em>Jimmy Parish <br>- Prepaid Legal Services, Inc</em></p>
    </div>
</div>
