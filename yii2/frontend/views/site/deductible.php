<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'No more guesswork.';
?>
<h1>No more guesswork.</h1>
<div class="container-left1 live-demo1" id="contentPromoLeft">
    <div class="col-det1"><img src="/images/deduct_suit.jpg" class="imageLeftWrap" alt="image" /></div>
    <div class="right-sec" id="pageContent">
        <p>
            You'll be surprised what is deductible. With KeepMore<sup>TM</sup>, current deduction laws are at your fingertips. Simply enter your business receipts and record your mileage using the Track Auto function.
        </p>
        <p>
            If it is a deduction you can take, it will be sorted out as you go, instead of having to figure everything out at the end of the year.
        </p>
        <br>
        <p>
            <?php echo Html::a( "Want to learn more?", ['site/learnmore'],  ["title"=>"Want to learn more?", "class"=>"link_cls"]); ?>
    </div>
</div>
<div class="container-right" id="contentPromoRight">
    <?= $this->render( 'right_banners'); ?>
</div>
