<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Data Security';
?>
<h1>Data Security</h1>
<div class="learn-more">
    <div class="tab-button">
        <?php echo Html::a( "back", ['site/learnmore'],  ["title"=>"back","class"=>"back"]); ?>
        <?php echo Html::a( "System Requirements", ['site/requirements'],  ["title"=>"System Requirements"]); ?>
        <?php echo Html::a( "Data Security", ['site/security'],  ["title"=>"Data Security","class"=>"active"]); ?>

    </div>
    <div class="aset">
        <h3>Keeping Your Data Safe & Secure</h3>
        <br />
        <p>"Security" once evoked images of gold and cash protected by armed guards and steel bars. On the Internet, information - not gold - is the precious resource. Locks and keys have given way to Public Key Infrastructure and steel bars to Firewalls. </p><br>
        <p>The Web lets you exchange data in seconds, but it also creates the risk of "spoofing", interception, and tampering. That's why solutions like privacy, encryption, digital certificates, and secure email are essential to everyone online, whether you're implementing an e-commerce site, posting confidential information on your intranet, using your credit card to shop on the Web, or using Keepmore.net  Solutions.</p><br />
        <p>When you do business on the Internet, you should have the same concerns as you do when you use a catalog to shop over the telephone.</p><br/>
        <p><strong>Impersonation:</strong> Is the business that takes receiving my order authentic?</p><br/>
        <p><strong>Eavesdropping:</strong> Could someone "listen in" to my order and steal my credit card number?</p><br/>
        <p>In the real world, you often give your credit card to cashiers or waiters, and you give out your account number over the phone when placing an order. Posting confidential information on the Internet is no more dangerous than these practices. In fact, it is often more secure to give out your account number over the Internet, because many sites work with your browser software to encode your transaction so if outside parties intercept it, they won't be able to read it.</p><br/>
        <p>We counter security threats with a technology called SSL (Secure Sockets Layer). SSL is a set of rules followed by computers connected to the Internet. These rules include encryption, which guards against eavesdropping; data integrity, which assures that your communications aren't tampered with during transmission; and authentication, which verifies that the party actually receiving your communication is who it claims to be.</p><br/>
        <p>To check a site's security status, look at the site's URL in your browser window. An "s" added to the familiar "http" (to make "https") indicates that SSL is in effect. In Netscape Navigator 3.0 and earlier, the broken key symbol in the lower-left corner of your browser window becomes solid when you are in secure mode. In Netscape Communicator 4.0 and 4.5, the padlock symbol in the corner, usually open, is closed in secure mode. In Internet Explorer 4.0, a closed padlock appears when you are in secure mode.</p><br/>
        <p>If you're about to send information to a site that's not using SSL, your browser will warn you first.</p><br/>
        <p>Before your computer transmits your information to our site, the information is encrypted - turned into code. When the information reaches its destination, it is decoded. Anyone who intercepts the information during transmission receives only gibberish. We also encrypt all information that gets transmitted back to you.</p><br/>
        <p>There are two levels of encryption: 40-bit and 128-bit. With 40-bit encryption, there are billions of possible keys to decipher the coded information, and only one of them works. Someone intercepting the information would have to find the right key - a nearly impossible task. With 128-bit encryption, there are 300 billion trillion times as many keys as with 40-bit encryption. It is virtually impossible for an unauthorized party to find the right key, even if they are equipped with the best computers.</p><br/>
        <p>Note that security features do not prevent you from viewing non-encrypted web sites or place any limitations on your use of the World Wide Web, email, or newsgroups.</p><br/>
        <p>A useful online tool, the Verisign's Browser Check (<a class="link-style" href="http://verisign.netscape.com/advisor/index.html">http://verisign.netscape.com/advisor/index.html</a>), assists you in quickly checking the security level of your browser.</p><br/>
        <p>On our side, the server resides behind a very strong firewall to protect the server from hackers.  We also only run the Apache web server on the Linux Operating System, so there isn't much to fear from worms, viruses or hackers (as opposed to a Microsoft environment).  Your data is backed up nightly in case of some sort of hardware failure or data loss.</p><br/>
        <p>Our servers are run with High Availability (HA) in mind, and therefore, redundancy is key.  We guarantee data security and 100% uptime by leveraging load balancing with a High Availability approach - that way, if one server goes down, another is ready immediately to take it's place.  The servers also incorporate RAID 5 storage technology into the mix to allow for redundancy of the data storage as well.  All servers have redundant everything - from hard disks, to network interface cards, to power supplies.  This reduces the likelihood of hardware failures, and in the unlikely event of a hardware failure, there is another piece of hardware there, onsite to take it's place.</p><br/>
        <p>Our servers are hosted at one of ViaWest's (<a class="link-style" href="http://www.viawest.net">http://www.viawest.net</a>) most advanced facilities.  ViaWest has taken every precaution to assure their customers security and availability to their systems. By using multiple incoming high speed connections, multiple power generators, intense network monitoring, and a state of the art fire prevention system, ViaWest guarantees 100% uptime.  Basically, a major battle would have to be fought out in front of the facility for their system to go down.</p><br/>
        <p>We hope we have provided you with good information and true confidence in our ability to secure and protect your data.  Thank you for your business.</p><br/>
    </div>
</div>

