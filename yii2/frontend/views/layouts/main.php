<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use frontend\assets\AppAsset;
use common\widgets\Alert;

AppAsset::register($this);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/main.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">

<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <link rel="icon" href="/images/favicon.ico">


</head>

<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <!-- Fixed navbar -->
    <div class="container-fluid" id="header">
        <div class="container">
            <div class="row-header">
                <div class="col-lg-7 col-md-4 col-sm-5 col-sx-12">
                    <div class="logo">
                        <?= Html::img('@web/images/logo.png', ['class' => 'img-responsive']) ?>
                    </div>
                </div>
                <div class="col-lg-5 col-md-8 col-sm-7 col-sx-12 ">
                    <div class="row login-form">
                        <div class="col-lg-5 col-md-4 col-sm-4 col-xs-4">
                            <input id="loginform_username" placeholder="User Name" name="user_name" tabindex="1"
                                   class="frmfield">
                        </div>
                        <div class="col-lg-5 col-md-4 col-sm-4 col-xs-4">
                            <input id="loginform_username" placeholder="******" name="user_name" tabindex="2"
                                   class="frmfield">
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-3">
                            <input type="submit" class="login-btn" value="Login" name="user_name" tabindex="3">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid" id="navigation">
        <div class="container">
            <?php
            NavBar::begin([
                'brandLabel' => Html::img('@web/images/logo.png'),
                'brandUrl' => Yii::$app->homeUrl,
                'options' => [
                    'class' => 'navbar navbar-default  '
                ],
                'containerOptions' => [
                    'id' => 'navbar',

                ],
            ]);
            $menuItems = [
                [
                    'label' => 'Home',
                    'url' => ['/site/index']
                ],
                [
                    'label' => 'About us',
                    'url' => ['/site/about']
                ],
                [
                    'label' => 'Testimonials',
                    'url' => ['/site/testimonials']
                ],
                [
                    'label' => 'Create  account',
                    'url' => ['/user/register']
                ],
                [
                    'label' => 'Live demo',
                    'url' => ['/site/demo']
                ],
                [
                    'label' => 'Training',
                    'url' => ['/site/training']
                ],
                [
                    'label' => 'Learn more',
                    'url' => ['/site/learnmore']
                ],
                [
                    'label' => 'Contact us',
                    'url' => ['/site/contact']
                ],
            ];

            echo Nav::widget([
                'options' => [
                    'class' => 'nav navbar-nav',
                    'id' => 'menuarea',

                ],

                'id' => 'test',
                'items' => $menuItems,
            ]);
            NavBar::end();

            ?>

        </div>
    </div>

    <!-- Fixed navbar -->


    <div class="<?php if ($this->context->action->id == "index") {
        echo "container-fluid";
    } else {
        echo "container container-margin-top";
    } ?>">
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>


<footer class="container-fluid" id="footer">
    <div class="container">
        <div class=" row ">
            <div class="social-1 text-center">
                <p>
                    <span><a href="https://www.facebook.com/keepmorenet" target="_blank" title="Facebook"><img
                                src="/images/facebook.jpg" alt="Facebook"></a></span>
                    <span><a href="https://www.youtube.com/channel/UClDoJM6GPsNsOpFkpdxv6Pw" target="_blank"
                             title="Youtube"><img src="/images/you-tube.jpg" alt="Youtube"></a></span>
                    <span><a href="https://twitter.com/keepmorenet/lists/keepmore-net" target="_blank"
                             title="Twitter"><img src="/images/twitter.jpg" alt=""></a></span>
                    <span><a href="https://www.instagram.com/keepmorenet" target="_blank" title="Instagram"><img
                                src="/images/in.jpg" alt=""></a></span>
                    <span><a href="https://pinterest.com/guykeepmore/keepmorenet/" target="_blank"
                             title="Pinterest"><img src="/images/pinterest.jpg" alt=""></a></span>
                </p>
            </div>
        </div>
        <div class="row">
            <div id="quick-links" class="center-block">
                <ul class="nav1" style="padding-left: 0px;">
                    <li><strong>Quick Links:</strong></li>
                    <li><a id="sm_txt" href="</?page=privacy">Policy</a></li>
                    <li><a id="sm_txt" href="/?page=license">License Agreements</a></li>
                    <li><a id="sm_txt" href="/?page=about">About Us</a></li>
                    <li><a id="sm_txt" href="/?page=contact">Contact Us</a></li>
                </ul>
            </div>
        </div>

    </div>
    <div class="bottom text-center ">
        <p>All content & design Copyright &copy; <?= date('Y'); ?> Keepmore.net, LLC. All rights reserved.</p>
    </div>

</footer>


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
