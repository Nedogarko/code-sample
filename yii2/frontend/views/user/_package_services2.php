<?php

/* @var $this yii\web\View */
/* @var $model common\models\backend\Order */
/* @var $orderService common\models\backend\OrderService */
?>
<table class="table">
    <tr>
        <th>Name</th>
        <th class="text-right">Today's Price</th>
    </tr>
    <?php foreach ($model->getMySubscriptionServices() as $orderService) { ?>
        <tr>
            <td><?= $orderService->getServiceObject()->name ?></td>
            <td><?= Yii::$app->formatter->asCurrency($orderService->getPriceWithDiscount()) ?></td>
        </tr>
    <?php } ?>
</table>
