<?php
namespace frontend\controllers;

use Yii;
use common\models\backend\Package;
use common\models\backend\Order;
use frontend\components\Controller;
use yii\web\NotFoundHttpException;

/**
 * User controller
 */
class UserController extends Controller
{
    public function actionRegister()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        $model = new Order();
        $model->subscription_id = 1;
        $model->package_id = Package::DEFAULT_PACKAGE;
        $model->setScenario($model::SCENARIO_USER_REGISTER);
        $this->performAjaxValidation($model);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $this->setSession($model);

            return $this->redirect([
                'view',
                'id' => $model->id
            ]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionView($id)
    {
        if (!($sessionData = Yii::$app->session->get('sessionData'))) {
            throw new NotFoundHttpException('Order not found');
        }
        $model = $this->findModel($id);
        $model->setScenario($model::SCENARIO_USER_REGISTER);
        $model->setAttributes($sessionData['cardData']);

        return $this->render('view', [
            'model' => $model,
        ]);
    }

    public function actionUpdate($id)
    {
        if (!($sessionData = Yii::$app->session->get('sessionData'))) {
            throw new NotFoundHttpException('Order not found');
        }
        $model = $this->findModel($id);
        $model->setScenario($model::SCENARIO_USER_REGISTER);
        $model->setAttributes($sessionData['cardData']);
        $this->performAjaxValidation($model);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $this->setSession($model);

            return $this->redirect([
                'view',
                'id' => $model->id
            ]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionConfirm($id)
    {
        if (!($sessionData = Yii::$app->session->get('sessionData'))) {
            throw new NotFoundHttpException('Order not found');
        }
        $model = $this->findModel($id);
        $model->setScenario($model::SCENARIO_USER_REGISTER);
        $model->setAttributes($sessionData['cardData']);
        if ($model->confirm()) {
            Yii::$app->session->remove('sessionData');

            return $this->redirect(Yii::$app->urlManagerApp->createUrl('site/login'));
        } else {
            throw new \Exception('Something went wrong');
        }

        return $this->redirect([
            'update',
            'id' => $model->id
        ]);
    }


    /**
     * Finds the Order model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Order the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Order::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function setSession(Order $model)
    {
        $sessionData = [
            'orderId' => $model->id,
            'cardData' => [
                'billing_cardtype' => $model->billing_cardtype,
                'billing_cardnumber' => $model->billing_cardnumber,
                'billing_cardexp_month' => $model->billing_cardexp_month,
                'billing_cardexp_year' => $model->billing_cardexp_year,
                'billing_cardcvv' => $model->billing_cardcvv,
            ]
        ];
        Yii::$app->session->set('sessionData', $sessionData);
    }
}
