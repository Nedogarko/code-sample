<?php
namespace common\widgets;

class LinkPager extends \yii\widgets\LinkPager
{
    public $nextPageLabel = false;
    public $prevPageLabel = false;
}
