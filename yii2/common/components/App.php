<?php

namespace common\components;

use Yii;
use common\models\frontend\Auto;
use common\models\frontend\AutoAnnual;
use common\models\frontend\AutoMileage;
use common\models\frontend\BankAccount;
use common\models\frontend\CustomTag;
use common\models\frontend\File;
use common\models\frontend\FileData;
use common\models\frontend\ImportedItem;
use common\models\frontend\Invoice;
use common\models\frontend\InvoiceItem;
use common\models\frontend\InvoiceLineItem;
use common\models\frontend\InvoicePayment;
use common\models\frontend\InvoiceTax;
use common\models\frontend\Opportunity;
use common\models\frontend\OpportunityAction;
use common\models\frontend\OpportunityActionItem;
use common\models\frontend\OpportunityAssoc;
use common\models\frontend\OpportunityDoc;
use common\models\frontend\OpportunityTodo;
use common\models\frontend\Preference;
use common\models\frontend\Reconcile;
use common\models\frontend\SalesTax;
use common\models\frontend\Source;
use common\models\frontend\TaxTable;
use common\models\frontend\Transaction;
use common\models\frontend\Transfer;
use common\models\frontend\Address;
use common\models\frontend\AccountantClient;
use common\models\backend\User;
use common\models\backend\Order;
use yii\db\Connection;
use yii\db\Migration;

class App extends Migration
{
    public $db = 'db';

    /**
     * @inheritdoc
     */
    protected function getDb()
    {
        return $this->db;
    }

    public function initUser(Order $order)
    {
        $address = new \common\models\backend\Address();
        $address->first_name = $order->billing_firstname;
        $address->middle_initial = strlen($order->billing_initial)
            ? substr($order->billing_initial, 0, 1)
            : null;
        $address->last_name = $order->billing_lastname;
        $address->company = $order->billing_company;
        $address->addr1 = $order->billing_address1;
        $address->addr2 = $order->billing_address2;
        $address->city = $order->billing_city;
        $address->state = $order->billing_state;
        $address->zip = $order->billing_zip;
        $address->phone = $order->billing_phone;
        $address->email = $order->billing_email;
        if (!$address->save()) {
            Yii::error(var_export($address->errors, true), 'registration');

            return false;
        }
        $user = new User();
        $user->id = $order->user_id;
        $user->username = $order->billing_email;
        $user->db_host = Yii::$app->params['db_host'];
        $user->active = User::STATUS_ACTIVE;
        $user->acct_type = User::ACCOUNT_TYPE_USER;
        $user->mailing_addr_id = $address->id;
        $user->billing_addr_id = $address->id;
        $user->setPassword($order->user_password);
        if (!$user->save()) {
            Yii::error(var_export($user->errors, true), 'registration');

            return false;
        }
        $order->user_password = null;
        $order->save();

        return $this->createDB($user);
    }

    public function createDB(User $user)
    {
        $result = false;
        ob_start();
        /** @var Connection $connection */
        $connection = Yii::$app->db;
        try {
            $cmd = 'CREATE DATABASE `' . $user->id . '` /*!40100 COLLATE \'utf8_general_ci\' */';
            if (!$connection->createCommand($cmd)
                ->execute()
            ) {
                Yii::error($cmd, 'registration');

                return false;
            }
            $connection = Yii::$app->app_db;
            $connection->dsn = str_replace('{dbname}', $user->id, $connection->dsn);
            $this->db = $connection;
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=MyISAM';
            $connection->createCommand()
                ->createTable('log', [
                    'id' => $this->bigPrimaryKey(),
                    'level' => $this->integer(),
                    'category' => $this->string(),
                    'log_time' => $this->double(),
                    'prefix' => $this->text(),
                    'message' => $this->text(),
                ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB')
                ->execute();
            $this->createIndex('idx_log_level', 'log', 'level');
            $this->createIndex('idx_log_category', 'log', 'category');
            $connection->createCommand()
                ->createTable(AccountantClient::tableName(), [
                    'client_id' => $this->string(32)
                        ->notNull()
                        ->defaultValue(''),
                    'username' => $this->string(15)
                        ->notNull()
                        ->defaultValue(''),
                    'date_added' => $this->dateTime()
                        ->notNull()
                        ->defaultValue('0000-00-00 00:00:00'),
                    'address_id' => $this->string(32)
                        ->notNull()
                        ->defaultValue(''),
                    'active' => $this->char(1)
                        ->notNull()
                        ->defaultValue(''),
                    'last_access' => $this->dateTime()
                        ->notNull()
                        ->defaultValue('0000-00-00 00:00:00'),
                    'PRIMARY KEY (`client_id`)',
                ], $tableOptions)
                ->execute();
            $connection->createCommand()
                ->createTable(Address::tableName(), [
                    'id' => $this->string(32)
                        ->notNull()
                        ->defaultValue(''),
                    'title' => $this->string(5)
                        ->null()
                        ->defaultValue(null),
                    'first_name' => $this->string(20)
                        ->notNull()
                        ->defaultValue(''),
                    'middle_initial' => $this->char(1)
                        ->notNull()
                        ->defaultValue(''),
                    'last_name' => $this->string(20)
                        ->notNull()
                        ->defaultValue(''),
                    'suffix' => $this->string(5)
                        ->null()
                        ->defaultValue(null),
                    'company' => $this->string(75)
                        ->notNull()
                        ->defaultValue(''),
                    'addr1' => $this->string(125)
                        ->notNull()
                        ->defaultValue(''),
                    'addr2' => $this->string(125)
                        ->null()
                        ->defaultValue(null),
                    'addr3' => $this->string(125)
                        ->null()
                        ->defaultValue(null),
                    'city' => $this->string(75)
                        ->notNull()
                        ->defaultValue(''),
                    'state' => $this->char(2)
                        ->notNull()
                        ->defaultValue(''),
                    'zip' => $this->string(11)
                        ->notNull()
                        ->defaultValue(''),
                    'country' => $this->char(2)
                        ->notNull()
                        ->defaultValue(''),
                    'phone' => $this->string(20)
                        ->notNull()
                        ->defaultValue(''),
                    'fax' => $this->string(20)
                        ->null()
                        ->defaultValue(null),
                    'email' => $this->string(125)
                        ->notNull()
                        ->defaultValue(''),
                    'url' => $this->string(125)
                        ->notNull()
                        ->defaultValue(''),
                    'foreign_addr' => $this->binary()
                        ->null()
                        ->defaultValue(null),
                    'PRIMARY KEY (`id`)',
                ], $tableOptions)
                ->execute();
            $connection->createCommand()
                ->createTable(Auto::tableName(), [
                    'id' => $this->string(32)
                        ->notNull()
                        ->defaultValue(''),
                    'auto_name' => $this->string(20)
                        ->notNull()
                        ->defaultValue(''),
                    'make' => $this->string(12)
                        ->notNull()
                        ->defaultValue(''),
                    'model' => $this->string(25)
                        ->notNull()
                        ->defaultValue(''),
                    'purch_date' => $this->date()
                        ->notNull()
                        ->defaultValue('0000-00-00'),
                    'cost' => 'FLOAT(8,2) NOT NULL DEFAULT "0.0"',
                    'trade_in_allowance' => 'FLOAT(8,2) NOT NULL DEFAULT "0.0"',
                    'personal_vehicle' => $this->char(1)
                        ->notNull()
                        ->defaultValue(''),
                    'service_entry_date' => $this->date()
                        ->notNull()
                        ->defaultValue('0000-00-00'),
                    'begin_odo' => 'FLOAT(8,1) NOT NULL DEFAULT "0.0"',
                    'locked' => $this->char(1)
                        ->notNull()
                        ->defaultValue('N'),
                    'active' => $this->char(1)
                        ->notNull()
                        ->defaultValue('Y'),
                    'PRIMARY KEY (`id`)',
                ], $tableOptions)
                ->execute();
            $this->createIndex('auto_name', Auto::tableName(), 'auto_name', true);
            $this->createIndex('active', Auto::tableName(), 'active', false);
            $connection->createCommand()
                ->createTable(AutoAnnual::tableName(), [
                    'auto_id' => $this->char(32)
                        ->notNull()
                        ->defaultValue(''),
                    'year' => $this->integer(4),
                    'begin_odo' => 'FLOAT(7,1) NOT NULL DEFAULT "0.0"',
                    'end_odo' => 'FLOAT(7,1) NOT NULL DEFAULT "0.0"',
                    'PRIMARY KEY (`auto_id`, `year`)'
                ], $tableOptions)
                ->execute();
            $connection->createCommand()
                ->createTable(AutoMileage::tableName(), [
                    'id' => $this->string(32)
                        ->notNull()
                        ->defaultValue(''),
                    'auto_id' => $this->string(32)
                        ->notNull()
                        ->defaultValue(''),
                    'date' => $this->date()
                        ->notNull()
                        ->defaultValue('0000-00-00'),
                    'date_entered' => $this->datetime()
                        ->notNull()
                        ->defaultValue('0000-00-00 00:00:00'),
                    'odo_start' => 'FLOAT(10,1) NOT NULL DEFAULT "0.0"',
                    'odo_end' => 'FLOAT(10,1) NOT NULL DEFAULT "0.0"',
                    'mileage' => 'FLOAT(10,1) NOT NULL DEFAULT "0.0"',
                    'note' => $this->string(255)
                        ->null()
                        ->defaultValue(null),
                    'locked' => $this->char(1)
                        ->notNull()
                        ->defaultValue('N'),
                    'PRIMARY KEY (`id`)',
                ], $tableOptions)
                ->execute();
            $this->createIndex('date', AutoMileage::tableName(), 'date', false);
            $this->createIndex('auto_id', AutoMileage::tableName(), 'auto_id', false);
            $connection->createCommand()
                ->createTable(BankAccount::tableName(), [
                    'id' => $this->string(32)
                        ->notNull()
                        ->defaultValue(''),
                    'name' => $this->string(35)
                        ->notNull()
                        ->defaultValue(''),
                    'acct_type' => $this->char(1)
                        ->notNull()
                        ->defaultValue('B'),
                    'begin_balance' => 'FLOAT(10,2) NOT NULL DEFAULT "0.0"',
                    'balance' => 'FLOAT(10,2) NOT NULL DEFAULT "0.0"',
                    'last_reconciled' => $this->datetime()
                        ->null()
                        ->defaultValue(null),
                    'active' => $this->char(1)
                        ->notNull()
                        ->defaultValue('Y'),
                    'PRIMARY KEY (`id`)',
                ], $tableOptions)
                ->execute();
            $this->createIndex('name', BankAccount::tableName(), 'name', true);
            $connection->createCommand()
                ->createTable(CustomTag::tableName(), [
                    'id' => $this->string(32)
                        ->notNull()
                        ->defaultValue(''),
                    'name' => $this->string(25)
                        ->notNull()
                        ->defaultValue(''),
                    'type' => $this->char(1)
                        ->notNull()
                        ->defaultValue(''),
                    'active' => $this->char(1)
                        ->notNull()
                        ->defaultValue('Y'),
                    'PRIMARY KEY (`id`)',
                ], $tableOptions)
                ->execute();
            $this->createIndex('name', CustomTag::tableName(), 'name,type', true);
            $this->createIndex('type', CustomTag::tableName(), 'type', false);
            $this->createIndex('active', CustomTag::tableName(), 'active', false);
            $connection->createCommand()
                ->createTable(File::tableName(), [
                    'id' => 'MEDIUMINT(8) UNSIGNED NOT NULL AUTO_INCREMENT',
                    'datatype' => $this->string(60)
                        ->notNull()
                        ->defaultValue(''),
                    'purpose' => $this->string(50)
                        ->notNull()
                        ->defaultValue(''),
                    'parent_id' => 'MEDIUMINT(8) NOT NULL DEFAULT "0"',
                    'name' => $this->string(120)
                        ->notNull()
                        ->defaultValue(''),
                    'size' => $this->bigInteger(20)
                        ->unsigned()
                        ->notNull()
                        ->defaultValue('1024'),
                    'filedate' => $this->datetime()
                        ->notNull()
                        ->defaultValue('0000-00-00 00:00:00'),
                    'PRIMARY KEY (`id`)',
                ], $tableOptions)
                ->execute();
            $this->createIndex('datatype', File::tableName(), 'datatype,purpose,name,parent_id', true);
            $connection->createCommand()
                ->createTable(FileData::tableName(), [
                    'id' => 'MEDIUMINT(8) UNSIGNED NOT NULL AUTO_INCREMENT',
                    'masterid' => 'MEDIUMINT(8) UNSIGNED NOT NULL DEFAULT "0"',
                    'filedata' => $this->binary()
                        ->notNull(),
                    'PRIMARY KEY (`id`)',
                ], $tableOptions)
                ->execute();
            $this->createIndex('master_idx', FileData::tableName(), 'masterid', false);
            $connection->createCommand()
                ->createTable(ImportedItem::tableName(), [
                    'id' => $this->string(32)
                        ->notNull()
                        ->defaultValue(''),
                    'type' => "ENUM('ITEM','ORDER','INVOICE','FEE/CREDIT','INVESTMENT','UNDEFINED') NOT NULL DEFAULT 'UNDEFINED'",
                    'subtype' => "ENUM('I','E','O') NOT NULL DEFAULT 'O'",
                    'status' => "ENUM('PENDING','IGNORED','PROCESSED') NOT NULL DEFAULT 'PENDING'",
                    'source' => "ENUM('EBAY','AUTHNET','PAYPAL','FILE','UNDEFINED') NOT NULL DEFAULT 'UNDEFINED'",
                    'code' => $this->string(50)
                        ->notNull()
                        ->defaultValue(''),
                    'date_imported' => $this->datetime()
                        ->notNull()
                        ->defaultValue('0000-00-00 00:00:00'),
                    'date_ignored' => $this->datetime()
                        ->null()
                        ->defaultValue(null),
                    'date_processed' => $this->datetime()
                        ->null()
                        ->defaultValue(null),
                    'data' => 'LONGBLOB NOT NULL',
                    'data_version' => $this->integer(3)
                        ->notNull()
                        ->defaultValue(1),
                    'confidence_level' => "ENUM('GREEN','YELLOW','RED') NULL DEFAULT NULL",
                    'acct' => $this->string(50)
                        ->notNull()
                        ->defaultValue(''),
                    'transaction_id' => $this->string(32)
                        ->null()
                        ->defaultValue(null),
                    'PRIMARY KEY (`id`)',
                ], $tableOptions)
                ->execute();
            $this->createIndex('source', ImportedItem::tableName(), 'source', false);
            $this->createIndex('type', ImportedItem::tableName(), 'type', false);
            $this->createIndex('confidence_level', ImportedItem::tableName(), 'confidence_level', false);
            $this->createIndex('code', ImportedItem::tableName(), 'code', false);
            $this->createIndex('subtype', ImportedItem::tableName(), 'subtype', false);
            $this->createIndex('transaction_id', ImportedItem::tableName(), 'transaction_id', false);
            $connection->createCommand()
                ->createTable(Invoice::tableName(), [
                    'id' => $this->string(32)
                        ->notNull()
                        ->defaultValue(''),
                    'date' => $this->datetime()
                        ->notNull()
                        ->defaultValue('0000-00-00 00:00:00'),
                    'inv_number' => $this->string(20)
                        ->notNull()
                        ->defaultValue(''),
                    'inv_date' => $this->date()
                        ->notNull()
                        ->defaultValue('0000-00-00'),
                    'due_date' => $this->date()
                        ->notNull()
                        ->defaultValue('0000-00-00'),
                    'source_id' => $this->string(32)
                        ->notNull()
                        ->defaultValue(''),
                    'po_number' => $this->string(20)
                        ->null()
                        ->defaultValue(null),
                    'terms' => $this->integer(2)
                        ->notNull()
                        ->defaultValue(0),
                    'cust_terms' => $this->string(75)
                        ->null()
                        ->defaultValue(null),
                    'bill_addr' => $this->binary()
                        ->null()
                        ->defaultValue(null),
                    'ship_addr' => $this->binary()
                        ->null()
                        ->defaultValue(null),
                    'tax_rate' => 'FLOAT(6,4) NOT NULL DEFAULT "0.0"',
                    'status' => $this->integer(1)
                        ->notNull()
                        ->defaultValue(0),
                    'notes' => $this->binary()
                        ->null()
                        ->defaultValue(null),
                    'PRIMARY KEY (`id`)',
                ], $tableOptions)
                ->execute();
            $this->createIndex('inv_number', Invoice::tableName(), 'inv_number', true);
            $this->createIndex('source_id', Invoice::tableName(), 'source_id', false);
            $connection->createCommand()
                ->createTable(InvoiceItem::tableName(), [
                    'id' => $this->string(32)
                        ->notNull()
                        ->defaultValue(''),
                    'date_added' => $this->datetime()
                        ->notNull()
                        ->defaultValue('0000-00-00 00:00:00'),
                    'name' => $this->string(75)
                        ->notNull()
                        ->defaultValue(''),
                    'price' => 'FLOAT(9,2) NOT NULL DEFAULT "0.0"',
                    'cost' => 'FLOAT(9,2) NOT NULL DEFAULT "0.0"',
                    'desc' => $this->string(255)
                        ->null()
                        ->defaultValue(null),
                    'inventory_item' => $this->char(1)
                        ->null()
                        ->defaultValue(null),
                    'taxable' => $this->char(1)
                        ->notNull()
                        ->defaultValue('N'),
                    'active' => $this->char(1)
                        ->notNull()
                        ->defaultValue('Y'),
                    'PRIMARY KEY (`id`)',
                ], $tableOptions)
                ->execute();
            $this->createIndex('name', InvoiceItem::tableName(), 'name', true);
            $connection->createCommand()
                ->createTable(InvoiceLineItem::tableName(), [
                    'id' => $this->string(32)
                        ->notNull()
                        ->defaultValue(''),
                    'invoice_id' => $this->string(32)
                        ->notNull()
                        ->defaultValue(''),
                    'income_item_id' => $this->string(32)
                        ->notNull()
                        ->defaultValue(''),
                    'position' => $this->integer(3)
                        ->notNull()
                        ->defaultValue(0),
                    'date_added' => $this->datetime()
                        ->notNull()
                        ->defaultValue('0000-00-00 00:00:00'),
                    'qty' => 'FLOAT(9,2) NOT NULL DEFAULT "0.0"',
                    'modifier' => $this->string(32)
                        ->null()
                        ->defaultValue(null),
                    'item' => $this->string(75)
                        ->notNull()
                        ->defaultValue(''),
                    'desc' => $this->string(255)
                        ->null()
                        ->defaultValue(null),
                    'tax' => $this->char(1)
                        ->notNull()
                        ->defaultValue('N'),
                    'price_each' => 'FLOAT(9,2) NOT NULL DEFAULT "0.0"',
                    'PRIMARY KEY (`id`)',
                ], $tableOptions)
                ->execute();
            $this->createIndex('invoice_id', InvoiceLineItem::tableName(), 'invoice_id', false);
            $connection->createCommand()
                ->createTable(InvoicePayment::tableName(), [
                    'id' => $this->string(32)
                        ->notNull()
                        ->defaultValue(''),
                    'invoice_id' => $this->string(32)
                        ->notNull()
                        ->defaultValue(''),
                    'date_entered' => $this->datetime()
                        ->notNull()
                        ->defaultValue('0000-00-00 00:00:00'),
                    'date' => $this->date()
                        ->notNull()
                        ->defaultValue('0000-00-00'),
                    'method' => $this->string(32)
                        ->notNull()
                        ->defaultValue(''),
                    'desc' => $this->string(255)
                        ->null()
                        ->defaultValue(null),
                    'payment_id' => $this->string(20)
                        ->null()
                        ->defaultValue(null),
                    'amount' => 'FLOAT(9,2) NOT NULL DEFAULT "0.0"',
                    'PRIMARY KEY (`id`)',
                ], $tableOptions)
                ->execute();
            $this->createIndex('invoice_id', InvoicePayment::tableName(), 'invoice_id', false);
            $this->createIndex('date', InvoicePayment::tableName(), 'date', false);
            $connection->createCommand()
                ->createTable(InvoiceTax::tableName(), [
                    'id' => $this->string(32)
                        ->notNull()
                        ->defaultValue(''),
                    'invoice_id' => $this->string(32)
                        ->notNull()
                        ->defaultValue(''),
                    'taxcode_id' => $this->string(32)
                        ->notNull()
                        ->defaultValue(''),
                    'name' => $this->string(60)
                        ->notNull()
                        ->defaultValue(''),
                    'percent' => 'FLOAT(6,4) NOT NULL DEFAULT "0.0"',
                    'PRIMARY KEY (`id`)',
                ], $tableOptions)
                ->execute();
            $this->createIndex('invoice_id', InvoiceTax::tableName(), 'invoice_id', true);
            $connection->createCommand()
                ->createTable(Opportunity::tableName(), [
                    'id' => $this->string(32)
                        ->notNull()
                        ->defaultValue(''),
                    'date' => $this->datetime()
                        ->notNull()
                        ->defaultValue('0000-00-00 00:00:00'),
                    'address' => $this->string(32)
                        ->null()
                        ->defaultValue(null),
                    'status' => $this->string(50)
                        ->null()
                        ->defaultValue(null),
                    'classification' => $this->string(50)
                        ->null()
                        ->defaultValue(null),
                    'PRIMARY KEY (`id`)',
                ], $tableOptions)
                ->execute();
            $this->createIndex('address', Opportunity::tableName(), 'address', true);
            $connection->createCommand()
                ->createTable(OpportunityAction::tableName(), [
                    'id' => $this->string(32)
                        ->notNull()
                        ->defaultValue(''),
                    'opp' => $this->string(32)
                        ->notNull()
                        ->defaultValue(''),
                    'create_date' => $this->datetime()
                        ->notNull()
                        ->defaultValue('0000-00-00 00:00:00'),
                    'priority' => $this->char(1)
                        ->notNull()
                        ->defaultValue('1'),
                    'PRIMARY KEY (`id`)',
                ], $tableOptions)
                ->execute();
            $this->createIndex('opp', OpportunityAction::tableName(), 'opp', false);
            $connection->createCommand()
                ->createTable(OpportunityActionItem::tableName(), [
                    'id' => $this->string(32)
                        ->notNull()
                        ->defaultValue(''),
                    'opp_action' => $this->string(32)
                        ->notNull()
                        ->defaultValue(''),
                    'create_date' => $this->datetime()
                        ->notNull()
                        ->defaultValue('0000-00-00 00:00:00'),
                    'action_date' => $this->datetime()
                        ->notNull()
                        ->defaultValue('0000-00-00 00:00:00'),
                    'from_task' => $this->string(32)
                        ->notNull()
                        ->defaultValue(''),
                    'task' => $this->string(32)
                        ->notNull()
                        ->defaultValue(''),
                    'data' => $this->binary()
                        ->notNull(),
                    'PRIMARY KEY (`id`)',
                ], $tableOptions)
                ->execute();
            $this->createIndex('opp_action', OpportunityActionItem::tableName(), 'opp_action', false);
            $connection->createCommand()
                ->createTable(OpportunityAssoc::tableName(), [
                    'opp' => $this->string(32)
                        ->notNull()
                        ->defaultValue(''),
                    'trx' => $this->string(32)
                        ->notNull()
                        ->defaultValue(''),
                    'trx_type' => $this->string(32)
                        ->notNull()
                        ->defaultValue(''),
                    'action' => $this->string(32)
                        ->notNull()
                        ->defaultValue(''),
                    'PRIMARY KEY (`opp`, `trx`, `trx_type`)',
                ], $tableOptions)
                ->execute();
            $connection->createCommand()
                ->createTable(OpportunityDoc::tableName(), [
                    'id' => $this->string(32)
                        ->notNull()
                        ->defaultValue(''),
                    'name' => $this->string(50)
                        ->notNull()
                        ->defaultValue(''),
                    'subject' => $this->string(255)
                        ->null()
                        ->defaultValue(null),
                    'file' => $this->string(32)
                        ->null()
                        ->defaultValue(null),
                    'email_script' => $this->binary()
                        ->null()
                        ->defaultValue(null),
                    'letter_script' => $this->binary()
                        ->null()
                        ->defaultValue(null),
                    'PRIMARY KEY (`id`)',
                ], $tableOptions)
                ->execute();
            $this->createIndex('name', OpportunityDoc::tableName(), 'name', true);
            $connection->createCommand()
                ->createTable(OpportunityTodo::tableName(), [
                    'id' => $this->string(32)
                        ->notNull()
                        ->defaultValue(''),
                    'opp' => $this->string(32)
                        ->notNull()
                        ->defaultValue(''),
                    'action' => $this->string(32)
                        ->notNull()
                        ->defaultValue(''),
                    'name' => $this->string(255)
                        ->notNull()
                        ->defaultValue(''),
                    'date' => $this->datetime()
                        ->notNull()
                        ->defaultValue('0000-00-00 00:00:00'),
                    'data' => $this->binary()
                        ->notNull(),
                    'PRIMARY KEY (`id`)',
                ], $tableOptions)
                ->execute();
            $this->createIndex('opp', OpportunityTodo::tableName(), 'opp', false);
            $connection->createCommand()
                ->createTable(Preference::tableName(), [
                    'pref_key' => $this->string(32)
                        ->notNull()
                        ->defaultValue(''),
                    'pref_value' => $this->text()
                        ->notNull(),
                    'pref_userid' => $this->string(32)
                        ->notNull()
                        ->defaultValue(''),

                ], $tableOptions)
                ->execute();
            $this->createIndex('pref_key', Preference::tableName(), 'pref_key,pref_userid', true);
            $connection->createCommand()
                ->createTable(Reconcile::tableName(), [
                    'id' => $this->string(32)
                        ->notNull()
                        ->defaultValue(''),
                    'account_id' => $this->string(32)
                        ->notNull()
                        ->defaultValue(''),
                    'date' => $this->datetime()
                        ->notNull()
                        ->defaultValue('0000-00-00 00:00:00'),
                    'ending_date' => $this->date()
                        ->notNull()
                        ->defaultValue('0000-00-00'),
                    'ending_balance' => 'FLOAT(10,2) NOT NULL DEFAULT "0.0"',
                    'name' => $this->string(255)
                        ->notNull()
                        ->defaultValue(''),
                    'cleared_items' => $this->binary()
                        ->notNull(),
                    'all_items' => $this->binary()
                        ->notNull(),
                    'complete' => $this->char(1)
                        ->notNull()
                        ->defaultValue('N'),
                    'PRIMARY KEY (`id`)',
                ], $tableOptions)
                ->execute();
            $connection->createCommand()
                ->createTable(SalesTax::tableName(), [
                    'ref_type' => $this->string(10)
                        ->notNull()
                        ->defaultValue(''),
                    'ref_id' => $this->string(32)
                        ->notNull()
                        ->defaultValue(''),
                    'tax_id' => $this->string(32)
                        ->notNull()
                        ->defaultValue(''),
                    'tax_name' => $this->string(60)
                        ->notNull()
                        ->defaultValue(''),
                    'tax_percent' => 'FLOAT(6,4) NOT NULL DEFAULT "0.0"',
                    'date' => $this->datetime()
                        ->notNull()
                        ->defaultValue('0000-00-00 00:00:00'),
                ], $tableOptions)
                ->execute();
            $this->createIndex('ref_type', SalesTax::tableName(), 'ref_type,ref_id,tax_id', true);
            $connection->createCommand()
                ->createTable(Source::tableName(), [
                    'id' => $this->string(32)
                        ->notNull()
                        ->defaultValue(''),
                    'date_entered' => $this->datetime()
                        ->notNull()
                        ->defaultValue('0000-00-00 00:00:00'),
                    'name' => $this->string(255)
                        ->notNull()
                        ->defaultValue(''),
                    'category_id' => $this->string(32)
                        ->notNull()
                        ->defaultValue(''),
                    'cust_tag_id' => $this->string(32)
                        ->notNull()
                        ->defaultValue(''),
                    'memo' => $this->string(255)
                        ->null()
                        ->defaultValue(null),
                    'address_id' => $this->string(32)
                        ->null()
                        ->defaultValue(null),
                    'billing_id' => $this->string(32)
                        ->null()
                        ->defaultValue(null),
                    'auto' => $this->char(1)
                        ->notNull()
                        ->defaultValue('N'),
                    'active' => $this->char(1)
                        ->notNull()
                        ->defaultValue('Y'),
                    'PRIMARY KEY (`id`)',
                ], $tableOptions)
                ->execute();
            $this->createIndex('name', Source::tableName(), 'name', true);
            $connection->createCommand()
                ->createTable(TaxTable::tableName(), [
                    'id' => $this->string(32)
                        ->notNull()
                        ->defaultValue(''),
                    'name' => $this->string(60)
                        ->notNull()
                        ->defaultValue(''),
                    'percent' => 'FLOAT(6,4) NOT NULL DEFAULT "0.0"',
                    'def' => $this->char(1)
                        ->notNull()
                        ->defaultValue('N'),
                    'PRIMARY KEY (`id`)',
                ], $tableOptions)
                ->execute();
            $this->createIndex('name', TaxTable::tableName(), 'name', true);
            $connection->createCommand()
                ->createTable(Transaction::tableName(), [
                    'id' => $this->string(32)
                        ->notNull()
                        ->defaultValue(''),
                    'account_id' => $this->string(32)
                        ->notNull()
                        ->defaultValue(''),
                    'type' => $this->char(1)
                        ->notNull()
                        ->defaultValue(''),
                    'date' => $this->date()
                        ->notNull()
                        ->defaultValue('0000-00-00'),
                    'date_entered' => $this->datetime()
                        ->notNull()
                        ->defaultValue('0000-00-00 00:00:00'),
                    'date_modified' => $this->datetime()
                        ->null()
                        ->defaultValue(null),
                    'source_id' => $this->string(32)
                        ->notNull()
                        ->defaultValue(''),
                    'check_number' => $this->string(10)
                        ->null()
                        ->defaultValue(null),
                    'method_id' => $this->string(32)
                        ->notNull()
                        ->defaultValue(''),
                    'category_id' => $this->string(32)
                        ->notNull()
                        ->defaultValue(''),
                    'custom_tag_id' => $this->string(32)
                        ->null()
                        ->defaultValue(null),
                    'amount' => 'FLOAT(10,2) NOT NULL DEFAULT "0.0"',
                    'split' => $this->char(1)
                        ->notNull()
                        ->defaultValue('N'),
                    'master_id' => $this->string(32)
                        ->null()
                        ->defaultValue(null),
                    'inventory' => $this->char(1)
                        ->notNull()
                        ->defaultValue('N'),
                    'auto' => $this->char(1)
                        ->notNull()
                        ->defaultValue('N'),
                    'auto_id' => $this->string(32)
                        ->null()
                        ->defaultValue(null),
                    'memo' => $this->string(255)
                        ->null()
                        ->defaultValue(null),
                    'reconciled' => $this->char(1)
                        ->notNull()
                        ->defaultValue('N'),
                    'reconciled_date' => $this->datetime()
                        ->null()
                        ->defaultValue(null),
                    'locked' => $this->char(1)
                        ->notNull()
                        ->defaultValue('N'),
                    'locked_date' => $this->datetime()
                        ->null()
                        ->defaultValue(null),
                    'approved' => $this->char(1)
                        ->notNull()
                        ->defaultValue('N'),
                    'approved_date' => $this->datetime()
                        ->null()
                        ->defaultValue(null),
                    'accountant_changed' => $this->string(32)
                        ->notNull()
                        ->defaultValue(''),
                    'ref_source' => "ENUM('EBAY','FILE','AUTHNET','PAYPAL') NULL DEFAULT NULL",
                    'ref_type' => "ENUM('ITEM','ORDER','INVOICE','FEE/CREDIT','INVESTMENT') NULL DEFAULT NULL",
                    'ref_code' => $this->string(50)
                        ->null()
                        ->defaultValue(null),
                    'PRIMARY KEY (`id`)',
                ], $tableOptions)
                ->execute();
            $this->createIndex('date', Transaction::tableName(), 'date,source_id', false);
            $this->createIndex('account_id', Transaction::tableName(), 'account_id', false);
            $this->createIndex('master_id', Transaction::tableName(), 'master_id', false);
            $this->createIndex('auto', Transaction::tableName(), 'auto', false);
            $connection->createCommand()
                ->createTable(Transfer::tableName(), [
                    'from_id' => $this->string(32)
                        ->notNull()
                        ->defaultValue(''),
                    'to_id' => $this->string(32)
                        ->notNull()
                        ->defaultValue(''),
                    'type' => $this->char(1)
                        ->notNull()
                        ->defaultValue(''),
                    'PRIMARY KEY (`from_id`, `to_id`)'
                ], $tableOptions)
                ->execute();





            // Initial Data
            $auto = new Auto();
            $auto->auto_name = 'Business Auto';
            $auto->active = Auto::ACTIVE_ACTIVE;
            $auto->save();





            $bankAccount = new BankAccount();
            $bankAccount->name = 'Business Bank Account';
            $bankAccount->acct_type = BankAccount::TYPE_BUSINESS;
            $bankAccount->begin_balance = 0;
            $bankAccount->balance = 0;
            $bankAccount->active = BankAccount::STATUS_ACTIVE;
            $bankAccount->save();

            $bankAccount = new BankAccount();
            $bankAccount->name = 'Business Credit Card Account';
            $bankAccount->acct_type = BankAccount::TYPE_CREDIT;
            $bankAccount->begin_balance = 0;
            $bankAccount->balance = 0;
            $bankAccount->active = BankAccount::STATUS_ACTIVE;
            $bankAccount->save();

            $bankAccount = new BankAccount();
            $bankAccount->name = 'Petty Cash';
            $bankAccount->acct_type = BankAccount::TYPE_BUSINESS;
            $bankAccount->begin_balance = 0;
            $bankAccount->balance = 0;
            $bankAccount->active = BankAccount::STATUS_ACTIVE;
            $bankAccount->save();

            $bankAccount = new BankAccount();
            $bankAccount->name = 'Business Liability Loan';
            $bankAccount->acct_type = BankAccount::TYPE_LOAN;
            $bankAccount->begin_balance = 0;
            $bankAccount->balance = 0;
            $bankAccount->active = BankAccount::STATUS_ACTIVE;
            $bankAccount->save();





            $customTag = new CustomTag();
            $customTag->name = 'Service sales';
            $customTag->type = CustomTag::TYPE_I;
            $customTag->active = CustomTag::STATUS_ACTIVE;
            $customTag->save();

            $customTag = new CustomTag();
            $customTag->name = 'Product sales';
            $customTag->type = CustomTag::TYPE_I;
            $customTag->active = CustomTag::STATUS_ACTIVE;
            $customTag->save();

            $customTag = new CustomTag();
            $customTag->name = 'Commissions';
            $customTag->type = CustomTag::TYPE_I;
            $customTag->active = CustomTag::STATUS_ACTIVE;
            $customTag->save();

            $customTag = new CustomTag();
            $customTag->name = 'Fees';
            $customTag->type = CustomTag::TYPE_I;
            $customTag->active = CustomTag::STATUS_ACTIVE;
            $customTag->save();

            $customTag = new CustomTag();
            $customTag->name = 'Bonuses';
            $customTag->type = CustomTag::TYPE_I;
            $customTag->active = CustomTag::STATUS_ACTIVE;
            $customTag->save();

            $customTag = new CustomTag();
            $customTag->name = 'Sales Tax Collected';
            $customTag->type = CustomTag::TYPE_I;
            $customTag->active = CustomTag::STATUS_ACTIVE;
            $customTag->save();

            $customTag = new CustomTag();
            $customTag->name = 'Employee health insurance';
            $customTag->type = CustomTag::TYPE_E;
            $customTag->active = CustomTag::STATUS_ACTIVE;
            $customTag->save();

            $customTag = new CustomTag();
            $customTag->name = 'Accounting';
            $customTag->type = CustomTag::TYPE_E;
            $customTag->active = CustomTag::STATUS_ACTIVE;
            $customTag->save();

            $customTag = new CustomTag();
            $customTag->name = 'Consulting fees';
            $customTag->type = CustomTag::TYPE_E;
            $customTag->active = CustomTag::STATUS_ACTIVE;
            $customTag->save();

            $customTag = new CustomTag();
            $customTag->name = 'Bank charges';
            $customTag->type = CustomTag::TYPE_E;
            $customTag->active = CustomTag::STATUS_ACTIVE;
            $customTag->save();

            $customTag = new CustomTag();
            $customTag->name = 'Credit card charges';
            $customTag->type = CustomTag::TYPE_E;
            $customTag->active = CustomTag::STATUS_ACTIVE;
            $customTag->save();

            $customTag = new CustomTag();
            $customTag->name = 'Internet';
            $customTag->type = CustomTag::TYPE_E;
            $customTag->active = CustomTag::STATUS_ACTIVE;
            $customTag->save();

            $customTag = new CustomTag();
            $customTag->name = 'Office supplies';
            $customTag->type = CustomTag::TYPE_E;
            $customTag->active = CustomTag::STATUS_ACTIVE;
            $customTag->save();

            $customTag = new CustomTag();
            $customTag->name = 'Telephone';
            $customTag->type = CustomTag::TYPE_E;
            $customTag->active = CustomTag::STATUS_ACTIVE;
            $customTag->save();

            $customTag = new CustomTag();
            $customTag->name = 'Contract labor';
            $customTag->type = CustomTag::TYPE_E;
            $customTag->active = CustomTag::STATUS_ACTIVE;
            $customTag->save();

            $customTag = new CustomTag();
            $customTag->name = 'Dues and subscriptions';
            $customTag->type = CustomTag::TYPE_E;
            $customTag->active = CustomTag::STATUS_ACTIVE;
            $customTag->save();

            $customTag = new CustomTag();
            $customTag->name = 'Education';
            $customTag->type = CustomTag::TYPE_E;
            $customTag->active = CustomTag::STATUS_ACTIVE;
            $customTag->save();

            $customTag = new CustomTag();
            $customTag->name = 'Meetings and seminars';
            $customTag->type = CustomTag::TYPE_E;
            $customTag->active = CustomTag::STATUS_ACTIVE;
            $customTag->save();

            $customTag = new CustomTag();
            $customTag->name = 'Postage and shipping';
            $customTag->type = CustomTag::TYPE_E;
            $customTag->active = CustomTag::STATUS_ACTIVE;
            $customTag->save();

            $customTag = new CustomTag();
            $customTag->name = 'Sales Tax Remitted';
            $customTag->type = CustomTag::TYPE_E;
            $customTag->active = CustomTag::STATUS_ACTIVE;
            $customTag->save();

            $customTag = new CustomTag();
            $customTag->name = 'Taxes - FUTA';
            $customTag->type = CustomTag::TYPE_E;
            $customTag->active = CustomTag::STATUS_ACTIVE;
            $customTag->save();

            $customTag = new CustomTag();
            $customTag->name = 'Taxes - SUTA';
            $customTag->type = CustomTag::TYPE_E;
            $customTag->active = CustomTag::STATUS_ACTIVE;
            $customTag->save();

            $customTag = new CustomTag();
            $customTag->name = 'Taxes - FICA and medicare';
            $customTag->type = CustomTag::TYPE_E;
            $customTag->active = CustomTag::STATUS_ACTIVE;
            $customTag->save();

            $customTag = new CustomTag();
            $customTag->name = 'Taxes - payroll other';
            $customTag->type = CustomTag::TYPE_E;
            $customTag->active = CustomTag::STATUS_ACTIVE;
            $customTag->save();

            $connection->createCommand()
                ->batchInsert(Preference::tableName(), [
                    'pref_key',
                    'pref_value',
                    'pref_userid'
                ], [
                    [
                        Preference::VIEW_FIRST_AUTO,
                        'init',
                        $user->id
                    ],
                    [
                        Preference::VIEW_FIRST_TRANSACTIONS,
                        'init',
                        $user->id
                    ],
                    [
                        Preference::VIEW_FIRST_BALANCE,
                        'comp',
                        $user->id
                    ],
                    [
                        Preference::VIEW_FIRST_INVOICE,
                        'init',
                        $user->id
                    ],
                    [
                        Preference::RECENT_AUTO,
                        '',
                        $user->id
                    ],
                    [
                        Preference::RECENT_ACCOUNT,
                        '',
                        $user->id
                    ],
                    [
                        Preference::AUTH_ACCOUNTANT,
                        '',
                        $user->id
                    ],
                    [
                        Preference::BUSINESS_TYPE,
                        '',
                        $user->id
                    ],
                    [
                        Preference::BUSINESS_ENTITY,
                        '',
                        $user->id
                    ],
                    [
                        Preference::EXISTING_BUSINESS,
                        '',
                        $user->id
                    ],
                    [
                        Preference::DATE_STARTED,
                        '0000-00-00',
                        $user->id
                    ],
                    [
                        Preference::INVENTORY_PREF,
                        '',
                        $user->id
                    ],
                    [
                        Preference::INVOICE_PREF,
                        '',
                        $user->id
                    ],
                    [
                        Preference::COLLECT_SALES_TAX,
                        '',
                        $user->id
                    ],
                    [
                        Preference::SALES_TAX_RATE,
                        '0.000',
                        $user->id
                    ],
                    [
                        Preference::PREPARE_1099,
                        '',
                        $user->id
                    ],
                    [
                        Preference::EMPLOYEES,
                        'N',
                        $user->id
                    ],
                    [
                        Preference::NUMBER_EMPLOYEES,
                        '',
                        $user->id
                    ],
                    [
                        Preference::TAB_OPTION,
                        'tab',
                        $user->id
                    ],
                    [
                        Preference::LOGOUT_TIME,
                        '25',
                        $user->id
                    ],
                    [
                        Preference::GRX_LOC,
                        '/assets/',
                        $user->id
                    ],
                    [
                        Preference::ACCEPT_LICENSE_AGREEMENT,
                        '',
                        $user->id
                    ],
                    [
                        Preference::CHOSE_PATH,
                        '',
                        $user->id
                    ],
                    [
                        Preference::GREETING,
                        '',
                        $user->id
                    ],
                ])
                ->execute();


            $taxTable = new TaxTable();
            $taxTable->name = 'Default';
            $taxTable->percent = 0;
            $taxTable->def = TaxTable::DEFAULT_TAX;
            $taxTable->save();

            $result = true;
        } catch (\Exception $e) {
            Yii::error($e, 'registration');
        }
        ob_end_clean();

        return $result;
    }
}
