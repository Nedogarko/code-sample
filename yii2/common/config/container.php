<?php
\Yii::$container->set('kartik\select2\Select2', [
    'hideSearch' => true,
]);

\Yii::$container->set('yii\bootstrap\ActiveForm', [
    'fieldClass' => 'common\widgets\ActiveField',
]);

\Yii::$container->set('dosamigos\datepicker\DatePicker', [
    'clientOptions' => [
        'autoclose' => true,
    ],
]);

\Yii::$container->set('yii\data\Pagination', [
    'defaultPageSize' => 10,
]);

\Yii::$container->set('yii\widgets\LinkPager', [
    'nextPageLabel' => false,
    'prevPageLabel' => false,
]);

\Yii::$container->set('common\components\GridView', [
    'layout' => "{items}\n{summary}\n<div class=\"pagination-wrapper\">{pager}</div>",
    'options' => ['class' => 'table-app'],
    'tableOptions' => ['class' => 'table table-striped'],
]);
