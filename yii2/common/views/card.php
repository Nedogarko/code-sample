<?php

use yii\web\View;
use common\models\backend\Order;

/* @var $this yii\web\View */
/* @var $form \yii\widgets\ActiveForm */

$formId = $form->id;

$js = <<<JS
    $('input[data-stripe="number"]').payment('formatCardNumber');
	$('input[data-stripe="cvc"]').payment('formatCardCVC');
	form = $("#$formId");
    function stripeResponseHandler(status, response) {
        if (response.error) {
            $('#stripe_message').text(response.error.message);
            $('#modalProcessing').modal('hide');
        } else {
            var token = response.id;
            $('#order-stripe_token').val(token);
            form.get(0).submit();
        }
    }
    form.on('afterValidate', function (event, attribute, messages, deferreds) {
        /*
        */
        validation_flag=1;
        for(var k in attribute)
        {
            if ((attribute[k]!="") && (k!="order-stripe_token"))
            {
                validation_flag=0;
            }
        }

        if (validation_flag==1) {
            $('#modalProcessing').modal('show');
            Stripe.card.createToken($(this), stripeResponseHandler);
        }
        return false;
    });
JS;
$this->registerJs($js);
$this->registerJsFile('https://js.stripe.com/v2/', ['position' => $this::POS_HEAD]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/jquery.payment.js',
    ['depends' => [\yii\web\JqueryAsset::className()]]);
$stripeKey = Yii::$app->params['stripe']['publishableKey'];
$js = <<<JS
    Stripe.setPublishableKey('$stripeKey');
JS;
$this->registerJs($js, View::POS_BEGIN);
?>

<div class="row">
    <div class="col-lg-6 col-md-6 col-sm-6 xs-12 text-right">
        <?= $form->field($model, 'billing_cardtype')
            ->dropDownList(Order::getCardTypes()) ?>
    </div>
    <div class="col-lg-6 col-md-6 col-sm-6 xs-12 text-left">
        <?= $form->field($model, 'billing_cardnumber')
            ->textInput([
                'maxlength' => true,
                'placeholder' => '**** **** **** ****',
                'data-stripe' => 'number'
            ]) ?>
        <?= $form->field($model, 'stripe_token')
            ->hiddenInput()
            ->label(false) ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-6 col-md-6 col-sm-6 xs-12 text-right">
        <?= $form->field($model, 'billing_cardexp_month', [
            'template' => "{label}{input}"
        ])
            ->dropDownList(Order::getCardMonth(), [
                'data-stripe' => 'exp_month'
            ]) ?>
        <?= $form->field($model, 'billing_cardexp_year', [
            'template' => "{input}"
        ])
            ->dropDownList(Order::getCardYear(), [
                'data-stripe' => 'exp_year'
            ]) ?>
    </div>
    <div class="col-lg-6 col-md-6 col-sm-6 xs-12 text-left">
        <?= $form->field($model, 'billing_cardcvv')
            ->passwordInput([
                'maxlength' => true,
                'placeholder' => '***',
                'data-stripe' => 'cvc'
            ]) ?>
    </div>
</div>
<div class="row text-center text-danger">
    <div id="stripe_message"></div>
</div>
