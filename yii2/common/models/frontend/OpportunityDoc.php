<?php

namespace common\models\frontend;

use Yii;

/**
 * This is the model class for table "{{%OPP_DOC}}".
 *
 * @property string $id
 * @property string $name
 * @property string $subject
 * @property string $file
 * @property resource $email_script
 * @property resource $letter_script
 */
class OpportunityDoc extends \common\components\AppActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%OPP_DOC}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'email_script',
                    'letter_script'
                ],
                'string'
            ],
            [
                [
                    'id',
                    'file'
                ],
                'string',
                'max' => 32
            ],
            [
                ['name'],
                'string',
                'max' => 50
            ],
            [
                ['subject'],
                'string',
                'max' => 255
            ],
            [
                ['name'],
                'unique'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'subject' => 'Subject',
            'file' => 'File',
            'email_script' => 'Email Script',
            'letter_script' => 'Letter Script',
        ];
    }
}
