<?php

namespace common\models\frontend;

use Yii;

/**
 * This is the model class for table "{{%OPP_ASSOC}}".
 *
 * @property string $opp
 * @property string $trx
 * @property string $trx_type
 * @property string $action
 */
class OpportunityAssoc extends \common\components\AppActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%OPP_ASSOC}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'opp',
                    'trx',
                    'trx_type'
                ],
                'required'
            ],
            [
                [
                    'opp',
                    'trx',
                    'trx_type',
                    'action'
                ],
                'string',
                'max' => 32
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'opp' => 'Opp',
            'trx' => 'Trx',
            'trx_type' => 'Trx Type',
            'action' => 'Action',
        ];
    }
}
