<?php

namespace common\models\frontend;

use Yii;
use yii\db\Expression;

/**
 * This is the model class for table "{{%INVOICE_ITEMS}}".
 *
 * @property string $id
 * @property string $date_added
 * @property string $name
 * @property double $price
 * @property double $cost
 * @property string $desc
 * @property string $inventory_item
 * @property string $taxable
 * @property string $active
 */
class InvoiceItem extends \common\components\AppActiveRecord
{

    const STATUS_ACTIVE = 'Y';
    const STATUS_INACTIVE = 'N';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%INVOICE_ITEMS}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'name',
                    'price',
                    'cost'
                ],
                'required'
            ],
            [
                ['date_added'],
                'safe'
            ],
            [
                [
                    'price',
                    'cost'
                ],
                'number'
            ],
            [
                ['id'],
                'string',
                'max' => 32
            ],
            [
                ['name'],
                'string',
                'max' => 75
            ],
            [
                ['desc'],
                'string',
                'max' => 255
            ],
            [
                [
                    'inventory_item',
                    'taxable',
                    'active'
                ],
                'string',
                'max' => 1
            ],
            [
                ['name'],
                'unique'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date_added' => 'Date Added',
            'name' => 'Name',
            'price' => 'Price',
            'cost' => 'Cost',
            'desc' => 'Description',
            'inventory_item' => 'Inventory Item',
            'taxable' => 'Taxable',
            'active' => 'Active',
        ];
    }

    public function beforeSave($insert)
    {

        if (parent::beforeSave($insert)) {
            if ($insert) {
                if ($this->hasAttribute('date_added')) {
                    $this->date_added =new Expression('NOW()');
                }
            }

            return true;
        }

        return false;
    }



    public static function getAll($showInactive = false)
    {
        $query = self::find();
        if (!$showInactive) {
            $query->where(['active' => self::STATUS_ACTIVE]);
        }

        return $query->all();
    }

    public static function getPrices()
    {
        $result = [];
        $data= self::getAll();
        foreach($data  as $key => $item ){
            $result[$item["id"]] = ['data-price' => $item["price"],'data-tax' => $item["taxable"]] ;
        }

        return $result;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoice()
    {
        return $this->hasMany(Invoice::className(), ['id' => 'invoice_id'])
            ->viaTable(InvoiceLineItem::tableName(), ['income_item_id' => 'id']);
    }
}
