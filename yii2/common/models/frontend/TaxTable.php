<?php

namespace common\models\frontend;

use Yii;

/**
 * This is the model class for table "{{%TAX_TABLE}}".
 *
 * @property string $id
 * @property string $name
 * @property double $percent
 * @property string $def
 */
class TaxTable extends \common\components\AppActiveRecord
{
    const DEFAULT_TAX = 'Y';
    const NOT_DEFAULT_TAX = 'N';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%TAX_TABLE}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                ['name', 'percent'],
                'required'
            ],
            [
                ['def'],
                'safe',
            ],
            [
                ['percent'],
                'number'
            ],
            [
                ['id'],
                'string',
                'max' => 32
            ],
            [
                ['name'],
                'string',
                'max' => 60
            ],
            [
                ['def'],
                'string',
                'max' => 1
            ],
//            [
//                ['def'],
//                'default',
//                'value' => self::NOT_DEFAULT_TAX
//            ],
//            [
//                ['def'],
//                'in',
//                'range' => [null, self::DEFAULT_TAX, self::NOT_DEFAULT_TAX,]
//            ],
            [
                ['name'],
                'unique'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'percent' => 'Percent',
            'def' => 'Default',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoiceTax()
    {
        return $this->hasOne(InvoiceTax::className(), ['taxcode_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoice()
    {
        return $this->hasOne(Invoice::className(), ['id' => 'invoice_id'])
            ->via('invoiceTax');
    }

    /**
     * @inheritdoc
     */
    /*
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $diff = array_diff_assoc($this->oldAttributes, $this->attributes);

            if ($this->isDefault() && $this->def != $this->oldAttributes['def']) {
                self::updateAll(['def' => self::NOT_DEFAULT_TAX]);
            }

            return true;
        }

        return false;
    }
    */

    /**
     * @return bool
     */
    public function beforeDelete()
    {
        if (!$this->canBeRenamedOrDeleted()) {
            return false;
        }

        return parent::beforeDelete();
    }

    public function afterDelete()
    {
        parent::afterDelete();
        if (!self::find()
            ->where(['def' => self::DEFAULT_TAX])
            ->exists()
        ) {
            self::find()->one()->setAsDefault();
        }
    }

    public static function getActiveTaxesSum()
    {
        return  self::find()->where(['def'=>self::DEFAULT_TAX])->sum('percent');
    }



    /**
     * @return bool
     */
    public function setAsDefault()
    {
        $this->def = self::DEFAULT_TAX;

        return $this->save();
    }

    /**
     * @return bool
     */
    public function isDefault()
    {
        return $this->def == self::DEFAULT_TAX;
    }

    /**
     * @return bool
     */
    public function canBeRenamedOrDeleted()
    {
        return true;
    }

    public static function getAll()
    {
        $query = self::find();
        return $query->all();
    }

}
