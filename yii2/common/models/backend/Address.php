<?php

namespace common\models\backend;

use Yii;

/**
 * This is the model class for table "{{%ADDRESS}}".
 *
 * @property string $id
 * @property string $title
 * @property string $first_name
 * @property string $middle_initial
 * @property string $last_name
 * @property string $suffix
 * @property string $company
 * @property string $addr1
 * @property string $addr2
 * @property string $addr3
 * @property string $city
 * @property string $state
 * @property string $zip
 * @property string $country
 * @property string $phone
 * @property string $fax
 * @property string $email
 * @property string $url
 * @property resource $foreign_addr
 *
 * @property User $user
 */
class Address extends \common\components\ActiveRecord
{
    const DEFAULT_COUNTRY = 'US';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%ADDRESS}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                ['foreign_addr'],
                'string'
            ],
            [
                ['id'],
                'string',
                'max' => 32
            ],
            [
                [
                    'title',
                    'suffix'
                ],
                'string',
                'max' => 5
            ],
            [
                [
                    'first_name',
                    'last_name',
                    'phone',
                    'fax'
                ],
                'string',
                'max' => 20
            ],
            [
                ['middle_initial'],
                'string',
                'max' => 1
            ],
            [
                [
                    'company',
                    'city'
                ],
                'string',
                'max' => 75
            ],
            [
                [
                    'addr1',
                    'addr2',
                    'addr3',
                    'email',
                    'url'
                ],
                'string',
                'max' => 125
            ],
            [
                [
                    'state',
                    'country'
                ],
                'string',
                'max' => 2
            ],
            [
                ['zip'],
                'string',
                'max' => 11
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'first_name' => 'First Name',
            'middle_initial' => 'Middle Initial',
            'last_name' => 'Last Name',
            'suffix' => 'Suffix',
            'company' => 'Company',
            'addr1' => 'Addr1',
            'addr2' => 'Addr2',
            'addr3' => 'Addr3',
            'city' => 'City',
            'state' => 'State',
            'zip' => 'Zip',
            'country' => 'Country',
            'phone' => 'Phone',
            'fax' => 'Fax',
            'email' => 'Email',
            'url' => 'Url',
            'foreign_addr' => 'Foreign Addr',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['mailing_addr_id' => 'id']);
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($insert) {
                $this->country = self::DEFAULT_COUNTRY;

                return true;
            } else {
            }
        }

        return true;
    }
}
