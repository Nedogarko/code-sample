<?php

namespace common\models\backend;

use Yii;
use yii\base\Exception;
use yii\db\Expression;


/**
 * This is the model class for table "{{%MESSAGES}}".
 *
 * @property integer $id
 * @property integer $thread_id
 * @property integer $new_replies
 * @property string $to_id
 * @property string $from_id
 * @property string $date_sent
 * @property string $date_read
 * @property string $date_replied
 * @property string $type
 * @property string $subject
 * @property string $message
 * @property string $location
 * @property integer $new_answer
 *
 * @property User $from
 * @property User $to
 */
class Message extends \common\components\ActiveRecord
{
    const TYPE_NEED_HELP = 'NEED_HELP';
    const TYPE_FEATURE_REQUEST = 'FEATURE_REQUEST';
    const TYPE_BUG_REPORT = 'BUG_REPORT';
    const TYPE_PRAISE = 'PRAISE';
    const TYPE_COMMUNICATE = 'COMMUNICATE';
    const TYPE_BILLING = 'BILLING';

    const SCENARIO_MESSAGE = 'message';
    const SCENARIO_ANSWER = 'answer';
    const SCENARIO_NEW_ANSWER = 'new_answer';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%MESSAGES}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'thread_id',
                    'new_replies',
                    'new_answer'
                ],
                'integer'
            ],
            [
                [
                    'date_sent',
                    'date_read',
                    'date_replied'
                ],
                'safe'
            ],
            [
                [
                    'type',
                    'message'
                ],
                'string'
            ],
            [
                [
                    'message',
                    'subject',
                    'type'
                ],
                'required'
            ],
            [
                [
                    'to_id',
                    'from_id'
                ],
                'string',
                'max' => 32
            ],
            [
                ['subject'],
                'string',
                'max' => 50
            ],
            [
                ['location'],
                'string',
                'max' => 255
            ],
        ];
    }



    public function scenarios()
    {
        return [
            self::SCENARIO_MESSAGE => ['to_id', 'type', 'subject', 'message','from_id'],
            self::SCENARIO_ANSWER => ['thread_id','message','to_id','from_id'],
            self::SCENARIO_NEW_ANSWER => ['new_answer'],
        ];
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'thread_id' => 'Thread ID',
            'new_replies' => 'New Replies',
            'to_id' => 'To ID',
            'from_id' => 'From ID',
            'date_sent' => 'Date Sent',
            'date_read' => 'Date Read',
            'date_replied' => 'Date Replied',
            'type' => 'Type',
            'subject' => 'Subject',
            'message' => 'Message',
            'location' => 'Location',
            'new_answer' => 'New Answer',
        ];
    }


    public function beforeSave($insert)
    {
        if ($this->hasAttribute('date_sent')) {
            $this->date_sent =new Expression('NOW()');
        }
        if (    count($this->getAnswers()->all())!=0
                 &&
                ($this->from_id==Yii::$app->user->identity->getId())
            ){

            $this->new_answer=1;
        }

        return true;
    }



    /**
     * @return array
     */
    public static function getTypes()
    {
        return [
            self::TYPE_NEED_HELP => 'Need Help',
            self::TYPE_FEATURE_REQUEST => 'Feature Request',
            self::TYPE_BUG_REPORT => 'Bug Report',
            self::TYPE_PRAISE => 'Praise',
            self::TYPE_COMMUNICATE => 'Communicate',
            self::TYPE_BILLING => 'Billing',
        ];
    }

    /**
     * @return mixed
     * @throws Exception
     */
    public function getType()
    {
        $types = self::getTypes();
        if (isset($this->type) && !array_key_exists($this->type, $types)) {
            throw new Exception('Unknown message type');
        }

        return $types[$this->type];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFrom()
    {
        return $this->hasOne(User::className(), ['id' => 'from_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTo()
    {
        return $this->hasOne(User::className(), ['id' => 'to_id']);
    }


    public function getCreator()
    {
        return $this->hasOne(User::className(), ['id' => 'from_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAnswers()
    {
        return $this->hasOne(Message::className(), ['thread_id' => 'id']);
    }


    public static function getToUser()
    {
        $result = [
            'help@keepmore.net' => 'Support Keepmore.net',
        ];

        return $result;
    }

}
