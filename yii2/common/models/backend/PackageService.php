<?php

namespace common\models\backend;

use Yii;

/**
 * This is the model class for table "{{%PACKAGES_SERVICES}}".
 *
 * @property integer $package_id
 * @property integer $service_id
 * @property string $cardinality
 */
class PackageService extends \common\components\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%PACKAGES_SERVICES}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'package_id',
                    'service_id'
                ],
                'integer'
            ],
            [
                ['cardinality'],
                'string'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'package_id' => 'Package ID',
            'service_id' => 'Service ID',
            'cardinality' => 'Cardinality',
        ];
    }
}
