<?php
namespace app\models;

use Yii;
use yii\base\Model;

/**
 * OpportunityLicense  form
 */
class OpportunityLicenseForm extends Model
{
    public $flow;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'flow'
                ],
                'required'
            ],
        ];
    }
}
