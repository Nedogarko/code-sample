<?php
namespace app\components\validators;

use yii\validators\Validator;

class OdometerValidator extends Validator
{
    public function init()
    {
        parent::init();
        $this->message = 'Invalid mileage entry, please correct.';
    }

    public function validateAttribute($model, $attribute)
    {
        $start = floatval($model->odo_start);
        $end = floatval($model->odo_end);
        if ($start && $end && ($end < $start)) {
            $model->addError($attribute, $this->message);
        }
    }

    public function clientValidateAttribute($model, $attribute, $view)
    {
        return <<<JS
var start = parseFloat($('#automileage-odo_start').val());
var end = parseFloat($('#automileage-odo_end').val());
if (!isNaN(start) && !isNaN(end)) {
  if (start > end) {
    messages.push('$this->message');
  } else {
    if (start>0 && end>0) {
        $('#automileage-mileage').val(end - start);
    }
  }
}
JS;
    }
}
