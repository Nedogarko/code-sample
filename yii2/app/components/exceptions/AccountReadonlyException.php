<?php

namespace app\components\exceptions;

use yii\base\Exception;

class AccountReadonlyException extends Exception
{
    /**
     * @return string the user-friendly name of this exception
     */
    public function getName()
    {
        return 'Account is in Readonly mode';
    }
}
