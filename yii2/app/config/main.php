<?php
$params = array_merge(require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'), require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php'));

return [
    'id' => 'app',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'app\controllers',
    'modules' => [
        'transaction' => [
            'class' => 'app\modules\transaction\Module',
            //'defaultRoute' => 'transaction/index',
        ],
        'auto' => [
            'class' => 'app\modules\auto\Module',
            //'defaultRoute' => 'auto',
        ],
        'profile' => [
            'class' => 'app\modules\profile\Module',
            //'defaultRoute' => 'profile',
        ],
        'communicate' => [
            'class' => 'app\modules\communicate\Module',
            //'defaultRoute' => 'communicate',
        ],
        'invoice' => [
            'class' => 'app\modules\invoice\Module',
            //'defaultRoute' => 'invoice',
        ],
        'practices' => [
            'class' => 'app\modules\practices\Module',
            //'defaultRoute' => 'practices',
        ],
        'account' => [
            'class' => 'app\modules\account\Module',
            //'defaultRoute' => 'account',
        ],
        'report' => [
            'class' => 'app\modules\report\Module',
            //'defaultRoute' => 'report',
        ],
        'service' => [
            'class' => 'app\modules\service\Module',
            //'defaultRoute' => 'service',
        ],


    ],
    'components' => [

        'formatter' => [
            'class' => 'yii\i18n\Formatter',
            'nullDisplay' => '',
        ],

        'user' => [
            'identityClass' => 'common\models\backend\User',
            'enableAutoLogin' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG
                ? 3
                : 0,
            'targets' => [
                [
                    'class' => 'app\components\DbTarget',
                    'db' => 'app_db',
                    'logVars' => [],
                    'logTable' => 'log',
                    'levels' => [
                        'error',
                        'warning'
                    ],
                    'categories' => [
                        'app*',
                    ],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                'agreements' => 'site/agreements',
                'transaction' => 'transaction/transaction/index',
                'auto' => 'auto/auto/index',
                'profile' => 'profile/company-info/index',
                'communicate' => 'communicate/communicate/index',
                'invoice' => 'invoice/invoice/index',
                'practices' => 'practices/practices/index',
                'account' => 'account/account/index',
                'report' => 'report/report/index',
                'service' => 'service/service/index',

            ],
        ],
    ],
    'params' => $params,
];
