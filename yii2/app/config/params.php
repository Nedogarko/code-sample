<?php
return [
    'adminEmail' => 'admin@example.com',
    'companyName' =>'Keepmore.net',
    'supportEmail' =>'help@keepmore.net',
    'icon-framework' => 'fa',
    'DateFormatApplication' => 'd.m.Y',
    'DateTimeFormatApplication' => 'd.m.Y H:i',
    'DateFormatInternational' => 'Y-m-d',
    'DateTimeFormatInternational' => 'Y-m-d H:i:s',
];
