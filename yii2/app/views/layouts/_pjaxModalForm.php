<?php
use yii\bootstrap\Modal;

Modal::begin([
    'id' => $modalId,
    'header' => '<h4>' . $modalHeader . '</h4>',
]);
Modal::end();

$reloadTarget = '';
if (isset($gridPjaxId)) {
    $reloadTarget = "$.pjax.reload({container:'#" . $gridPjaxId . "'});";
}

$js = <<<JS
$(document).on('ready pjax:success', function() {
    var elements = $('$selectorActivation').each(function(i) {
        var elEvents = $._data($(this).get(0), 'events');
        if (!elEvents || (elEvents && !elEvents.click)) {
            $(this).click(function(e){
                e.preventDefault();
                if ($(this).data('action') != 'update') {
                    gridViewSelectRow($('#$gridPjaxId table.table:first'));
                }
                $('#$modalId').modal('show').find('.modal-body').load($(this).data('url'));
            });
        }
    });
});

$('body').on('beforeSubmit', 'form#$formId', function () {
    var form = $(this);
    if (form.find('.has-error').length) {
        return false;
    }
    $.ajax({
        url: form.attr('action'),
        type: 'post',
        data: form.serialize(),
        success: function (response) {
            if ($(response).find('.has-error').length) {
                form.parents('.modal-body').html(response);
                return false;
            }
            $('#$modalId').modal('toggle');
            $reloadTarget
        }
    });
    return false;
});
JS;
$this->registerJs($js);
?>
