<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\helpers\Url;
use app\assets\AppAsset;
use common\widgets\Alert;
use common\widgets\ModalProcessing\ModalProcessing;
use \yii\bootstrap\Modal;

AppAsset::register($this);
$module = $this->context->module->id;
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php if ($module != 'site') { ?>
        <?php $this->registerCssFile('@web/css/colors/' . $module . '.css', [
            'depends' => [
                \yii\bootstrap\BootstrapAsset::className(),
            ]
        ]); ?>
    <?php } ?>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<div class="wrapper">
    <div class="content">
        <header class="header-page-wrapper">
            <div class="container">
                <div class="row">
                    <div class="header-page">
                        <div class="logo-page">
                            <a href="/"><?= Html::img('@web/images/logo-KM_pages.png') ?></a>
                        </div>
                        <div class="menu-page">
                            <div class="menu-item <?= $module == 'transaction'
                                ? 'active'
                                : '' ?>">
                                <a href="<?= Url::to(['/transaction']) ?>">
                                    <div class="menu-icon-wrapper">
                                        <div class="menu-icon"></div>
                                    </div>
                                    <div>
                                        <p>Enter Transaction</p>
                                    </div>
                                </a>
                            </div>
                            <div class="menu-item <?= $module == 'auto'
                                ? 'active'
                                : '' ?>">
                                <a href="<?= Url::to(['/auto']) ?>">
                                    <div class="menu-icon-wrapper">
                                        <div class="menu-icon"></div>
                                    </div>
                                    <div>
                                        <p>Track Auto</p>
                                    </div>
                                </a>
                            </div>
                            <div class="menu-item <?= $module == 'invoice'
                                ? 'active'
                                : '' ?>">
                                <a href="<?= Url::to(['/invoice']) ?>">
                                    <div class="menu-icon-wrapper">
                                        <div class="menu-icon"></div>
                                    </div>
                                    <div>
                                        <p>Track Invoices</p>
                                    </div>
                                </a>
                            </div>
                            <div class="menu-item <?= $module == 'account'
                                ? 'active'
                                : '' ?>">
                                <a href="<?= Url::to(['/account']) ?>">
                                    <div class="menu-icon-wrapper">
                                        <div class="menu-icon"></div>
                                    </div>
                                    <div>
                                        <p>Balance Accounts</p>
                                    </div>
                                </a>
                            </div>
                            <div class="menu-item <?= $module == 'report'
                                ? 'active'
                                : '' ?>">
                                <a href="<?= Url::to(['/report']) ?>">
                                    <div class="menu-icon-wrapper">
                                        <div class="menu-icon"></div>
                                    </div>
                                    <div>
                                        <p>Generate Reports</p>
                                    </div>
                                </a>
                            </div>
                            <div class="menu-item <?= $module == 'profile'
                                ? 'active'
                                : '' ?>">
                                <a href="<?= Url::to(['/profile']) ?>">
                                    <div class="menu-icon-wrapper">
                                        <div class="menu-icon"></div>
                                    </div>
                                    <div>
                                        <p>Company Profile</p>
                                    </div>
                                </a>
                            </div>
                            <div class="menu-item <?= $module == 'communicate'
                                ? 'active'
                                : '' ?>">
                                <a href="<?= Url::to(['/communicate']) ?>">
                                    <div class="menu-icon-wrapper">
                                        <div class="menu-icon"></div>
                                    </div>
                                    <div>
                                        <p>Communicate</p>
                                    </div>
                                </a>
                            </div>
                            <div class="menu-item <?= $module == 'practices'
                                ? 'active'
                                : '' ?>">
                                <a href="<?= Url::to(['/practices']) ?>">
                                    <div class="menu-icon-wrapper">
                                        <div class="menu-icon"></div>
                                    </div>
                                    <div>
                                        <p>Explore Best Practices</p>
                                    </div>
                                </a>
                            </div>
                            <div class="menu-item <?= $module == 'service'
                                ? 'active'
                                : '' ?>">
                                <a href="<?= Url::to(['/service']) ?>">
                                    <div class="menu-icon-wrapper">
                                        <div class="menu-icon"></div>
                                    </div>
                                    <div>
                                        <p>Premium Services</p>
                                    </div>
                                </a>
                            </div>
                            <div class="menu-item <?= $module == 'help'
                                ? 'active'
                                : '' ?>">
                                <a href="<?= Url::to(['/help']) ?>">
                                    <div class="menu-icon-wrapper">
                                        <div class="menu-icon"></div>
                                    </div>
                                    <div>
                                        <p>Get Help</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>

        <section>
            <div class="container">
                <div class="row">
                    <div class="content-page">
                        <?= Alert::widget() ?>
                        <?= ModalProcessing::widget(); ?>
                        <?= $content ?>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <?php
    $js = <<<JS
$.ajaxSetup({
    error : function(jqXHR, textStatus, errorThrown) {
        if (jqXHR.status == 500) {
            alert(jqXHR.responseText);
        } else {
            alert("Error: " + textStatus + ": " + errorThrown);
        }
    }
});
JS;
    $this->registerJs($js);
    ?>

    <footer>
        <div class="container">
            <div class="row">
                <p>© 2016 Keepmore.net, LLC. All Rights Reserved.</p>
            </div>
        </div>
    </footer>
</div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
