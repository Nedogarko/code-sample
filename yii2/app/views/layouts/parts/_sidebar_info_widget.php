<?php
use yii\helpers\Url;

if (count($infoWidget) > 0) { ?>
    <div class="sidebar-info-widget">
        <div class="info-inner-div col-md-12">
            <div class="info-header">
                <p><?= $infoWidget['header'] ?></p>
            </div>
            <?php if (count($infoWidget['items']) > 1) { ?>
                <div class="info-radio col-md-12">
                    <div class="row">
                        <?php $selected = true;
                        foreach ($infoWidget['items'] as $key => $value) { ?>
                            <div class="input">
                                <input type="radio" name="radio" <?= $selected
                                    ? 'checked="checked"'
                                    : '' ?> class="radio-button"
                                       id="<?= $key ?>">
                                <label for="<?= $key ?>"><?= $value['label'] ?></label>
                            </div>
                            <?php $selected = false;
                        } ?>
                    </div>
                </div>
            <?php } ?>
            <?php $selected = true; ?>
            <?php foreach ($infoWidget['items'] as $tab => $items) { ?>
                <div class="info col-md-12 items <?= $tab ?> <?= $selected
                    ? ''
                    : 'hidden' ?>">
                    <?php foreach ($items['items'] as $label => $value) { ?>
                        <div class="row">
                            <div class="col-md-6 text"><?= $label ?>:</div>
                            <div class="col-md-6 number"><?= $value ?></div>
                        </div>
                    <?php } ?>
                    <?php
                        if (isset($items['actions'])) {
                            foreach ($items['actions'] as $label => $value) { ?>
                                <div class="row">
                                    <div class="col-md-12 text-center">
                                        <a href="<?= Url::to([$value]) ?>"><?= $label ?></a>
                                    </div>
                                </div>
                            <?php }
                        }?>
                </div>
                <?php $selected = false; ?>
            <?php } ?>
        </div>
    </div>
<?php } ?>
