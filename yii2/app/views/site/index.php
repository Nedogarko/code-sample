<?php

use yii\helpers\Url;

/* @var $this yii\web\View */

$this->title = 'KeepMore';
?>
<div class="row">
    <div class="main-content first-row">
        <div class="item-wrapper">
            <a href="<?= Url::to(['/transaction']) ?>">
                <div class="item-div">
                    <div class="item-icon">
                        <div class="icon"></div>
                    </div>
                    <div>
                        <h2>Enter Transactions</h2>
                    </div>
                </div>
            </a>
        </div>
        <div class="item-wrapper">
            <a href="<?= Url::to(['/auto']) ?>">
                <div class="item-div">
                    <div class="item-icon">
                        <div class="icon"></div>
                    </div>
                    <div>
                        <h2>Track Auto</h2>
                    </div>
                </div>
            </a>
        </div>
        <div class="item-wrapper">
            <a href="<?= Url::to(['/invoice']) ?>">
                <div class="item-div">
                    <div class="item-icon">
                        <div class="icon"></div>
                    </div>
                    <div>
                        <h2>Track Invoices</h2>
                    </div>
                </div>
            </a>
        </div>
        <div class="item-wrapper">
            <a href="<?= Url::to(['/report']) ?>">
                <div class="item-div">
                    <div class="item-icon">
                        <div class="icon"></div>
                    </div>
                    <div>
                        <h2>Generate Reports</h2>
                    </div>
                </div>
            </a>
        </div>
        <div class="item-wrapper">
            <a href="<?= Url::to(['/account']) ?>">
                <div class="item-div">
                    <div class="item-icon">
                        <div class="icon"></div>
                    </div>
                    <div>
                        <h2>Balance Accounts</h2>
                    </div>
                </div>
            </a>
        </div>
    </div>
</div>

<div class="row">
    <div class="main-content second-row">
        <div class="item-wrapper">
            <a href="<?= Url::to(['/profile']) ?>">
                <div class="item-div">
                    <div class="item-icon">
                        <div class="icon"></div>
                    </div>
                    <div>
                        <h2>Company Profile</h2>
                    </div>
                </div>
            </a>
        </div>
        <div class="item-wrapper">
            <a href="<?= Url::to(['/communicate']) ?>">
                <div class="item-div">
                    <div class="item-icon">
                        <div class="icon"></div>
                    </div>
                    <div>
                        <h2>Communicate</h2>
                    </div>
                </div>
            </a>
        </div>
        <div class="item-wrapper">
            <a href="<?= Url::to(['/practices']) ?>">
                <div class="item-div">
                    <div class="item-icon">
                        <div class="icon"></div>
                    </div>
                    <div>
                        <h2>Explore Best Practices</h2>
                    </div>
                </div>
            </a>
        </div>
        <div class="item-wrapper">
            <a href="<?= Url::to(['/service']) ?>">
                <div class="item-div">
                    <div class="item-icon">
                        <div class="icon"></div>
                    </div>
                    <div>
                        <h2>Premium Services</h2>
                    </div>
                </div>
            </a>
        </div>
        <div class="item-wrapper">
            <a href="<?= Url::to(['/help']) ?>">
                <div class="item-div">
                    <div class="item-icon">
                        <div class="icon"></div>
                    </div>
                    <div>
                        <h2>Get Help</h2>
                    </div>
                </div>
            </a>
        </div>
    </div>
</div>
