<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model \common\models\backend\Order */

$this->title = 'Payment Card Add';
?>
<div>
    <?php $form = ActiveForm::begin(['id' => 'form']); ?>
    <?= $this->render('@common/views/card.php', [
        'form' => $form,
        'model' => $model
    ]); ?>
    <div class="form-group">
        <?= Html::submitButton('Add', [
            'class' => 'btn btn-primary',
        ]) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
