<?php
    use yii\helpers\Url;
?>
<p>
    <b> Are prospects falling through the cracks? Did you remember to follow up and call them 3 days later?<br>
        Are you using the tried &amp; proven sales techniques of several hundred years of sales experience?<br>
        Do you want an automatic reminder about your ToDos on the day you want to work on them?
    </b>
</p>
<p>
    <a href="/images/full_ot_large.jpg" target="ot" border=0><img src="/images/full_ot.jpg" align=right border=0 hspace=10 alt="Enlarge Image"></a>
    <b>The Problem</b> has been in our very hectic work day, sales people have not had time to do it all.
</p>
<p>
    As a result, they did not record expenses, and 99% of them overpaid the IRS.
</p>
<p>
    <b>The simple solution: Opportunity Tracker</b>
    <br>Opportunity Tracker provides a very efficient method for <?=Yii::$app->params['companyName']?> subscribers to follow-up with their prospects and customers,
    and maximize business opportunities. Opportunity Tracker is designed to make you much more efficient, and therefore give you significantly more selling time.
    When you use your automobile to visit a prospect, or incur business transactions during a visit with your customers, that individual will be listed as an Opportunity.
    Opportunities are then linked to the specific miles driven or specific business transactions, and that is where Opportunity Tracker takes over.
    Click on the image to the right to see just one of many ways OT helps you sell more.
</p>
<p>
    Once turned on, Opportunity Tracker is embedded within <?=Yii::$app->params['companyName']?>, and you also have a dedicated Opportunity Tracker module within Premium Services.
</p>
<p>
    Activate Opportunity Tracker today and start maximizing your sales efforts!
</p>
<p>Want it? <a href="<?= Url::to([$this->context->getHomeUrl()]) ?>">Click here to Activate.</a>
