<?php
    use yii\helpers\ArrayHelper;
    use yii\widgets\ActiveForm;
    use kartik\select2\Select2;
    use common\models\backend\OpportunityFlow;
    use yii\helpers\Html;
    use yii\bootstrap\Modal;
    use yii\helpers\Url;
?>
<div class="row">
    <div class="col-md-10">
        <p>
            Welcome to SageFire's Opportunity Tracker!  We are excited to get you started.
        </p>
        <p>
            Opportunity Tracker provides a very efficient method for <?=Yii::$app->params['companyName']?> subscribers to follow-up with their prospects and customers,
            and maximize their business opportunities.  Opportunity Tracker is designed to make you much more efficient, and therefore give you significantly more selling time.
            When you use your automobile to visit a prospect, or incur business transactions during a visit with your customers, that individual will be listed as an Opportunity.
            Opportunities are then linked to the specific miles driven or specific business transactions, and that is where Opportunity Tracker takes over.
        </p>
        <p>
            Once turned on, Opportunity Tracker is embedded within your <?=Yii::$app->params['companyName']?> account, and a centralized Opportunity Tracker module within Premium Services.
        </p>
    </div>
    <div class="col-md-2">
        <img src="/images/dollarguy.jpg" height="300px" align="right">
    </div>
</div>
<p>
    <b>To get started</b>, please select the Sales Process Flow you wish to install:

    <?php
        $form = ActiveForm::begin([
            'action' => Url::to(['tracker-training'])
        ]);
    ?>
    <div class="row">
        <div class="col-md-6">
        <?php
            echo $form->field($model, 'flow')
                ->label(false)
                ->widget(Select2::classname(), [
                    'data' => ArrayHelper::map(OpportunityFlow::getAll(), 'name', 'description'),
                    'options' => ['placeholder' => 'Select the Flow'],
                ]);
        ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            Next, please read and agree to the KeepMore.net License Agreement.
            <?php
            Modal::begin([
                'header' => '<h2>'.Yii::$app->params['companyName'].' License Agreement</h2>',
                'toggleButton' => [
                                    'label' => 'Read it here',
                                    'class' => 'btn btn-primary'
                                   ],
                'options' =>['class' => 'license-text']
            ]);

            echo Yii::$app->controller->renderPartial('_license');

            Modal::end();
            ?>

        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            By clicking "I Agree", you agree to all terms and conditions.
        </div>
    </div>
    <?php
        echo Html::submitButton('I Agree', [
            'class' => 'btn btn-success'
        ]);
        $form::end();
    ?>
    <div class="row">
        <div class="col-md-12">
            <br>
            <a href="<?= Url::to([$this->context->getHomeUrl()]) ?>">No Thanks. Take me to Premium Services.</a>
        </div>
    </div>