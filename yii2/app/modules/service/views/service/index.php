<?php
    use yii\helpers\Url;
    use yii\helpers\Html;
?>
<h2>Opportunity Tracker</h2>
<div class="row">
    <div class="col-md-1">
        <a href="<?= Url::to([$this->context->getHomeUrl().'/tracker']) ?>"><img alt='Opportunity Tracker' src='/images/opp.gif' border=0 align=left></a>
    </div>
    <?php
    if (!$opportunityEnabled) {
    ?>
    <div class="col-md-11">
        Don't miss out on the impact of Opportunity Tracker on your business. Rapidly track your contacts, send thank you emails, and get reminders to make your follow-ups.
        <a href="<?= Url::to([$this->context->getHomeUrl().'/opportunity-marketing']) ?>">Learn More</a>
        <p>
            I want the Opportunity Tracker!<img src="/images/c.gif" width=10 height=0><a href="<?= Url::to(['service/tracker-license']) ?>">
            <img src="/images/activate.gif" border="0" align="top"></a>
        </p>
    </div>
    <?php }
    else  {
     ?>
        <div class="col-md-11">
            <i>Your Opportunity Tracker is active!</i> &nbsp;&nbsp; <a href="<?= Url::to([$this->context->getHomeUrl().'/opportunity']) ?>">Take me there</a><br>
            <?php
                echo Html::beginForm(['tracker-disable']) . Html::submitButton('Turn it off!', [
                        'class' => 'btn btn-success',
                        'data-confirm' => 'Turning off your Opportunity Tracker will result in the loss of your current contacts and past history.
                                           Click "OK" if you want to continue to turn it off this service.'
                    ]) . Html::endForm();
            ?>
        </div>
    <?php
    }
    ?>
</div>
<div class="row">
    <div class="col-md-12>">
        <hr>
    </div>
</div>
<h2>Direct eBay Transaction Import</h2>
<div class="row">
    <div class="col-md-1">
        <a href="#" onclick="window.open('/app/import/index.php','import','top=80,left=80,width=825,height=450,scrollbars=yes,resizable=yes');">
            <img alt='Import Transactions' src='/images/ebay.gif' border=0 align=left width="64px">
        </a>
    </div>
    <div class="col-md-11">
        <?=Yii::$app->params['companyName']?> makes it simple to <i>know</i> your eBay business. Rapidly import all eBay bought or sold items...&nbsp;&nbsp;
        <a href="#" onclick="window.open('/app/import/index.php','import','top=80,left=80,width=825,height=450,scrollbars=yes,resizable=yes');">Take me there</a>
    </div>
</div>
<div class="row">
    <div class="col-md-12>">
        <hr>
    </div>
</div>
<h2>eDocument Storage - Now Available in Beta - Activate Below</h2>
<div class="row">
    <div class="col-md-1">
        <img alt='eDocument Storage' src='/images/dsOn.gif' border=0 align=left>
    </div>
    <?php
    if (!$docStorageEnabled) {
    ?>
    <div class="col-md-11">
        <p>
            Is your receipt management driving you crazy? Can't keep track of all your receipts? Is your office a paper forest?
        </p>
        <p>
            No more faded receipts, no more lost receipts! No more receipt management headaches!
        </p>
        <p>
            It's simple! Import the scanned image of your receipt or document from your computer and link it to your specific transaction.
            Just like Opportunity Tracker, each transaction can be linked to a specific transaction. As an example, if you have an
            extraordinary expense associated with a large sales meeting, import the receipt, a photo of your group, and the original letter
            of invitation, and link them together using eDocument Storage. During the Beta Release, please keep your original receipts as
            additional back-up.
        </p>
        <p>
            We want you to use eDocument Storage in any way that it helps you be more successful, and we would like to hear how it has
            uniquely helped your business. During the Beta Release, please email us and keep us advised. Remember, we are in the business
            of making you more efficient in managing your small business.
        </p>
        <p>
            <ul>
                <li> Expense receipts </li>
                <li> Marketing flyers, documents, or a resume </li>
                <li> Logo of your prospective client or of a joint promotion </li>
                <li> A special photo of your team or your kids </li>
                <li> Your incorporation documents or an important written contract </li>
            </ul>
        </p>
        <p>
            Simply upload and safely store your files and documents in eDocument Storage!
        <p/>
        <p>
            I want eDocument storage!<img src="/images/c.gif" width=10 height=0>
            <a href="<?= Url::to(['service/doc-storage-license']) ?>">
                <img src="/images/activate.gif" border="0" align="top">
            </a>
        </p>
    </div>
    <?php }
        else {
    ?>
    <div class="col-md-11">
        <i>Your eDocument storage service is already active!</i>&nbsp;&nbsp; <a href="<?= Url::to([$this->context->getHomeUrl().'/doc-storage']) ?>">Take me there</a>
        <?php
        echo Html::beginForm(['service/doc-storage-disable']) . Html::submitButton('Turn it off!', [
                'class' => 'btn btn-success',
                'data-confirm' => 'Turning off your eDocument Storage will result in the loss of your current stored documents.
                                   Click "OK" if you want to continue to turn it off this service.'
            ]) . Html::endForm();
        ?>

    </div>
    <?php
        }
    ?>
</div>
<div class="row">
    <div class="col-md-12>">
        <hr>
    </div>
</div>
<h2>Full Tax Services!!</h2>
<div class="row">
    <div class="col-md-12">
        Apply for our QuickStart Program to get started fast and eliminate the backlog of your business transactions.  What a relief to be completely caught up and have your <?=Yii::$app->params['companyName']?> account set up for your particular business.
        <p>Leverage the knowledge and experience of small business accountants who are here to help you maximize your business profits.  From tax returns to business advice to bullet proofing your business, our accountants are here to help!
        <p>To learn more contact us at <a href="mailto:<?=Yii::$app->params['supportEmail']?>"><?=Yii::$app->params['supportEmail']?></a>.</p>
    </div>
</div>

