<?php

namespace app\modules\service\controllers;


use Yii;
use app\modules\service\components\Controller;
use common\components\OpportunityTracker;
use common\components\DocStorage;
use app\models\OpportunityLicenseForm;
use yii\helpers\Url;

/**
 * ServiceController
 */
class ServiceController extends Controller
{
    public function actionIndex()
    {
        $opportunityEnabled = OpportunityTracker::isEnabled();
        $docStorageEnabled = DocStorage::isEnabled();

        return $this->render('index',[
            'opportunityEnabled'=> $opportunityEnabled,
            'docStorageEnabled' => $docStorageEnabled
        ]);
    }

    public function actionTrackerLicense()
    {
        $model = new OpportunityLicenseForm();
        $this->titlePage = "Opportunity Tracker Agreement";

        return $this->render('tracker-license', [
            'model' => $model,

        ]);
    }

    public function actionTrackerTraining()
    {
        $model = new OpportunityLicenseForm();
        $request = Yii::$app->request;
        $flow = $request->post('OpportunityLicenseForm');


        if (count($flow)==0){
            return $this->redirect(Url::to(['/service']));
        }
        $this->titlePage = "Opportunity Tracker Training";

        return $this->render('tracker-training', [
            'model' => $model,
            'flow'  => $flow["flow"]
        ]);
    }

    public function actionTrackerEnable()
    {
        $request = Yii::$app->request;
        $flow = $request->post('OpportunityLicenseForm');

        if (count($flow)>0) {
            if (OpportunityTracker::Enable($flow["flow"])){
                return $this->redirect(Url::to(['/service/opportunity']));
            }
            else {
                return $this->redirect(Url::to(['/service']));
            }
        }
        else {
            return $this->redirect(Url::to(['/service']));
        }
    }

    public function actionTrackerDisable()
    {
        OpportunityTracker::Disable();
        return $this->redirect(Url::to(['/service']));
    }

    public function actionDocStorageLicense()
    {
        $model = new OpportunityLicenseForm();
        //$this->titlePage = "eDocument Storage Agreement";
        $this->view->title ="eDocument Storage Agreement";

        return $this->render('doc-storage-license', [
            'model' => $model,
        ]);
    }

    public function actionDocStorageEnable()
    {
        DocStorage::Enable();
        return $this->redirect(Url::to(['/service']));
    }

    public function actionDocStorageDisable()
    {
        DocStorage::Disable();
        return $this->redirect(Url::to(['/service']));
    }
}
