<?php
namespace app\modules\service\components;

use Yii;

/**
 * Controller
 */
class Controller extends \app\components\Controller
{
    public $relatedLinks = [
        '/service' => 'Premium Services',
        '/service/opportunity' => 'Opportunity Tracker',
        '/service/doc-storage' => 'eDocument Storage',
    ];
}
