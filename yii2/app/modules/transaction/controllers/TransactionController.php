<?php
namespace app\modules\transaction\controllers;

use Yii;
use app\modules\transaction\components\Controller;
use common\models\frontend\Transaction;
use common\models\frontend\TransactionSearch;
use common\models\frontend\BankAccount;
use yii\web\NotFoundHttpException;

/**
 * TransactionController
 */
class TransactionController extends Controller
{
    public function actionIndex()
    {
        $searchModel = new TransactionSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);

    }

    public function actionCreate()
    {
        $transactionModel = new Transaction();
        $transactionModel->scenario = Transaction::TYPE_FUNDS_IN;
        //$transactionModel->date=(new \DateTime())->format('Y-m-d');
        if ( ($transactionModel->load(Yii::$app->request->post())) && ($transactionModel->save())){

        }
        else
        {
            return $this->renderAjax('_form', [
                'transactionModel' => $transactionModel,
            ]);
        }
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $beginBalance = false;
        if ($model->type == 'B') $beginBalance = true;
        $bankAccountModel = new BankAccount();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
        } else {
            return $this->renderAjax('_form', [
                'transactionModel' => $model,
                'bankAccountModel' => $bankAccountModel,
                'beginBalance' => $beginBalance
            ]);
        }
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Transaction the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Transaction::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
