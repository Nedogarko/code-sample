<?php
namespace app\modules\transaction\controllers;

use Yii;
use common\models\frontend\BankAccount;
use app\modules\transaction\components\Controller;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;
use common\models\frontend\Transaction;

class AccountsController extends Controller
{
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => BankAccount::find()
                ->select([
                    '{{BANK_ACCOUNTS}}.*',
                    'sum(ifnull({{TRANSACTION}}.amount, 0)) as total_balance',
                    'sum( CASE  WHEN ifnull({{TRANSACTION}}.amount, 0)>=0 THEN  ifnull({{TRANSACTION}}.amount,0) ELSE null END ) as total_in',
                    'sum( CASE  WHEN ifnull({{TRANSACTION}}.amount, 0)<0 THEN  ifnull({{TRANSACTION}}.amount,0) ELSE null END ) as total_out'
                ])
                ->joinWith('transaction')
                ->groupBy('{{BANK_ACCOUNTS}}.id')
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'totals' => BankAccount::getSumTotal(true),
        ]);
    }

    public function actionCreate()
    {
        $model = new BankAccount();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
        } else {
            return $this->renderAjax('_form', [
                'model' => $model,
            ]);
        }
    }

    public function actionUpdate($id)
    {
        $model = $this->findBankAccountModel($id);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
        } else {
            return $this->renderAjax('_form', [
                'model' => $model,
            ]);
        }
    }

    public function actionBeginBalance()
    {
        $transactionModel = new Transaction();
        $transactionModel->scenario = Transaction::TYPE_BEGINNING_BALANCE;
        if ($transactionModel->load(Yii::$app->request->post()) && $transactionModel->save()) {

        } else {
            return $this->renderAjax('_begin_balance_form', [
                'model' => $transactionModel,
            ]);
        }
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return BankAccount the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findBankAccountModel($id)
    {
        if (($model = BankAccount::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
