<?php
namespace app\modules\transaction\components;

use common\components\Helper;
use common\models\frontend\Transaction;
use Yii;

/**
 * Controller
 */
class Controller extends \app\components\Controller
{
    public $relatedLinks = [
        '/transaction' => 'Transactions',
        //'/transaction/view-edit-transaction' => 'View / Edit Transactions',
        //'/transaction/transfer-funds' => 'Transfer Funds',
        //'/transaction/setup-beginning-balance' => 'Setup Beginning Balance',
        '/transaction/accounts' => 'Manage Accounts',
        '/transaction/loans' => 'Manage Loans',
        '/transaction/to-froms' => 'Manage To/From\'s',
        '/transaction/custom-tags' => 'Manage Custom Tags',
        //'/transaction/import-transactions' => 'Import Transactions',
        '/transaction/first-time-in-help' => 'First Time In Help',
    ];

    public function getInfoWidget()
    {
        $data = Transaction::getInfoWidgetData();
        $result = [
            'header' => date('Y') . ' YTD Bottom Line',
            'items' => [
                'net-income' => [
                    'items' => [
                        'Income' => Helper::formatNumber($data["net_income"]["income"],''),
                        'Expense' => Helper::formatNumber($data["net_income"]["expense"],''),
                        'Net Income' => Helper::formatNumber($data["net_income"]["net_income"],'')
                    ],
                    'label' => 'Net Income'
                ],
                'funds-in-out' => [
                    'items' => [
                        'Funds In' => Helper::formatNumber($data["funds_in_out"]["funds_in"],''),
                        'Funds Out' => Helper::formatNumber($data["funds_in_out"]["funds_out"],''),
                        'Total' => Helper::formatNumber($data["funds_in_out"]["total"],''),
                    ],
                    'label' =>'Funds In/Out'
                ],
            ],
        ];

        return $result;
    }
}
