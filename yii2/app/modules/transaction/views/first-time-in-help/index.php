<?php
use yii\helpers\Url;
?>


<p>
    You have two options for getting started entering your business transactions.
</p>
<p>
    <b>Option 1</b> - If you are an existing business and have a starting account balance that you want to bring into <?php echo  Yii::$app->params['companyName']; ?> now for your business bank or credit card account take this path.
</p>
<p>
    Setting up your starting account balance will allow <?php echo  Yii::$app->params['companyName']; ?> to show you the correct account balance; until this
    step is complete your account balance will be incorrect.  This step could take a little bit of time.
    <b><a href="<?= Url::to(['/balance/first-time-in-help']) ?>">Take Me To Balance Accounts</a></b>
</p>
<hr>
<p>
    <b>Option 2</b> - Go ahead and get started entering your business transactions.   If you have a starting account balance to set up, you can come back and set it up
    later when you balance your account for the first time.
</p>
<p>
    Here's an every day example - you have just been to the office supply store and have a receipt for your purchase - with this receipt in hand you have everything you
    need to enter your business transaction.  Every transaction has a <b>Transaction Type</b> and think of it this way - ask yourself are funds coming into or going
    out of my account, the <b>Account</i></b> (you used your Visa), choose a <b>Method</i></b> so you can track how you received or disbursed funds.
    The <b>Date</i></b> is the date you spent the funds and <b>To/From</i></b> identifies where funds were received from or disbursed to
    (the name of the office supply store).
</p>
<p>
    If you have one receipt that needs to be split out for different types of purchases use the <b>Split</i></b> function.
</p>
<p align="center">
    <img src="/images/shots/Enter-Trx-sized.jpg">
</p>
<p>
    One very important step is selecting the <b>Category for your entry</i></b>.  As we said in our philosophy, <?php echo  Yii::$app->params['companyName']; ?>
    has made this simple for you.  We have purposely fixed the categories into groupings that make your tax reporting easy and more straight forward.
    In this example, we happen to know that office supplies are categorized as an 'Office Expense'.
    But if we did not know the right category - we would use the <?php echo  Yii::$app->params['companyName']; ?>' <b>Guide Me</i></b> to figure it out.
    One last note, if you cannot figure it out, don't stress - just mark the entry as <b>'Unknown Category - To Be Resolved'</i></b> - you can always come
    back and change the category later once you have the answer.  If you wanted a secondary level of detail recorded for this transaction,
    use a <b>Custom Tag</i></b> to personalize the record keeping for your business.  <b>Amount</i></b> is the dollar amount received or disbursed.
</p>
<p>
    To continue, the same is true when you get paid after making a sale. Let's say you receive a check - you know the transaction type, the account you are going
    to be depositing the funds into, the method, the date you received the funds, the To/From (the name of your customer) and the amount- just select a category and
    you are done.  <b>Yes, it really is that simple!</b>
</p>
<p>
    One last perk - <b>Last Five Transactions Entered</i></b> displays your entries by account for easy reference as you enter your business transactions.
</p>
<p>
    Use the <b>Related Activities</i></b> on the right side of your screen to View and Edit Transactions and Manage your Accounts, To/From set up and Custom Tags.
</p>
<p>
    <b>Bottom Line</i></b> is a quick reference for the year-to-date funds in vs. funds out for your business.
</p>
<p align="center"><b><a href="<?= Url::to([$this->context->getHomeUrl()]) ?>">Take Me to Enter Transactions</a></b></p>
