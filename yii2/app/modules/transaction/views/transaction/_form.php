<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\widgets\Pjax;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use common\models\frontend\BankAccount;
use common\models\frontend\Transaction;
use common\models\frontend\Source;
use common\models\backend\Category;
use common\models\frontend\CustomTag;
use dosamigos\datepicker\DatePicker;
use common\models\frontend\Auto;

/* @var $this yii\web\View */
/* @var $bankAccountModel common\models\frontend\BankAccount */
/* @var $transactionModel common\models\frontend\Transaction */
/* @var $form yii\widgets\ActiveForm */

$js = <<<JS
    if($("#transaction-check_number").val()=="") {
        $(".field-transaction-check_number").hide();
    }
    else {
        $(".field-transaction-check_number").show();
    }

    $('#transaction-method_id').on('change', function(){
        if($(this).val()=="Check"){
            $(".field-transaction-check_number").show();
        }
        else{
            $(".field-transaction-check_number").hide();
        }

    });


JS;
$this->registerJs($js);

?>




<?php Pjax::begin(); ?>
<div class="model-content-wrapper">
<?php $form = ActiveForm::begin([
    'options' => [
        'class' => 'form-horizontal',
        'data-pjax' => true,
        'id' => 'transaction-form'
    ],
    'fieldConfig' => [
        'template' => "<div class=\"col-md-3\">{label}</div>\n<div class=\"col-md-9\">{input}</div>\n<div class=\"col-md-12\">{error}</div>",
    ],
]); ?>

<?= $form->field($transactionModel, 'type')
    ->widget(Select2::classname(), [
        'data' => Transaction::getTypes($beginBalance),
        'options' => ['placeholder' => 'Select A Type'],

    ]) ?>

<?= $form->field($transactionModel, 'account_id')->label('Account')
    ->widget(Select2::classname(), [
        'data' => ArrayHelper::map(BankAccount::getAllTotal(), 'id', 'name_total_balance'),
        'options' => ['placeholder' => 'Select A Account'],


    ]) ?>

<?= $form->field($transactionModel, 'method_id')
    ->widget(Select2::classname(), [
        'data' => Transaction::getMethods(),
        'options' => ['placeholder' => 'Select A Method'],
    ]) ?>
<?= $form->field($transactionModel, 'check_number')
    ->textInput() ?>

<?= $form->field($transactionModel, 'date')
    ->defaultValue((new \DateTime())->format('m/d/Y'))
    ->widget(DatePicker::className()) ?>


<?= $form->field($transactionModel, 'source_id')->label('To/From')
    ->widget(Select2::classname(), [
        'data' => ArrayHelper::map(Source::getAll(false, $beginBalance), 'id', 'name'),
        'options' => ['placeholder' => 'Select A To/From'],
    ]) ?>

<?= $form->field($transactionModel, 'category_id')
    ->widget(Select2::classname(), [
        'data' => ArrayHelper::map(Category::getAll(), 'id', 'name'),
        'options' => ['placeholder' => 'Select A Category'],

    ]) ?>

<?= $form->field($transactionModel, 'custom_tag_id')
    ->widget(Select2::classname(), [
        'data' => ArrayHelper::map(CustomTag::getAll(), 'id', 'name'),
        'options' => ['placeholder' => 'Select A Custom Tag'],
    ]) ?>
<?= $form->field($transactionModel, 'auto_id')
    ->widget(Select2::classname(), [
        'data' => ArrayHelper::map(Auto::getAll(), 'id', 'auto_name'),
        'options' => ['placeholder' => 'Select A Custom Tag'],
    ]) ?>


<?= $form->field($transactionModel, 'amount')
    ->textInput(['type' => 'number']) ?>

<?= $form->field($transactionModel, 'memo')
    ->textInput(['maxlength' => true]) ?>

<div class="form-group modal-footer">
        <?= Html::submitButton('<span class="glyphicon glyphicon-ok-circle"></span>' . ($transactionModel->isNewRecord
                ? ' Add'
                : ' Update'), [
            'class' => 'btn btn-success'
        ]) ?>
        <button type="button" class="btn btn-success" data-dismiss="modal"><span
                class="glyphicon glyphicon-ban-circle"></span> Cancel
        </button>

</div>

<?php ActiveForm::end(); ?>
</div>
<?php Pjax::end(); ?>
