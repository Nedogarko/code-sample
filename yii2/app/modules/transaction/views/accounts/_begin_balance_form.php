<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\widgets\Pjax;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use common\models\frontend\BankAccount;
use common\models\frontend\Transaction;
use dosamigos\datepicker\DatePicker;
use common\models\frontend\Source;


/* @var $this yii\web\View */
/* @var $model common\models\frontend\Transaction */
/* @var $form yii\widgets\ActiveForm */
?>

<?php Pjax::begin(); ?>
<?php $form = ActiveForm::begin([
    'options' => [
        'class' => 'form-horizontal',
        'data-pjax' => true,
        'id' => 'begin-balance-form'
    ],
    'fieldConfig' => [
        'template' => "<div class=\"col-md-3\">{label}</div>\n<div class=\"col-md-9\">{input}</div>\n<div class=\"col-md-12\">{error}</div>",
    ],
]); ?>

<?= $form->field($model, 'type')
    ->widget(Select2::classname(), [
        'data' => [Transaction::TYPE_BEGINNING_BALANCE => 'Beginning Balance'],
    ]) ?>

<?= $form->field($model, 'account_id')
    ->widget(Select2::classname(), [
        'data' => ArrayHelper::map(BankAccount::getAllTotal(), 'id', 'name_total_balance'),
        'options' => ['placeholder' => 'Select A Account Type'],
    ]) ?>

<?= $form->field($model, 'source_id')->label('To/From')
    ->widget(Select2::classname(), [
        'data' => ArrayHelper::map(Source::getAll(false, true), 'id', 'name'),
    ]) ?>

<?= $form->field($model, 'date')
    ->widget(DatePicker::className()) ?>

<?= $form->field($model, 'amount')
    ->textInput(['type' => 'number']) ?>

<?= $form->field($model, 'memo')
    ->textInput(['maxlength' => true]) ?>

<div class="form-group modal-footer">
        <?= Html::submitButton('<span class="glyphicon glyphicon-ok-circle"></span>Save', [
            'class' => 'btn btn-success'
        ]) ?>
        <button type="button" class="btn btn-success" data-dismiss="modal"><span
                class="glyphicon glyphicon-ban-circle"></span> Cancel
        </button>
</div>

<?php ActiveForm::end(); ?>
<?php Pjax::end(); ?>
