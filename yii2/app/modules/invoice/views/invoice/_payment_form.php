<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\widgets\Pjax;
use dosamigos\datepicker\DatePicker;

/* @var $invoicePayment common\models\frontend\invoicePayment */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="col-lg-12 text-center"><b>Invoice Payments</b></div>
<div class="col-lg-12">
    <?php Pjax::begin(); ?>
    <?php $form = ActiveForm::begin([
        'options' => [
            'id' => 'invoice-payment-form',
            'class' => 'form-horizontal',
            'data-pjax' => true,
        ],
        'fieldConfig' => [
            'template' => "<div class=\"col-md-3\">{label}</div>\n<div class=\"col-md-9\">{input}</div>\n<div class=\"col-md-12\">{error}</div>",
        ],
    ]); ?>


    <?= $form->field($invoicePayment, 'date')
        ->defaultValue((new \DateTime())->format('m/d/Y'))
        ->widget(DatePicker::className()) ?>


    <?= $form->field($invoicePayment, 'desc')
        ->textInput(['maxlength' => true]) ?>

    <?= $form->field($invoicePayment, 'method')
        ->textInput(['maxlength' => true]) ?>

    <?= $form->field($invoicePayment, 'payment_id')
        ->textInput(['maxlength' => true]) ?>

    <?= $form->field($invoicePayment, 'amount')
        ->textInput(['maxlength' => true]) ?>

    <div class="form-group modal-footer">
        <?= Html::submitButton('<span class="glyphicon glyphicon-ok-circle"></span> ' . ($invoicePayment->isNewRecord
                ? 'Add'
                : 'Update'), [
            'class' => 'btn btn-success'
        ]); ?>
        <button type="button" class="btn btn-success" data-dismiss="modal"><span
                class="glyphicon glyphicon-ban-circle"></span> Cancel
        </button>
    </div>
    <?php ActiveForm::end(); ?>
    <?php Pjax::end(); ?>
</div>
