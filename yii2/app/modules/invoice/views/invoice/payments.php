<?php
    use yii\helpers\Html;
    use yii\widgets\Pjax;
    use common\components\GridView;
    use yii\helpers\Url;

    /* @var $this yii\web\View */
    /* @var $invoicePayment common\models\frontend\InvoicePayment */
    /* @var $dataProvider yii\data\ActiveDataProvider */
    /* @var $retURL string */

    echo $this->render('_invoice_header',array('invoice' => $invoice));
?>
<div class="row text-center"><b>Payments</b></div>
<div class="body-content-options">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-6">
                <div class="row">
                    <button class="btn btn-success plus" id="btnItemAdd" data-url="<?= Url::to([
                        'invoice/payment',
                        'invoiceId' => $invoice->id,
                    ]) ?>"><i
                            class="fa fa-plus" aria-hidden="true"> </i> Add
                    </button>
                    <a class="btn btn-success plus" href="<?= (isset($retURL) && !is_null($retURL))
                        ? $retURL
                        : Url::to('/invoice') ?>"><i
                            class="fa fa-arrow-left" aria-hidden="true"> </i> Return
                    </a>
                </div>
            </div>
            <div class="col-md-6 actions">
                <button title="Update" data-action="update" disabled="disabled" class="btn btn-action"
                        type="button">
                    <span class="action edit"></span>
                </button>
                <button title="Delete" data-action="delete" disabled="disabled" class="btn btn-action"
                        type="button">
                    <span class="action delete"></span>
                </button>
            </div>
        </div>
    </div>
</div>
<div>
    <?php Pjax::begin(['id' => 'invoice-payments-pjax-grid']) ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'showFooter' => true,
        'id' => 'invoice-payments',
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'date',
                'headerOptions' => ['width' => '100px;'],
                'contentOptions' => ['class' => 'text-left'],
            ],
            [
                'attribute' => 'desc',
                'headerOptions' => ['width' => '200px;'],
                'contentOptions' => ['class' => 'text-left'],
            ],
            [
                'attribute' => 'method',
                'headerOptions' => ['width' => '200px;'],
                'contentOptions' => ['class' => 'text-left'],
            ],
            [
                'attribute' => 'payment_id',
                'headerOptions' => ['width' => '70px;'],
            ],
            [
                'label' => 'Amount',
                'value' => 'amount',
                'headerOptions' => ['width' => '70px;'],
                'contentOptions' => ['class' => 'text-right'],
                'footerOptions' => ['class' => 'text-right'],
                'footer' => $invoice->total_sum_payment

            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}',
                'contentOptions' => ['style' => 'display:none;'],
                'buttons' => [
                    'update' => function ($url, $invoicePayment) {
                        return Html::button('<span class="glyphicon glyphicon-pencil"></span>', [
                            'title' => 'Update',
                            'data-action' => 'update',
                            'class' => 'btn btn-link',
                            'data-url' => Url::to([
                                'invoice/payment',
                                'invoiceId' => $invoicePayment->invoice->id,
                                'id' => $invoicePayment->id,
                            ])
                        ]);
                    },
                    'delete' => function ($url, $invoicePayment) {
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', Url::to([
                            'invoice/delete-invoice-payment',
                            'id' => $invoicePayment->id
                        ]), [
                            'data-action' => 'delete',
                            'data-pjax' => 'invoice-payments-pjax-grid',
                        ]);
                    },
                ],
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>

<?= \app\components\PjaxModalForm::get([
    'modalId' => 'modalInvoicePayment',
    'modalHeader' => 'Payment',
    'selectorActivation' => '[data-action="update"],#btnItemAdd',
    'formId' => 'invoice-payment-form',
    'gridPjaxId' => 'invoice-payments-pjax-grid',
]) ?>
