<?php
    use common\models\frontend\Invoice;
?>
<div class="col-md-2 col-lg-2 text-right"><b><?= $invoice->getAttributeLabel('inv_date'); ?>:</b></div>
<div class="col-md-4 col-lg-4"><?= $invoice->inv_date; ?></div>
<div class="col-md-2 col-lg-2 text-right"><b><?= $invoice->getAttributeLabel('due_date'); ?>:</b></div>
<div class="col-md-4 col-lg-4"><?= $invoice->due_date; ?>&nbsp;</div>
<div class="col-md-2 col-lg-2 text-right"><b><?= $invoice->getAttributeLabel('inv_number'); ?>:</b></div>
<div class="col-md-4 col-lg-4"><?= $invoice->inv_number ?></div>
<div class="col-md-2 col-lg-2 text-right"><b><?= $invoice->getAttributeLabel('source_id'); ?>:</b></div>
<div class="col-md-4 col-lg-4"><?= $invoice->getSource()->one()->name; ?></div>

<div class="col-md-2 col-lg-2 text-right"><b><?= $invoice->getAttributeLabel('terms'); ?>:</b></div>
<div class="col-md-4 col-lg-4 "><?= Invoice::getTerms($invoice->terms); ?></div>
<div class="col-md-2 col-lg-2 text-right"><b><?= $invoice->getAttributeLabel('tax_rate'); ?>:</b></div>
<div class="col-md-4 col-lg-4 "><?= $invoice->tax_rate; ?>%</div>
<!--
<div class="col-md-2 col-lg-2 text-right"><b>Total:</b></div>
<div class="col-md-4 col-lg-4 "><?= $invoice->total_sum_with_tax; ?></div>
<div class="col-md-2 col-lg-2 text-right"><b></b></div>
<div class="col-md-4 col-lg-4 ">&nbsp;</div>
-->