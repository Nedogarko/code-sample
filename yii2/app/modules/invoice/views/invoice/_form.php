<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\widgets\Pjax;
use kartik\select2\Select2;
use dosamigos\datepicker\DatePicker;
use yii\helpers\ArrayHelper;
use common\models\frontend\Source;
use common\models\frontend\Invoice;


/* @var $this yii\web\View */
/* @var $model common\models\frontend\BankAccount */
/* @var $form yii\widgets\ActiveForm */
?>

<?php Pjax::begin(); ?>
<div class="model-content-wrapper">
<?php $form = ActiveForm::begin([
        'options' => [
            'class' => 'form-horizontal',
            'data-pjax' => true,
            'id' => 'invoice-form'
        ],
        'fieldConfig' => [
            'template' => "<div class=\"col-md-3\">{label}</div>\n<div class=\"col-md-9\">{input}</div>\n<div class=\"col-md-12\">{error}</div>",
        ],
    ]); ?>

    <?= $form->field($model, 'inv_number')
        ->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'inv_date')
        ->defaultValue((new \DateTime())->format('m/d/Y'))
        ->widget(DatePicker::className()) ?>

    <?= $form->field($model, 'terms')
        ->widget(Select2::classname(), [
            'data' => Invoice::getTerms(),
            'options' => ['placeholder' => 'Select terms'],
        ]);
    ?>

    <?= $form->field($model, 'source_id')
        ->widget(Select2::classname(), [
            'data' => ArrayHelper::map(Source::getInvoiceCustomer(), 'id', 'name', 'group'),
            'options' => ['placeholder' => 'Select Customer'],
        ]);
    ?>

    <?= $form->field($model, 'po_number')
        ->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'due_date')
        ->defaultValue((new \DateTime())->format('m/d/Y'))
        ->widget(DatePicker::className()) ?>

    <?= $form->field($model, 'bill_addr')
        ->textarea(['maxlength' => true]) ?>

    <?= $form->field($model, 'ship_addr')
        ->textarea() ?>

    <div class="form-group modal-footer">
        <?= Html::submitButton('<span class="glyphicon glyphicon-ok-circle"></span>' . ($model->isNewRecord
                ? ' Add'
                : ' Update'), [
            'class' => 'btn btn-success'
        ]) ?>
        <button type="button" class="btn btn-success" data-dismiss="modal"><span
                class="glyphicon glyphicon-ban-circle"></span> Cancel
        </button>
    </div>

    <?php ActiveForm::end(); ?>
</div>
<?php Pjax::end(); ?>
