<?php

use yii\helpers\Html;
use yii\helpers\Url;
use common\components\GridView;
use yii\widgets\Pjax;

/* @var $dataProvider yii\data\ActiveDataProvider */
?>

<div class="body-content-options">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-6">
                <div class="row">
                    <button class="btn btn-success plus" id="btnAdd" data-url="<?= Url::to([
                        'create',
                    ]) ?>"><i
                            class="fa fa-plus" aria-hidden="true"> </i> Add New
                    </button>
                </div>
            </div>
            <div class="col-md-6 actions">
                <button title="Update" data-action="update" disabled="disabled" class="btn btn-action"
                        type="button">
                    <span class="action edit"></span>
                </button>
            </div>

        </div>
    </div>
</div>
<div>
    <?php Pjax::begin(['id' => 'nvoice-item-pjax-grid']) ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'id' => 'table',
        'showFooter' => true,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'label' => 'Active',
                'attribute' => 'active',
                'contentOptions' => ['style' => 'width:70px;']
            ],
            [
                'label' => 'Tax',
                'attribute' => 'taxable',
                'contentOptions' => ['style' => 'width:70px;']
            ],
            [
                'label' => 'Name',
                'value' => 'name',
                'contentOptions' => ['class'=>'text-left'],
            ],
            'desc',
            [
                'label' => 'Price',
                'value' => function ($model) {
                    return Yii::$app->formatter->asDecimal($model->price,2);
                },
            ],
            [
                'label' => 'Cost',
                'value' => function ($model) {
                    return Yii::$app->formatter->asDecimal($model->cost,2);
                },
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update}',
                'contentOptions' => ['style' => 'display:none;'],
                'buttons' => [
                    'update' => function ($url, $model) {
                        return Html::button('<span class="glyphicon glyphicon-pencil"></span>', [
                            'title' => 'Update',
                            'data-action' => 'update',
                            'class' => 'btn btn-link update',
                            'data-url' => Url::to([
                                'invoice-item/update',
                                'id' => $model->id
                            ])
                        ]);
                    }
                ],
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>

<?= \app\components\PjaxModalForm::get([
    'modalId' => 'modal',
    'modalHeader' => 'Invoice Item',
    'selectorActivation' => '#btnAdd, [data-action="update"]',
    'formId' => 'invoice-item-form',
    'gridPjaxId' => 'nvoice-item-pjax-grid'
]) ?>

