<?php
use common\components\Helper;
use app\models\ReportFilterForm;

?>
<div style="width:100%; " class="text-center">
    <h1><?=$this->context->titlePage;?></h1>
</div>
<div style="width:100%;  " class="text-center">
    <table width="100%" border="0">
        <tr>
            <td align="center"><h4><?= $order->billing_company ?></h4></td>
        </tr>
        <tr>
            <td align="center"><h4><?= $from ?> – <?= $to ?></h4></td>
        </tr>
    </table>
</div>
<br>
<table class="table-bordered-report" width="100%"
    <?php

    if ($output != ReportFilterForm::VIEW_PDF) {
        echo ' border="1" ';
    }
    ?>
    >
    <?php
    if ($type_view!=ReportFilterForm::VIEW_TYPE_SHORT) {
        ?>
        <tr>
            <td><b>Date</b></td>
            <td><b>Method</b></td>
            <td><b>To/From</b></td>
            <td><b>Funds In</b></td>
            <td><b>Funds Out</b></td>
            <td><b>Category</b></td>
            <td><b>Custom Tag</b></td>
            <td><b>Auto</b></td>
            <td><b>Memo</b></td>
        </tr>

        <?php
        foreach ($data["detailed"] as $item){
            ?>
            <tr>
                <td><?=Helper::toAppDate($item["date"])?></td>
                <td><?=$item["method_id"]?></td>
                <td><?=$item["source_name"]?></td>
                <td class="text-right"><?=$item["funds_in"]?></td>
                <td class="text-right"><?=$item["funds_out"]?></td>
                <td><?=$item["category_name"]?></td>
                <td><?=$item["custom_tag_name"]?></td>
                <td><?=$item["auto_name"]?></td>
                <td><?=$item["memo"]?></td>

            </tr>
        <?php
        }
    }

    if ($type_view==ReportFilterForm::VIEW_TYPE_SHORT) {
        ?>
        <tr>
            <td><b>Auto</b></td>
            <td><b>Funds In</b></td>
            <td><b>Funds Out</b></td>
            <td><b>Memo</b></td>
        </tr>

    <?php
    }
    $total = 0;
    foreach ($data["short"] as $key => $item){
        ?>
        <tr>
            <td colspan="<?php echo $type_view!=ReportFilterForm::VIEW_TYPE_SHORT?3:0;?>" ><b><?=$key?> Subtotal:</b></td>
            <td class="text-right"><b><?=Helper::formatNumber($item["in"]);?></b></td>
            <td class="text-right"><b><?=Helper::formatNumber($item["out"]);?></b></td>
            <td colspan="<?php echo $type_view!=ReportFilterForm::VIEW_TYPE_SHORT?4:0;?>" ></td>
        </tr>
    <?php
    }
    ?>
    <tr>
        <td colspan="<?php echo $type_view!=ReportFilterForm::VIEW_TYPE_SHORT?3:0;?>" ><b>Total:</b></td>
        <td class="text-right"><b><?=Helper::formatNumber($data["total_in"],'')?></b></td>
        <td class="text-right"><b><?=Helper::formatNumber($data["total_out"],'')?></b></td>
        <td colspan="<?php echo $type_view!=ReportFilterForm::VIEW_TYPE_SHORT?4:0;?>"></td>
    </tr>
</table>
