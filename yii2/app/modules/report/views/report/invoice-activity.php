<?php
use common\components\Helper;
use app\models\ReportFilterForm;

?>
<div style="width:100%; " class="text-center">
    <h1><?=$this->context->titlePage;?></h1>
</div>
<div style="width:100%;  " class="text-center">
    <table width="100%" border="0">
        <tr>
            <td align="center"><h4><?= $order->billing_company ?></h4></td>
        </tr>
        <tr>
            <td align="center"><h4><?= $from ?> – <?= $to ?></h4></td>
        </tr>
    </table>
</div>
<br>
<table class="table-bordered-report" width="100%"
    <?php

    if ($output != ReportFilterForm::VIEW_PDF) {
        echo ' border="1" ';
    }
    ?>
    >
    <?php
    if ($type_view != ReportFilterForm::VIEW_TYPE_SHORT) {
        ?>
        <tr>
            <td><b> #</b></td>
            <td><b>Inv. #</b></td>
            <td><b>Date</b></td>
            <td><b>Due Date</b></td>
            <td><b>Customer</b></td>
            <td><b>Terms</b></td>
            <td><b>Amount Due</b></td>
            <td><b>Invoice Total</b></td>
            <td><b>Payment Total</b></td>
            <td><b>Invoice Status</b></td>
        </tr>
    <?php
    } else {
        ?>
        <tr>
            <td><b>#</b></td>
            <td><b>Customer</b></td>
            <td colspan="<?php echo $type_view == ReportFilterForm::VIEW_TYPE_SHORT ? 2 : 0; ?>"><b>Invoice Total</b>
            </td>
        </tr>

    <?php
    }
    $totalInvoice = 0;
    $count = 1;
    $customerName = '';
    $totalCustomer = 0;
    $countCustomer = 0;
    foreach ($data["detailed"] as $item) {
        if ($customerName != $item["source_name"]) {
            if ($count != 1) {
                ?>
                <tr>
                    <td class="text-right"><?php echo $type_view == ReportFilterForm::VIEW_TYPE_SHORT ? $countCustomer : ''; ?></td>
                    <td colspan="<?php echo $type_view != ReportFilterForm::VIEW_TYPE_SHORT ? 4 : 0; ?>">
                        <b><?= $customerName ?></b></td>
                    <?php
                    if ($type_view != ReportFilterForm::VIEW_TYPE_SHORT) {
                        ?>
                        <td colspan="2" class="text-right"><b>Subtotal:</b></td>
                    <?php
                    }
                    ?>
                    <td class="text-right"><?= Helper::formatNumber($totalCustomer) ?></td>
                    <td colspan="<?php echo $type_view != ReportFilterForm::VIEW_TYPE_SHORT ? 2 : 0; ?>"></td>
                </tr>
            <?
            }
            $customerName = $item["source_name"];
            $totalCustomer = 0;
            $countCustomer++;
        }
        if ($type_view != ReportFilterForm::VIEW_TYPE_SHORT) {
            ?>

            <tr>
                <td class="text-right"><?= $count ?></td>
                <td><?= $item["inv_number"] ?></td>
                <td><?= Helper::toAppDate($item["inv_date"]) ?></td>
                <td><?= Helper::toAppDate($item["due_date"]) ?></td>
                <td><?= $item["source_name"] ?></td>
                <td class="text-right"><?= $item["terms"] ?></td>
                <td class="text-right"><?= Helper::formatNumber($item["amount_due"]) ?></td>
                <td class="text-right"><?= Helper::formatNumber($item["total"]) ?></td>
                <td class="text-right"><?= Helper::formatNumber($item["payment"]) ?></td>
                <td><?= $item["status"] ?></td>
            </tr>
        <?php
        }
        $count++;
        $totalInvoice += $item["total"];
        $totalCustomer += $item["total"];
    }

    if ($count != 1) {
        ?>
        <tr>
            <td class="text-right"><?php echo $type_view == ReportFilterForm::VIEW_TYPE_SHORT ? $countCustomer : ''; ?></td>
            <td colspan="<?php echo $type_view != ReportFilterForm::VIEW_TYPE_SHORT ? 4 : 0; ?>">
                <b><?= $customerName ?></b></td>
            <?php
            if ($type_view != ReportFilterForm::VIEW_TYPE_SHORT) {
                ?>
                <td colspan="2" class="text-right"><b>Subtotal:</b></td>
            <?php
            } ?>
            <td class="text-right"><?= Helper::formatNumber($totalCustomer) ?></td>
            <td colspan="<?php echo $type_view != ReportFilterForm::VIEW_TYPE_SHORT ? 2 : 0; ?>"></td>
        </tr>
    <?
    }
    ?>
    <tr>
        <td></td>
        <td colspan="<?php echo $type_view != ReportFilterForm::VIEW_TYPE_SHORT ? 6 : 0; ?>"><b>Total:</b></td>
        <td class="text-right"><b><?= Helper::formatNumber($totalInvoice) ?></b></td>
        <td colspan="<?php echo $type_view != ReportFilterForm::VIEW_TYPE_SHORT ? 2 : 0; ?>"></td>
    </tr>
</table>
