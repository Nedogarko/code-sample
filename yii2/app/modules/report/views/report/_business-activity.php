<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use dosamigos\datepicker\DatePicker;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use common\models\frontend\Source;
use common\models\frontend\Transaction;
use app\models\ReportFilterForm;
use common\models\frontend\BankAccount;
use common\models\frontend\CustomTag;
use common\models\backend\Category;
?>
<?php
$form = ActiveForm::begin([
    'action' => Url::to([$report]),
    'options' => ['target' => '_blank']
]);
?>
<div class="row">
    <div class="col-md-5">
        <div class="row">
            <?php
            echo $form->field($model, 'from',
                ['template' => '<div class="col-md-4">{label}</div><div class="col-md-6 no-margin">{input}</div>'])
                ->widget(DatePicker::className(), ['addon' => '']);
            ?>
        </div>
    </div>
    <div class="col-md-5">
        <div class="row">
            <?= $form->field($model, 'type',
                ['template' => '<div class="col-md-4">{label}</div><div class="col-md-6 no-margin">{input}</div>'])
                ->widget(Select2::classname(), [
                    'data' => Transaction::getTypes(),
                    'options' => [
                        'placeholder' => 'All',
                        'multiple' => true
                    ],
                ]); ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-5">
        <div class="row">
            <?php
            echo $form->field($model, 'to',
                ['template' => '<div class="col-md-4">{label}</div><div class="col-md-6 no-margin">{input}</div>'])
                ->widget(DatePicker::className(), ['addon' => '']);
            ?>
        </div>
    </div>
    <div class="col-md-5">
        <div class="row">
            <?= $form->field($model, 'account',
                ['template' => '<div class="col-md-4">{label}</div><div class="col-md-6 no-margin">{input}</div>'])
                ->widget(Select2::classname(), [
                    'data' => ArrayHelper::map(BankAccount::getAll(), 'id', 'name'),
                    'options' => [
                        'placeholder' => 'All',
                        'multiple' => true
                    ],
                ]); ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-5">
        <div class="row">
            <?= $form->field($model, 'source',
                ['template' => '<div class="col-md-4">{label}</div><div class="col-md-6 no-margin">{input}</div>'])
                ->widget(Select2::classname(), [
                    'data' => ArrayHelper::map(Source::getInvoiceCustomer(), 'id', 'name', 'group'),
                    'options' => [
                        'placeholder' => 'All',
                        'multiple' => true
                    ],
                ]); ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-5">
        <div class="row">
            <?php
            echo $form->field($model, 'amount_from',
                ['template' => '<div class="col-md-4">{label}</div><div class="col-md-6 no-margin">{input}</div>'])
                ->textInput(['type' => 'number']);;
            ?>
        </div>
    </div>
    <div class="col-md-5">
        <div class="row">
            <?= $form->field($model, 'amount_to',
                ['template' => '<div class="col-md-4">{label}</div><div class="col-md-6 no-margin">{input}</div>'])
                ->textInput(['type' => 'number']);
                 ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-5">
        <div class="row">
            <?= $form->field($model, 'split_level',
                [
                    'template' => '<div class="col-md-4">{label}</div><div class="col-md-7 no-margin">{input}</div>'
                ])
                ->label('Search At')
                ->radioList([
                    ReportFilterForm::SPLIT_LEVEL_TRANSACTION => 'Transaction',
                    ReportFilterForm::SPLIT_LEVEL_SPLIT => 'Split',
                ], [
                    'item' => function ($index, $label, $name, $checked, $value) use ($model) {
                        $id = Html::getInputId($model, 'split_level') . '_' . $index;

                        return '<div class="col-md-6 labeled">' . Html::radio($name, $checked, [
                            'id' => $id,
                            'value' => $value
                        ]) . '<label for="' . $id . '"><span>' . $label . '</span></label></div>';
                    },
                    'value' => ReportFilterForm::SPLIT_LEVEL_TRANSACTION,
                ]) ?>
        </div>
    </div>
</div>
<div class="row" style = "display: none;" id="row_filter">
    <div class="col-md-5">
        <div class="row">
            <?= $form->field($model, 'custom_tag',
                ['template' => '<div class="col-md-4">{label}</div><div class="col-md-6 no-margin">{input}</div>'])
                ->widget(Select2::classname(), [
                    'data' => ArrayHelper::map(CustomTag::getAll(), 'id', 'name'),
                    'options' => [
                        'placeholder' => 'All',
                        'multiple' => true,
                        'visible' =>false
                    ],
                ]); ?>
        </div>
    </div>
    <div class="col-md-5">
        <div class="row">
            <?= $form->field($model, 'category',
                ['template' => '<div class="col-md-4">{label}</div><div class="col-md-6 no-margin">{input}</div>'])
                ->widget(Select2::classname(), [
                    'data' => ArrayHelper::map(Category::getAll(), 'id', 'name'),
                    'options' => [
                        'placeholder' => 'All',
                        'multiple' => true
                    ],
                ]); ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-5">
        <div class="row">
            <?= $form->field($model, 'type_view',
                ['template' => '<div class="col-md-4">{label}</div><div class="col-md-7 no-margin">{input}</div>'])
                //->defaultValue($model::VIEW_HTML)
                ->label('View')
                ->radioList([
                    ReportFilterForm::VIEW_TYPE_DETAILED => 'Detailed',
                    ReportFilterForm::VIEW_TYPE_SHORT => 'Short',
                ], [
                    'item' => function ($index, $label, $name, $checked, $value) use ($model) {
                        $id = Html::getInputId($model, 'type_view') . '_' . $index;

                        return '<div class="col-md-6 labeled">' . Html::radio($name, $checked, [
                            'id' => $id,
                            'value' => $value
                        ]) . '<label for="' . $id . '"><span>' . $label . '</span></label></div>';
                    },
                    'value' => ReportFilterForm::VIEW_TYPE_DETAILED,
                ]) ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-5">
        <div class="row">
            <?= $form->field($model, 'output',
                ['template' => '<div class="col-md-4">{label}</div><div class="col-md-7 no-margin">{input}</div>'])
                //->defaultValue($model::VIEW_HTML)
                ->label('Output')
                ->radioList([
                    ReportFilterForm::VIEW_HTML => 'HTML',
                    ReportFilterForm::VIEW_PDF => 'PDF',
                ], [
                    'item' => function ($index, $label, $name, $checked, $value) use ($model) {
                        $id = Html::getInputId($model, 'output') . '_' . $index;

                        return '<div class="col-md-6 labeled">' . Html::radio($name, $checked, [
                            'id' => $id,
                            'value' => $value
                        ]) . '<label for="' . $id . '"><span>' . $label . '</span></label></div>';
                    },
                    'value' => ReportFilterForm::VIEW_HTML,
                ]) ?>
        </div>
    </div>
</div>


<div class="row">
    <div class="col-md-5  text-center">
        <?php
        echo Html::submitButton('Generate report', [
            'class' => 'btn btn-success',
            'id' => 'btn_report',
        ]);
        $form::end();
        ?>
    </div>
</div>
<?php
$jsScript =<<< JS
     $( "[name*='ReportFilterForm']").click(function(){
        if ($(this).val()=="split"){
            $("#row_filter").show();
        } else {
             $("#row_filter").hide();
        }
     });
JS;

$this->registerJs($jsScript);
?>