<?php
use common\components\Helper;
use app\models\ReportFilterForm;

?>
<div style="width:100%; " class="text-center">
    <h1><?=$this->context->titlePage;?></h1>
</div>
<div style="width:100%;  " class="text-center">
    <table width="100%" border="0">
        <tr>
            <td align="center"><h4><?= $order->billing_company ?></h4></td>
        </tr>
        <tr>
            <td align="center"><h4><?= $from ?> – <?= $to ?></h4></td>
        </tr>
    </table>
</div>
<br>
<table class="table-bordered-report" width="100%"
    <?php

    if ($output != ReportFilterForm::VIEW_PDF) {
        echo ' border="1" ';
    }
    ?>
    >
    <?php
    if ($type_view != ReportFilterForm::VIEW_TYPE_SHORT) {
        ?>
        <tr>
            <td><b> #</b></td>
            <td><b>Inv. #</b></td>
            <td><b>Date</b></td>
            <td><b>Customer</b></td>
            <td><b>Item</b></td>
            <td><b>Description</b></td>
            <td><b>Quantity</b></td>
            <td><b>Price</b></td>
            <td><b>Line Total</b></td>
        </tr>
    <?php
    } else {
        ?>
        <tr>
            <td><b>#</b></td>
            <td><b>Customer</b></td>
            <td colspan="<?php echo $type_view == ReportFilterForm::VIEW_TYPE_SHORT ? 2 : 0; ?>"><b>Invoice Total</b>
            </td>
        </tr>

    <?php
    }
    $totalInvoice = 0;
    $count = 1;
    $customerName = '';
    $totalCustomer = 0;
    $countCustomer = 0;
    foreach ($data["detailed"] as $item) {
        if ($customerName != $item["source_name"]) {
            if ($count != 1) {
                ?>
                <tr>
                    <td class="text-right"><?php echo $type_view == ReportFilterForm::VIEW_TYPE_SHORT ? $countCustomer : ''; ?></td>
                    <td colspan="<?php echo $type_view != ReportFilterForm::VIEW_TYPE_SHORT ? 4 : 0; ?>">
                        <b><?= $customerName ?></b></td>
                    <?php
                    if ($type_view != ReportFilterForm::VIEW_TYPE_SHORT) {
                        ?>
                        <td colspan="2" class="text-right"><b>Subtotal:</b></td>
                    <?php
                    }
                    ?>
                    <td class="text-right"><b><?= Helper::formatNumber($totalCustomer) ?></b></td>
                    <td colspan="<?php echo $type_view != ReportFilterForm::VIEW_TYPE_SHORT ? 2 : 0; ?>"></td>
                </tr>
            <?
            }
            $customerName = $item["source_name"];
            $totalCustomer = 0;
            $countCustomer++;
        }
        if ($type_view != ReportFilterForm::VIEW_TYPE_SHORT) {
            ?>

            <tr>
                <td class="text-right"><?= $count ?></td>
                <td><?= $item["inv_number"] ?></td>
                <td><?= Helper::toAppDate($item["inv_date"]) ?></td>
                <td><?= $item["source_name"] ?></td>
                <td><?= $item["item_name"] ?></td>
                <td><?= $item["item_desc"] ?></td>
                <td class="text-right"><?= Helper::formatNumber($item["qty"],'') ?></td>
                <td class="text-right"><?= Helper::formatNumber($item["price_each"]) ?></td>
                <td class="text-right"><?= Helper::formatNumber($item["item_total"]) ?></td>
            </tr>
        <?php
        }
        $count++;
        $totalInvoice += $item["item_total"];
        $totalCustomer += $item["item_total"];
    }

    if ($count != 1) {
        ?>
        <tr>
            <td class="text-right"><?php echo $type_view == ReportFilterForm::VIEW_TYPE_SHORT ? $countCustomer : ''; ?></td>
            <td colspan="<?php echo $type_view != ReportFilterForm::VIEW_TYPE_SHORT ? 4 : 0; ?>">
                <b><?= $customerName ?></b></td>
            <?php
            if ($type_view != ReportFilterForm::VIEW_TYPE_SHORT) {
                ?>
                <td colspan="2" class="text-right"><b>Subtotal:</b></td>
            <?php
            } ?>
            <td class="text-right"><b><?= Helper::formatNumber($totalCustomer) ?></b></td>
            <td colspan="<?php echo $type_view != ReportFilterForm::VIEW_TYPE_SHORT ? 2 : 0; ?>"></td>
        </tr>
    <?
    }
    ?>
    <tr>
        <td></td>
        <td colspan="<?php echo $type_view != ReportFilterForm::VIEW_TYPE_SHORT ? 6 : 0; ?>"><b>Total:</b></td>
        <td class="text-right"><b><?= Helper::formatNumber($totalInvoice) ?></b></td>
        <td colspan="<?php echo $type_view != ReportFilterForm::VIEW_TYPE_SHORT ? 2 : 0; ?>"></td>
    </tr>
</table>
