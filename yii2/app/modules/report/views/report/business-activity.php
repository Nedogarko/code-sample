<?php
use common\components\Helper;
use app\models\ReportFilterForm;

?>
<div style="width:100%; " class="text-center">
    <h1><?=$this->context->titlePage;?></h1>
</div>
<div style="width:100%;  " class="text-center">
    <table width="100%" border="0">
        <tr>
            <td align="center"><h4><?= $order->billing_company ?></h4></td>
        </tr>
        <tr>
            <td align="center"><h4><?= $from ?> – <?= $to ?></h4></td>
        </tr>
    </table>
</div>
<br>
<table class="table-bordered-report" width="100%"
    <?php

    if ($output != ReportFilterForm::VIEW_PDF) {
        echo ' border="1" ';
    }
    ?>
    >
    <?php
    if ($type_view != ReportFilterForm::VIEW_TYPE_SHORT) {
        ?>
        <tr>
            <td><b> #</b></td>
            <td><b>Date</b></td>
            <td><b>Account</b></td>
            <td><b>Method</b></td>
            <td><b>Customer</b></td>
            <td><b>Founds In</b></td>
            <td><b>Founds Out</b></td>
            <td><b>Category</b></td>
            <td><b>Custom Tag</b></td>
        </tr>
    <?php
    } else {
        ?>
        <tr>
            <td></td>
            <td colspan="4"></td>
            <td><b>Founds In</b></td>
            <td><b>Founds Out</b></td>
        </tr>

    <?php
    }
    $totalInvoice = 0;
    $count = 1;
    $customerName = '';
    $totalCustomer = 0;
    $countCustomer = 0;
    foreach ($data["detailed"] as $item) {
        if ($type_view != ReportFilterForm::VIEW_TYPE_SHORT) {
            ?>

            <tr>
                <td class="text-right"><?= $count ?></td>
                <td><?= Helper::toAppDate($item["date"]) ?></td>
                <td><?= $item["account_name"] ?></td>
                <td><?= $item["method_id"] ?></td>
                <td><?= $item["source_name"] ?></td>
                <td class="text-right"><?= Helper::formatNumber($item["funds_in"]) ?></td>
                <td class="text-right"><?= Helper::formatNumber($item["funds_out"]) ?></td>
                <td class="text-right"><?= $item["category_name"] ?></td>
                <td><?= $item["custom_tag_name"] ?></td>
            </tr>
        <?php
        }
        $count++;
        $totalInvoice += $item["total"];
        $totalCustomer += $item["total"];
    }
?>
    <tr>
        <td></td>
        <td colspan="4"><b>Total:</b></td>
        <td class="text-right"><b><?= Helper::formatNumber($data["total_in"]) ?></b></td>
        <td class="text-right"><b><?= Helper::formatNumber($data["total_out"]) ?></b></td>
        <?php
            if ($type_view != ReportFilterForm::VIEW_TYPE_SHORT) {
        ?>
        <td colspan="2"></td>
        <?php
            }
        ?>
    </tr>
    <tr>
        <td></td>
        <td colspan="5"><b>Net Total:</b></td>
        <td class="text-right"><b><?= Helper::formatNumber($data["total_in"] - $data["total_out"]) ?></b></td>
        <?php
        if ($type_view != ReportFilterForm::VIEW_TYPE_SHORT) {
            ?>
            <td colspan="2"></td>
        <?php
        }
        ?>
    </tr>
</table>
