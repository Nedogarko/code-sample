<?php

namespace app\modules\report\components;

use common\models\backend\Category;
use common\components\Helper;
use common\models\frontend\AutoMileage;
use common\models\frontend\Invoice;

use Yii;

class Report extends \common\components\ActiveRecord
{
    public static $items = [
        [
            "name" => 'Income and Expense',
            'view_part' => 'income-and-expense'
        ],
        [
            "name" => 'Schedule C Worksheet',
            'view_part' => 'schedule-c-worksheet'
        ],
        [
            "name" => 'Reconciliation',
            'view_part' => 'reconciliation'
        ],
        [
            "name" => 'Business Activity',
            'view_part' => 'business-activity'
        ],
        [
            "name" => 'Loan Activity',
            'view_part' => 'loan-activity'
        ],
        [
            "name" => 'Mileage',
            'view_part' => 'mileage'
        ],
        [
            "name" => 'Auto Actual Expense',
            'view_part' => 'auto-actual-expense'
        ],
        [
            "name" => 'Invoice Aging',
            'view_part' => 'invoice-aging'
        ],
        [
            "name" => 'Invoice Activity',
            'view_part' => 'invoice-activity'
        ],
        [
            "name" => 'Invoice Item Activity',
            'view_part' => 'invoice-item-activity'
        ],
        [
            "name" => 'Invoice Sales Tax',
            'view_part' => 'invoice-sales-tax'
        ],
        [
            "name" => 'Asset Activity',
            'view_part' => 'asset-activity'
        ],
        [
            "name" => 'Balance Sheet',
            'view_part' => 'balance-sheet'
        ],
        [
            "name" => 'Trial Balance',
            'view_part' => 'trial-balance'
        ],
    ];

    public static function getAll()
    {
        return self::$items;
    }

    public static function getAppDb()
    {
        if (preg_match('/dbname=([A-Za-z]+)/', Category::getDb()->dsn, $matches)) {
            return $matches[1];
        } else {
            return '';
        }
    }

    public function getIncomeAndExpenseData($from, $to)
    {

        $to = Helper::toStorageDate($to);
        $from = Helper::toStorageDate($from);
        $userDB = Yii::$app->user->id;
        $appDB = self::getAppDb();
        $sql = "select name, sum(amount) as amount, " . $userDB . ".TRANSACTION.type as type from " . $userDB . ".TRANSACTION, " . $appDB . ".CATEGORY ";
        $sql .= "where date >= :from and date <= :to and category_id = " . $appDB . ".CATEGORY.id and category_id != '' and category_id != 'split' group by  name,category_id,TRANSACTION.type";
        $query = Yii::$app->db->createCommand($sql)
            ->bindValue(':from', $from)
            ->bindValue(':to', $to);
        $result = $query->queryAll();

        $c = [];
        $ci = [];
        $co = [];
        $lli = [];
        $lld = [];
        $ali = [];
        $ald = [];
        $resArr = [];
        foreach ($result as $value) {
            if ($value["type"] == "E") {
                $c[$value["name"]] = isset($c[$value["name"]]) ? ($c[$value["name"]] - $value["amount"]) : -$value["amount"];
                if ($value["amount"] > 0) {
                    $co[$value["name"]] = isset($co[$value["name"]]) ? ($co[$value["name"]] + $value["amount"]) : $value["amount"];
                } else {
                    $ci[$value["name"]] = isset($ci[$value["name"]]) ? ($ci[$value["name"]] - $value["amount"]) : -$value["amount"];
                }
            } else {

                $c[$value["name"]] = isset($c[$value["name"]]) ? ($c[$value["name"]] + $value["amount"]) : $value["amount"];
                if ($value["amount"] > 0) {
                    $ci[$value["name"]] = isset($ci[$value["name"]]) ? ($ci[$value["name"]] + $value["amount"]) : $value["amount"];
                } else {
                    $co[$value["name"]] = isset($co[$value["name"]]) ? ($co[$value["name"]] - $value["amount"]) : -$value["amount"];
                };
            }
        }

        $sql = "select " . $appDB . ".CATEGORY.name as name, amount, " . $userDB . ".TRANSACTION.type as type, " . $userDB . ".BANK_ACCOUNTS.acct_type as account_type from " . $userDB . ".TRANSACTION, " . $userDB . ".BANK_ACCOUNTS, " . $appDB . ".CATEGORY ";
        $sql .= "where date >= :from and date <= :to and category_id = " . $appDB . ".CATEGORY.id and category_id != '' and category_id != 'split' and " . $userDB . ".TRANSACTION.account_id = " . $userDB . ".BANK_ACCOUNTS.id";
        $query = Yii::$app->db->createCommand($sql)
            ->bindValue(':from', $from)
            ->bindValue(':to', $to);
        $result = $query->queryAll();

        foreach ($result as $value) {
            if ($value["account_type"] == "B" || $value["account_type"] == "C") {
                if ($value["type"] == "E") {
                    $c[$value["name"]] = isset($c[$value["name"]]) ? ($c[$value["name"]] - $value["amount"]) : -$value["amount"];
                    if ($value["amount"] > 0) {
                        $co[$value["name"]] = isset($co[$value["name"]]) ? ($co[$value["name"]] + $value["amount"]) : $value["amount"];
                    } else {
                        $ci[$value["name"]] = isset($ci[$value["name"]]) ? ($ci[$value["name"]] - $value["amount"]) : -$value["amount"];
                    }
                } else {
                    $c[$value["name"]] = isset($c[$value["name"]]) ? ($c[$value["name"]] + $value["amount"]) : $value["amount"];
                    if ($value["amount"] > 0) {
                        $ci[$value["name"]] = isset($ci[$value["name"]]) ? ($ci[$value["name"]] + $value["amount"]) : $value["amount"];
                    } else {
                        $co[$value["name"]] = isset($co[$value["name"]]) ? ($co[$value["name"]] - $value["amount"]) : -$value["amount"];
                    };
                }
            } else {
                $amount = ($value["account_type"] == "L" && ($value["type"] == "B" || $value["type"] == "A") ? (-$value["amount"]) : $value["amount"]);
                if ($value["account_type"] == "L") {
                    if ($amount > 0) {
                        $lli[$value["name"]] = isset($lli[$value["name"]]) ? ($lli[$value["name"]] + $value["amount"]) : $value["amount"];

                    } else {
                        $lld[$value["name"]] = isset($lld[$value["name"]]) ? ($lld[$value["name"]] - $value["amount"]) : -$value["amount"];
                    }
                } else {
                    if ($amount > 0) {
                        $ali[$value["name"]] = isset($ali[$value["name"]]) ? ($ali[$value["name"]] + $value["amount"]) : $value["amount"];
                    } else {
                        $ald[$value["name"]] = isset($ald[$value["name"]]) ? ($ald[$value["name"]] - $value["amount"]) : -$value["amount"];
                    }
                }
            }
        }


        $total_expenses =
            Helper::getIntegerValue($c["Advertising"]) + Helper::getIntegerValue($c["Bad debts from sales or services"]) +
            Helper::getIntegerValue($c["Car and truck expenses"]) + Helper::getIntegerValue($c["Commissions and fees"]) +
            Helper::getIntegerValue($c["Employee benefit programs"]) + Helper::getIntegerValue($c["Insurance"]) +
            Helper::getIntegerValue($c["Interest - mortgage"]) + Helper::getIntegerValue($c["Interest - other"]) +
            Helper::getIntegerValue($c["Legal and professional services"]) + Helper::getIntegerValue($c["Office expense"]) +
            Helper::getIntegerValue($c["Other expenses"]) + Helper::getIntegerValue($c["Payroll taxes and withholding"]) +
            Helper::getIntegerValue($c["Pension and profit-sharing plans"]) + Helper::getIntegerValue($c["Rent or lease - machinery and equipment"]) +
            Helper::getIntegerValue($c["Rent or lease - other business property"]) + Helper::getIntegerValue($c["Rent or lease - vehicles"]) +
            Helper::getIntegerValue($c["Repairs and maintenance"]) + Helper::getIntegerValue($c["Sales tax remitted"]) +
            Helper::getIntegerValue($c["Supplies"]) + Helper::getIntegerValue($c["Taxes and licenses"]) +
            Helper::getIntegerValue($c["Travel, meals, ent. - meals & entertainment"]) + Helper::getIntegerValue($c["Travel, meals, ent. - travel"]) +
            Helper::getIntegerValue($c["Utilities"]) + Helper::getIntegerValue($c["Wages"]);

        $sub_tot1 = (isset($c["Gross receipts or sales"]) ? $c["Gross receipts or sales"] : 0) + (isset($c["Returns and allowances"]) ? $c["Returns and allowances"] : 0);
        $sub_tot2 = $sub_tot1 + (isset($c["Purchases - goods to be sold"]) ? $c["Purchases - goods to be sold"] : 0);
        $income = $sub_tot2 + (isset($c["Sales tax collected"]) ? $c["Sales tax collected"] : 0);
        $income_div = $sub_tot1 / 100;

        $mileage = AutoMileage::find()
            ->andFilterWhere([
                '>= ',
                'date',
                Helper::toStorageDate($from)
            ])
            ->andFilterWhere([
                '<= ',
                'date',
                Helper::toStorageDate($to)
            ])
            ->sum('mileage');

        $resArr["c"] = $c;
        $resArr["ci"] = $ci;
        $resArr["co"] = $co;
        $resArr["lli"] = $lli;
        $resArr["lld"] = $lld;
        $resArr["ald"] = $ald;
        $resArr["ali"] = $ali;
        $resArr["total_expenses"] = $total_expenses;
        $resArr["sub_tot1"] = $sub_tot1;
        $resArr["sub_tot2"] = $sub_tot2;
        $resArr["income"] = $income;
        $resArr["income_div"] = $income_div;
        $resArr["mileage"] = $mileage;


        return $resArr;
    }

    public function getScheduleCWorksheet($from, $to)
    {

        $to = Helper::toStorageDate($to);
        $from = Helper::toStorageDate($from);
        $userDB = Yii::$app->user->id;
        $appDB = self::getAppDb();

        $o = [];
        $c = [];
        $ci = [];
        $co = [];
        $lli = [];
        $lld = [];
        $ali = [];
        $ald = [];
        $e = [];
        $resArr = [];

        $sql = "select name, amount, " . $userDB . ".TRANSACTION.type as type from " . $userDB . ".TRANSACTION, " . $userDB . ".CUSTOM_TAGS ";
        $sql .= "where date >= :from and date <= :to and custom_tag_id = " . $userDB . ".CUSTOM_TAGS.id and category_id = '56f479607979dc375e97aecc64dfbcfb' order by name";
        $query = Yii::$app->db->createCommand($sql)
            ->bindValue(':from', $from)
            ->bindValue(':to', $to);
        $result = $query->queryAll();

        $other_total = 0;
        foreach ($result as $value) {
            if ($value["type"] == "E") {
                $o[$value["name"]] = isset($o[$value["name"]]) ? ($o[$value["name"]] - $value["amount"]) : -$value["amount"];
                $other_total -= $value["amount"];
            } else {
                $o[$value["name"]] = isset($o[$value["name"]]) ? ($o[$value["name"]] + $value["amount"]) : +$value["amount"];
                $other_total += $value["amount"];
            }
        }

        $sql = "select " . $appDB . ".CATEGORY.name as name, amount, " . $userDB . ".TRANSACTION.type as type, " . $userDB . ".BANK_ACCOUNTS.acct_type as account_type from " . $userDB . ".TRANSACTION, " . $userDB . ".BANK_ACCOUNTS, " . $appDB . ".CATEGORY ";
        $sql .= "where date >= :from and date <= :to and category_id = " . $appDB . ".CATEGORY.id and category_id != '' and category_id != 'split' and " . $userDB . ".TRANSACTION.account_id = " . $userDB . ".BANK_ACCOUNTS.id";
        $query = Yii::$app->db->createCommand($sql)
            ->bindValue(':from', $from)
            ->bindValue(':to', $to);
        $result = $query->queryAll();

        foreach ($result as $value) {
            if ($value["account_type"] == "B" || $value["account_type"] == "C") {
                if ($value["type"] == "E") {
                    $c[$value["name"]] = isset($c[$value["name"]]) ? ($c[$value["name"]] - $value["amount"]) : -$value["amount"];
                    if ($value["amount"] > 0) {
                        $co[$value["name"]] = isset($co[$value["name"]]) ? ($co[$value["name"]] + $value["amount"]) : $value["amount"];
                    } else {
                        $ci[$value["name"]] = isset($ci[$value["name"]]) ? ($ci[$value["name"]] - $value["amount"]) : -$value["amount"];
                    }
                } else {
                    $c[$value["name"]] = isset($c[$value["name"]]) ? ($c[$value["name"]] + $value["amount"]) : $value["amount"];
                    if ($value["amount"] > 0) {
                        $ci[$value["name"]] = isset($ci[$value["name"]]) ? ($ci[$value["name"]] + $value["amount"]) : $value["amount"];
                    } else {
                        $co[$value["name"]] = isset($co[$value["name"]]) ? ($co[$value["name"]] - $value["amount"]) : -$value["amount"];
                    };
                }
            } else {
                $amount = ($value["account_type"] == "L" && ($value["type"] == "B" || $value["type"] == "A") ? (-$value["amount"]) : $value["amount"]);
                if ($value["account_type"] == "L") {
                    if ($amount > 0) {
                        $lli[$value["name"]] = isset($lli[$value["name"]]) ? ($lli[$value["name"]] + $value["amount"]) : $value["amount"];

                    } else {
                        $lld[$value["name"]] = isset($lld[$value["name"]]) ? ($lld[$value["name"]] - $value["amount"]) : -$value["amount"];
                    }
                } else {
                    if ($amount > 0) {
                        $ali[$value["name"]] = isset($ali[$value["name"]]) ? ($ali[$value["name"]] + $value["amount"]) : $value["amount"];
                    } else {
                        $ald[$value["name"]] = isset($ald[$value["name"]]) ? ($ald[$value["name"]] - $value["amount"]) : -$value["amount"];
                    }
                }
            }
        }

        $sql = "select sum(" . $userDB . ".AUTO_MILEAGE.mileage*" . $appDB . ".AUTO.cents_per_mile) as m_exp  from " . $userDB . ".AUTO_MILEAGE, " . $appDB . ".AUTO ";
        $sql .= "where date >= :from and date <= :to and " . $userDB . ".AUTO_MILEAGE.date>=" . $appDB . ".AUTO.start_date and " . $userDB . ".AUTO_MILEAGE.date<=" . $appDB . ".AUTO.end_date GROUP BY " . $appDB . ".AUTO.cents_per_mile";
        $query = Yii::$app->db->createCommand($sql)
            ->bindValue(':from', $from)
            ->bindValue(':to', $to);
        $result = $query->queryAll();
        $total_mileage_deduction = 0;

        foreach ($result as $value) {
            $total_mileage_deduction += $value;
        }

        $sql = "select name, id  from " . $appDB . ".CATEGORY";
        $query = Yii::$app->db->createCommand($sql);
        $result = $query->queryAll();

        foreach ($result as $value) {
            $e[$value["name"]] = $value["id"];
        }


        $total_expenses =
            Helper::getIntegerValue($c["Advertising"]) + Helper::getIntegerValue($c["Bad debts from sales or services"]) +
            Helper::getIntegerValue($c["Commissions and fees"]) + Helper::getIntegerValue($c["Employee benefit programs"]) + Helper::getIntegerValue($c["Insurance"]) +
            Helper::getIntegerValue($c["Interest - mortgage"]) + Helper::getIntegerValue($c["Interest - other"]) + Helper::getIntegerValue($c["Legal and professional services"]) +
            Helper::getIntegerValue($c["Office expense"]) + Helper::getIntegerValue($c["Other expenses"]) +
            Helper::getIntegerValue($c["Pension and profit-sharing plans"]) + Helper::getIntegerValue($c["Rent or lease - machinery and equipment"]) +
            Helper::getIntegerValue($c["Rent or lease - other business property"]) + Helper::getIntegerValue($c["Repairs and maintenance"]) +
            Helper::getIntegerValue($c["Supplies"]) + Helper::getIntegerValue($c["Taxes and licenses"]) + .5 * Helper::getIntegerValue($c["Travel, meals, ent. - meals & entertainment"]) +
            Helper::getIntegerValue($c["Payroll taxes and withholding"]) + Helper::getIntegerValue($c["Sales tax remitted"]) +
            Helper::getIntegerValue($c["Travel, meals, ent. - travel"]) + Helper::getIntegerValue($c["Utilities"]) +
            Helper::getIntegerValue($c["Wages"]) - $total_mileage_deduction;


        $resArr["e"] = $e;
        $resArr["o"] = $o;
        $resArr["c"] = $c;
        $resArr["ci"] = $ci;
        $resArr["co"] = $co;
        $resArr["lli"] = $lli;
        $resArr["lld"] = $lld;
        $resArr["ald"] = $ald;
        $resArr["ali"] = $ali;
        $resArr["total_expenses"] = $total_expenses;
        $resArr["other_total"] = $other_total;
        $resArr["total_mileage_deduction"] = $total_mileage_deduction;


        return $resArr;
    }


    public function getMileage($from, $to, $auto = null)
    {
        $to = Helper::toStorageDate($to);
        $from = Helper::toStorageDate($from);
        $userDB = Yii::$app->user->id;

        $sql = ' select a.id as id,date,auto_id,auto_name,odo_start,odo_end,mileage,note from ' . $userDB . '.AUTO_MILEAGE am, ' . $userDB . '.AUTO a';
        $sql .= ' where date >= :from and date <= :to and am.auto_id = a.id';
        if (isset($auto)) {
            $sql .= ' and auto_id in ( \'' . $auto . '\')';
        }

        $query = Yii::$app->db->createCommand($sql)
            ->bindValue(':from', $from)
            ->bindValue(':to', $to);

        $result["detailed"] = $query->queryAll();

        $sql = ' select auto_name, sum(mileage) as mileage from ' . $userDB . '.AUTO_MILEAGE am, ' . $userDB . '.AUTO a';
        $sql .= ' where date >= :from and date <= :to and am.auto_id = a.id';
        if (isset($auto)) {
            $sql .= ' and auto_id in ( \'' . $auto . '\')';
        }
        $sql .= ' GROUP BY auto_name';

        $query = Yii::$app->db->createCommand($sql)
            ->bindValue(':from', $from)
            ->bindValue(':to', $to);

        $result["short"] = $query->queryAll();

        return $result;
    }

    public function getAutoActualExpense($from, $to, $auto = null)
    {
        $to = Helper::toStorageDate($to);
        $from = Helper::toStorageDate($from);
        $userDB = Yii::$app->user->id;
        $appDB = self::getAppDb();

        $sql = ' select tr.*, a.auto_name,ct.name as custom_tag_name, c.name as category_name, s.name source_name from  ' . $userDB . '.TRANSACTION tr ';
        $sql .= ' LEFT JOIN ' . $userDB . '.CUSTOM_TAGS ct ON tr.custom_tag_id = ct.id ';
        $sql .= ' LEFT JOIN ' . $userDB . '.SOURCE s ON tr.source_id = s.id ';
        $sql .= ' LEFT JOIN ' . $userDB . '.AUTO a ON tr.auto_id = a.id ';
        $sql .= ' LEFT JOIN ' . $appDB . '.CATEGORY c ON tr.category_id =c.id';
        $sql .= ' where (tr.category_id = \'d39dfaaec3eaa3785e8a456ede076832\' or tr.category_id =\'fa2f21f556984750e5954a9da01315b7\') ';
        $sql .= ' and tr.date >= :from and tr.date <= :to ';


        if (isset($auto)) {
            $sql .= ' and auto_id in ( \'' . $auto . '\')';
        }
        $query = Yii::$app->db->createCommand($sql)
            ->bindValue(':from', $from)
            ->bindValue(':to', $to);
        $results = $query->queryAll();

        $total_in = 0;
        $total_out = 0;
        $auto_totals = [];
        $resArr = [];
        $res = [];

        foreach ($results as $result) {
            $resArr["auto_name"] = $result["auto_name"];
            $resArr["category_name"] = (($result["split"] == 'Y') ? 'Split' : $result["category_name"]);
            $resArr["amount"] = $result["amount"];
            $resArr["method_id"] = $result["method_id"];
            $resArr["memo"] = $result["memo"];
            $resArr["date"] = $result["date"];
            $resArr["master_id"] = $result["master_id"];
            $resArr["check_number"] = $result["check_number"];
            $resArr["reconciled"] = $result["reconciled"];
            $resArr["source_id"] = $result["source_id"];
            $resArr["source_name"] = $result["source_name"];
            $resArr["custom_tag_name"] = (($result["split"] == 'Y') ? 'Split' : $result["custom_tag_name"]);
            $resArr["auto_name"] = $result["auto_name"];

            if ($result["type"] == "I") {
                $resArr["funds_in"] = $result["amount"];
                $resArr["funds_out"] = "";
                $total_in += $result["amount"];
                $auto_totals[$result["auto_name"]]["in"] = isset($auto_totals[$result["auto_name"]]["in"]) ? ($auto_totals[$result["auto_name"]]["in"] + $result["amount"]) : $result["amount"];
            } elseif ($type = "E") {
                $resArr["funds_in"] = "";
                $resArr["funds_out"] = $result["amount"];
                $total_out += $result["amount"];
                $auto_totals[$result["auto_name"]]["out"] = isset($auto_totals[$result["auto_name"]]["out"]) ? ($auto_totals[$result["auto_name"]]["out"] + $result["amount"]) : $result["amount"];
            }
            $res[] = $resArr;
        }

        $result["short"] = $auto_totals;
        $result["detailed"] = $res;
        $result["total_out"] = $total_out;
        $result["total_in"] = $total_in;

        return $result;
    }

    public function getInvoiceActivity($from, $to, $source = null)
    {
        $to = Helper::toStorageDate($to);
        $from = Helper::toStorageDate($from);
        $userDB = Yii::$app->user->id;

        $sql = ' SELECT ';
        $sql .= ' i.*, s.name as source_name, ';
        $sql .= ' round(sum(ili.price_each * ili.qty),2) as total_sum_without_tax, ';
        $sql .= ' sum(CASE  WHEN ili.tax = \'T\' THEN  round((ili.price_each * ili.qty*i.tax_rate)/100,2) ELSE 0 END)  as total_sum_tax, ';
        $sql .= ' sum(CASE  WHEN ili.tax = \'T\' THEN  round((ili.price_each * ili.qty*(100+i.tax_rate))/100,2) ELSE round(ili.price_each * ili.qty,2) END)  as total_sum_with_tax ';
        $sql .= ' FROM  ' . $userDB . '.INVOICES i ';
        $sql .= ' LEFT JOIN ' . $userDB . '.INVOICE_LINE_ITEMS ili ON i.id = ili.invoice_id ';
        $sql .= ' LEFT JOIN ' . $userDB . '.SOURCE s ON i.source_id = s.id ';
        $sql .= ' WHERE i.inv_date >= :from AND i.inv_date <= :to ';

        if (isset($source)) {
            $sql .= ' and i.source_id in ( \'' . $source . '\')';
        }
        $sql .= ' GROUP BY i.id ';
        $sql .= ' ORDER BY s.name, i.inv_date ';

        $query = Yii::$app->db->createCommand($sql)
            ->bindValue(':from', $from)
            ->bindValue(':to', $to);
        $results = $query->queryAll();

        $resArr = [];
        $res = [];

        foreach ($results as $result) {
            $resArr["inv_number"] = $result["inv_number"];
            $resArr["source_id"] = $result["source_id"];
            $resArr["source_name"] = $result["source_name"];
            $resArr["due_date"] = $result["due_date"];
            $resArr["inv_date"] = $result["inv_date"];
            $resArr["terms"] = Invoice::getTerms($result["terms"]);
            $resArr["total"] = $result["total_sum_tax"];
            $resArr["status"] = Invoice::getStatus($result["status"]);
            $totalPayment = Invoice::getTotalSumPayment($result["id"]);
            $resArr["amount_due"] = $result["total_sum_tax"] - $totalPayment;
            $resArr["payment"] = $totalPayment;
            $res[] = $resArr;
        }

        $result["detailed"] = $res;

        return $result;
    }

    public function getInvoiceItemActivity($from, $to, $source = null, $invoice_item = null)
    {
        $to = Helper::toStorageDate($to);
        $from = Helper::toStorageDate($from);
        $userDB = Yii::$app->user->id;

        $sql = ' SELECT ';
        $sql .= ' i.*, s.name as source_name,  ii.name as item_name, ili.qty, ili.price_each, ili.desc as item_desc ';
        $sql .= ' FROM  ' . $userDB . '.INVOICES i ';
        $sql .= ' LEFT JOIN ' . $userDB . '.INVOICE_LINE_ITEMS ili ON i.id = ili.invoice_id ';
        $sql .= ' LEFT JOIN ' . $userDB . '.SOURCE s ON i.source_id = s.id ';
        $sql .= ' LEFT JOIN ' . $userDB . '.INVOICE_ITEMS ii ON ili.income_item_id = ii.id ';
        $sql .= ' WHERE i.inv_date >= :from AND i.inv_date <= :to ';

        if (isset($source)) {
            $sql .= ' and i.source_id in ( \'' . $source . '\')';
        }

        if (isset($invoice_item)) {
            $sql .= ' and ili.income_item_id in ( \'' . $invoice_item . '\')';
        }
        $sql .= ' ORDER BY s.name, i.inv_date,ii.name ';

        $query = Yii::$app->db->createCommand($sql)
            ->bindValue(':from', $from)
            ->bindValue(':to', $to);
        $results = $query->queryAll();

        $resArr = [];
        $res = [];

        foreach ($results as $result) {
            $resArr["inv_number"] = $result["inv_number"];
            $resArr["source_name"] = $result["source_name"];
            $resArr["inv_date"] = $result["inv_date"];
            $resArr["price_each"] = $result["price_each"];
            $resArr["item_name"] = $result["item_name"];
            $resArr["item_desc"] = $result["item_desc"];
            $resArr["qty"] = $result["qty"];
            $resArr["item_total"] = $result["price_each"] * $result["qty"];

            $res[] = $resArr;
        }

        $result["detailed"] = $res;

        return $result;
    }

    public function getInvoiceSalesTax($from, $to, $invoice_item = null, $sales_tax =null)
    {
        $to = Helper::toStorageDate($to);
        $from = Helper::toStorageDate($from);
        $userDB = Yii::$app->user->id;

        $sql = ' SELECT ';
        $sql .= ' i.*,  ii.name as item_name, ili.qty, ili.price_each, ili.desc as item_desc ';
        $sql .= ' FROM  ' . $userDB . '.INVOICES i ';
        $sql .= ' LEFT JOIN ' . $userDB . '.INVOICE_LINE_ITEMS ili ON i.id = ili.invoice_id ';
        $sql .= ' LEFT JOIN ' . $userDB . '.INVOICE_ITEMS ii ON ili.income_item_id = ii.id ';
        $sql .= ' WHERE i.inv_date >= :from AND i.inv_date <= :to ';

        if (isset($source)) {
            $sql .= ' and i.source_id in ( \'' . $source . '\')';
        }

        if (isset($invoice_item)) {
            $sql .= ' and ili.income_item_id in ( \'' . $invoice_item . '\')';
        }
        $sql .= ' ORDER BY i.inv_date,ii.name ';

        $query = Yii::$app->db->createCommand($sql)
            ->bindValue(':from', $from)
            ->bindValue(':to', $to);
        $results = $query->queryAll();

        $resArr = [];
        $res = [];

        foreach ($results as $result) {
            $resArr["id"] = $result["id"];
            $resArr["inv_number"] = $result["inv_number"];
            $resArr["tax_rate"] = $result["tax_rate"];
            $resArr["inv_date"] = $result["inv_date"];
            $resArr["price_each"] = $result["price_each"];
            $resArr["item_name"] = $result["item_name"];
            $resArr["item_desc"] = $result["item_desc"];
            $resArr["qty"] = $result["qty"];
            $resArr["item_total"] = $result["price_each"] * $result["qty"];
            $resArr["item_total_tax"] = $resArr["item_total"] * $result["tax_rate"]/100;

            $res[] = $resArr;
        }

        $result["detailed"] = $res;

        return $result;
    }

    public function getInvoiceAging($from, $to, $source = null)
    {
        $to = Helper::toStorageDate($to);
        $from = Helper::toStorageDate($from);
        $userDB = Yii::$app->user->id;

        $sql = ' select DATEDIFF(now(),inv_date) as aging, i.*,  s.name source_name, ';
        $sql .= ' round(sum(ili.price_each * ili.qty),2) as total_sum_without_tax ,';
        $sql .= ' sum(CASE  WHEN ili.tax = \'T\' THEN  round((ili.price_each * ili.qty*(100+i.tax_rate))/100,2) ELSE round(ili.price_each * ili.qty,2) END)  as total_sum_with_tax ';
        $sql .= ' FROM  ' . $userDB . '.INVOICES i ';
        $sql .= ' LEFT JOIN ' . $userDB . '.SOURCE s ON i.source_id = s.id ';
        $sql .= ' LEFT JOIN ' . $userDB . '.INVOICE_LINE_ITEMS ili ON i.id = ili.invoice_id ';
        $sql .= ' WHERE i.inv_date >= :from and i.inv_date <= :to and status = 1 ';

        if (isset($source)) {
            $sql .= ' and source_id in ( \'' . $source . '\')';
        }

        $sql .= ' GROUP BY i.id ';
        $sql .= ' ORDER BY s.name, i.inv_date ';

        $query = Yii::$app->db->createCommand($sql)
            ->bindValue(':from', $from)
            ->bindValue(':to', $to);
        $results = $query->queryAll();

        $resArr = [];
        $res = [];

        foreach ($results as $result) {
            $resArr["inv_number"] = $result["inv_number"];
            $resArr["tax_rate"] = $result["tax_rate"];
            $resArr["inv_date"] = $result["inv_date"];
            $resArr["amount"] = $result["amount"];
            $resArr["due_date"] = $result["due_date"];
            $resArr["terms"] = Invoice::getTerms($result["terms"]);
            $resArr["method_id"] = $result["method_id"];
            $resArr["memo"] = $result["memo"];
            $resArr["date"] = $result["date"];
            $resArr["master_id"] = $result["master_id"];
            $resArr["check_number"] = $result["check_number"];
            $resArr["source_id"] = $result["source_id"];
            $resArr["source_name"] = $result["source_name"];
            $totalPayment = Invoice::getTotalSumPayment($result["id"]);
            $resArr["amount_due"] = $result["total_sum_with_tax"] - $totalPayment;
            $resArr["total_payment"] = $totalPayment;

            $resArr["aging"] = $result["aging"]>0?$result["aging"]:0;

            $res[] = $resArr;
        }

        $result["detailed"] = $res;

        return $result;
    }

    public function getBusinessActivity($from, $to, $type = null, $account = null, $source = null, $amount_from = null, $amount_to = null, $custom_tag = null, $category = null)
    {
        $to = Helper::toStorageDate($to);
        $from = Helper::toStorageDate($from);
        $userDB = Yii::$app->user->id;
        $appDB = self::getAppDb();

        $sql = ' select tr.*,ba.acct_type,  ba.name as account_name, ct.name as custom_tag_name, c.name as category_name, s.name source_name from  ' . $userDB . '.TRANSACTION tr ';
        $sql .= ' LEFT JOIN ' . $userDB . '.CUSTOM_TAGS ct ON tr.custom_tag_id = ct.id ';
        $sql .= ' LEFT JOIN ' . $userDB . '.SOURCE s ON tr.source_id = s.id ';
        $sql .= ' LEFT JOIN ' . $userDB . '.BANK_ACCOUNTS ba ON tr.account_id = ba.id ';
        $sql .= ' LEFT JOIN ' . $appDB . '.CATEGORY c ON tr.category_id =c.id';
        $sql .= ' where (tr.category_id = \'d39dfaaec3eaa3785e8a456ede076832\' or tr.category_id =\'fa2f21f556984750e5954a9da01315b7\') ';
        $sql .= ' and tr.date >= :from and tr.date <= :to ';


        if (isset($source)) {
            $sql .= ' and tr.source_id in ( \'' . $source . '\')';
        }
        if (isset($account)) {
            $sql .= ' and tr.account_id in ( \'' . $account . '\')';
        }
        if (isset($custom_tag)) {
            $sql .= ' and tr.custom_tag_id in ( \'' . $custom_tag . '\')';
        }

        if (isset($category)) {
            $sql .= ' and tr.category_id in ( \'' . $category . '\')';
        }

        if (isset($type)) {
            $sql .= ' and tr.type in ( \'' . $type . '\')';
        }

        if (isset($amount_from)) {
            $sql .= ' and tr.amount>='.$amount_from;
        }

        if (isset($amount_to)) {
            $sql .= ' and tr.amount<='.$amount_to;
        }


        $query = Yii::$app->db->createCommand($sql)
            ->bindValue(':from', $from)
            ->bindValue(':to', $to);
        $results = $query->queryAll();

        $total_in = 0;
        $total_out = 0;
        $auto_totals = [];
        $resArr = [];
        $res = [];

        foreach ($results as $result) {
            $resArr["auto_name"] = $result["auto_name"];
            $resArr["category_name"] = (($result["split"] == 'Y') ? 'Split' : $result["category_name"]);
            $resArr["amount"] = $result["amount"];
            $resArr["method_id"] = $result["method_id"];
            $resArr["memo"] = $result["memo"];
            $resArr["date"] = $result["date"];
            $resArr["reconciled"] = $result["reconciled"];
            $resArr["source_id"] = $result["source_id"];
            $resArr["source_name"] = $result["source_name"];
            $resArr["custom_tag_name"] = (($result["split"] == 'Y') ? 'Split' : $result["custom_tag_name"]);
            $resArr["account_name"] = $result["account_name"];

            $amount = $result["amount"];
            $type = $result["type"];
            $amount = ($result["acct_type"]=="L"&&($type=="B"||$type=="A")?(-$amount):$amount);

            $funds_out = 0;
            $funds_in = 0;
            if( $type == "I" && $amount >= 0){
                $type = "I";
                $resArr["funds_in"] = $amount;
                $resArr["funds_out"] = 0;
                $total_in += $amount;
            } elseif( $type == "E" && $amount >= 0){
                $type = "E";
                $resArr["funds_out"] = $amount;
                $resArr["funds_in"] = 0;
                $total_out += $amount;
            } elseif( $type == "I" && ($amount < 0)){
                $type = "E";
                $amount = -$amount;
                $resArr["funds_out"] = $amount;
                $resArr["funds_in"] = 0;
                $total_out += ($amount);
            } elseif( $type == "E" && ($amount < 0)){
                $type = "I";
                $amount = -$amount;
                $resArr["funds_in"] = $amount;
                $resArr["funds_out"] = 0;
                $total_in += ($amount);
            } elseif( $type == "T" ){
                if ($amount < 0 ){
                    $type = "E";
                    $amount = -$amount;
                    $resArr["funds_out"] = $amount;
                    $resArr["funds_in"] = 0;
                    $total_out += ($amount);
                } else {
                    $type = "I";
                    $resArr["funds_in"] = $amount;
                    $resArr["funds_out"] = 0;
                    $total_in += $amount;
                }
            } elseif( $type == "B" ){
                if ($amount < 0 ){
                    $type = "E";
                    $amount = -$amount;
                    $resArr["funds_out"] = $amount;
                    $resArr["funds_in"] = 0;
                    $total_out += ($amount);
                } else {
                    $type = "I";
                    $resArr["funds_in"] = $amount;
                    $resArr["funds_out"] = 0;
                    $total_in += $amount;
                }
            } elseif( $type == "A" ){
                if ($amount < 0 ){
                    $type = "E";
                    $amount = -$amount;
                    $resArr["funds_out"] = $amount;
                    $resArr["funds_in"] = 0;
                    $total_out += ($amount);
                } else {
                    $type = "I";
                    $resArr["funds_in"] = $amount;
                    $resArr["funds_out"] = 0;
                    $total_in += $amount;
                }
            } elseif( $type == "D" ){
                if ($amount < 0 ){
                    $type = "E";
                    $amount = -$amount;
                    $resArr["funds_out"] = $amount;
                    $resArr["funds_in"] = 0;
                    $total_out += ($amount);
                } else {
                    $type = "I";
                    $resArr["funds_in"] = $amount;
                    $resArr["funds_out"] = 0;
                    $total_in += $amount;
                }
            }
            $res[] = $resArr;
        }

        $result["short"] = $auto_totals;
        $result["detailed"] = $res;
        $result["total_out"] = $total_out;
        $result["total_in"] = $total_in;

        return $result;
    }

    public function getReconciliation($reconciled)
    {
        $userDB = Yii::$app->user->id;

        $sql='SELECT ba.name, r.ending_date ';
        $sql .= ' FROM  ' . $userDB . '.RECONCILE r ';
        $sql .= ' LEFT JOIN ' . $userDB . '.BANK_ACCOUNTS ba ON r.account_id = ba.id ';
        $sql .= ' WHERE r.id = :reconciled ';
        $query = Yii::$app->db->createCommand($sql)
            ->bindValue(':reconciled', $reconciled);
        $results = $query->queryAll();

        $accountName=$results[0]["name"];
        $endingDate = $results[0]["ending_date"];

        $sql = ' select DATEDIFF(now(),inv_date) as aging, i.*,  s.name source_name, ';
        $sql .= ' round(sum(ili.price_each * ili.qty),2) as total_sum_without_tax ,';
        $sql .= ' sum(CASE  WHEN ili.tax = \'T\' THEN  round((ili.price_each * ili.qty*(100+i.tax_rate))/100,2) ELSE round(ili.price_each * ili.qty,2) END)  as total_sum_with_tax ';
        $sql .= ' FROM  ' . $userDB . '.INVOICES i ';
        $sql .= ' LEFT JOIN ' . $userDB . '.SOURCE s ON i.source_id = s.id ';
        $sql .= ' LEFT JOIN ' . $userDB . '.INVOICE_LINE_ITEMS ili ON i.id = ili.invoice_id ';
        $sql .= ' WHERE i.inv_date >= :from and i.inv_date <= :to and status = 1 ';

        if (isset($source)) {
            $sql .= ' and source_id in ( \'' . $source . '\')';
        }

        $sql .= ' GROUP BY i.id ';
        $sql .= ' ORDER BY s.name, i.inv_date ';

        $query = Yii::$app->db->createCommand($sql)
            ->bindValue(':from', $from)
            ->bindValue(':to', $to);
        $results = $query->queryAll();

        $resArr = [];
        $res = [];

        foreach ($results as $result) {
            $resArr["inv_number"] = $result["inv_number"];
            $resArr["tax_rate"] = $result["tax_rate"];
            $resArr["inv_date"] = $result["inv_date"];
            $resArr["amount"] = $result["amount"];
            $resArr["due_date"] = $result["due_date"];
            $resArr["terms"] = Invoice::getTerms($result["terms"]);
            $resArr["method_id"] = $result["method_id"];
            $resArr["memo"] = $result["memo"];
            $resArr["date"] = $result["date"];
            $resArr["master_id"] = $result["master_id"];
            $resArr["check_number"] = $result["check_number"];
            $resArr["source_id"] = $result["source_id"];
            $resArr["source_name"] = $result["source_name"];
            $totalPayment = Invoice::getTotalSumPayment($result["id"]);
            $resArr["amount_due"] = $result["total_sum_with_tax"] - $totalPayment;
            $resArr["total_payment"] = $totalPayment;

            $resArr["aging"] = $result["aging"]>0?$result["aging"]:0;

            $res[] = $resArr;
        }

        $result["detailed"] = $res;
        $result["endingDate"] = $endingDate;
        $result["accountName"] = $accountName;

        return $result;
    }

}