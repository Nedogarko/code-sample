<p>
    Keep your business and personal accounts separate.  Establish separate bank and credit card accounts that are strictly used for business transactions.
</p>
<p>
    If your friendly IRS agent comes to visit you, they will want to see your business treated like a business.
    Our operations guru (you know, Ms. Type A) assisted in an audit for a business associate who had personal and business expenses mixed,
    an instant "Red Flag" for the IRS.<br><br>
    It was a simple, easy audit that had just turned into hundreds of hours of work, years to resolve, and will probably
    end with more taxes to pay, plus interest and penalty.
</p>