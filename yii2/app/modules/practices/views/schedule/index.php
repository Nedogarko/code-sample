<p>
    Set aside time each day or each week to enter your business transactions into <?php echo  Yii::$app->params['companyName']; ?>.
    (Don't get a cluttered, over-stuffed Shoebox.)  Each time you log-in to <?php echo  Yii::$app->params['companyName']; ?> have your physical receipts, checks and business
    mileage ready to enter.  Make your scheduled time as efficient as possible by entering each receipt as a separate transaction ? <br>Get in and out
    of <?php echo  Yii::$app->params['companyName']; ?> quickly and move on to revenue generating activities.
</p>
<p>
    In 2002, a Denver, CO based small business services company went out of business because they grew too quickly and did not keep track of their records.
    How? They grew their business by 250% in 2001 but did not set aside sufficient funds to pay their tax liability.<br><br>
    Track your records through <?php echo  Yii::$app->params['companyName']; ?> and you won't get caught by surprise.
</p>
