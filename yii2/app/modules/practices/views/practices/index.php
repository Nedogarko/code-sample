<?php
    use yii\helpers\Url;
?>
<p>
    When developing <?php echo  Yii::$app->params['companyName']; ?>, we took <strong>tried and true</strong> business practices from our accounting
    and small business partners across the nation - we call these practices our <strong> Best Practices?</strong>
    <br>
    If you are not already doing so, follow the practices below to make your business that much more successful.
</p>
<p>
    <strong>Set yourself up for success!</strong><br>
    <blockquote>
        <a href="<?= Url::to([$this->context->getHomeUrl().'/business']) ?>">Treat your business like a business</a><br>
        <a href="<?= Url::to([$this->context->getHomeUrl().'/principle']) ?>">Keep'em separated</a><br>
        <a href="<?= Url::to([$this->context->getHomeUrl().'/schedule']) ?>">Stick to a schedule</a><br>
        <a href="<?= Url::to([$this->context->getHomeUrl().'/leakage']) ?>">Stop the leakage</a><br>
        <a href="<?= Url::to([$this->context->getHomeUrl().'/advised']) ?>">Establish a relationship with a tax advisor</a>
    </blockquote>
</p>