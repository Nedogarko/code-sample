<?php
namespace app\modules\practices\components;

use Yii;

/**
 * Controller
 */
class Controller extends \app\components\Controller
{
    public $relatedLinks = [
        '/practices' => 'Best practices',
        '/practices/business' => 'Business 101',
        '/practices/principle' => 'Separation Principle',
        '/practices/schedule' => 'Stick to a schedule',
        '/practices/leakage' => 'Stop the Leakage',
        '/practices/advised' => 'Be Well Advised',
    ];


}
