<?php
namespace app\modules\practices\controllers;

use Yii;
use app\modules\practices\components\Controller;

/**
 * PrincipleController
 */
class PrincipleController extends Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }

}
