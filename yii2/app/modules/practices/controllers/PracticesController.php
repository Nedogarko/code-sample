<?php
namespace app\modules\practices\controllers;

use Yii;
use app\modules\practices\components\Controller;

/**
 * PracticesController
 */
class PracticesController extends Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }

}
