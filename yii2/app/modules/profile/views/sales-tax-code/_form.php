<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\widgets\Pjax;
use common\models\frontend\TaxTable;

/* @var $this yii\web\View */
/* @var $model common\models\frontend\TaxTable */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="col-lg-12">
    <?php Pjax::begin(); ?>
    <div>
        <?php $form = ActiveForm::begin([
            'options' => [
                'class' => 'form-horizontal',
                'data-pjax' => true,
                'id' => 'tax-codes-form'
            ],
            'fieldConfig' => [
                'template' => "<div class=\"col-md-3\">{label}</div>\n<div class=\"col-md-9\">{input}</div>\n<div class=\"col-md-12\">{error}</div>",
            ],
        ]); ?>

        <?= $form->field($model, 'name')
            ->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'percent')
            ->textInput(['type' => 'number', 'step' => 0.000001]) ?>

        <?= $form->field($model, 'def',
            ['template' => "<div class=\"col-md-3\">{label}</div>\n<div class=\"col-md-9\">{hint}\n{input}\n{beginLabel}{endLabel}</div>\n<div class=\"col-md-12\">{error}</div>",])
            ->checkbox(['value' => TaxTable::DEFAULT_TAX], false) ?>

        <div class="form-group modal-footer">
            <?= Html::submitButton('<span class="glyphicon glyphicon-ok-circle"></span>' . ($model->isNewRecord
                    ? ' Add'
                    : ' Update'), [
                'class' => 'btn btn-success'
            ]) ?>
            <button type="button" class="btn btn-success" data-dismiss="modal"><span
                    class="glyphicon glyphicon-ban-circle"></span> Cancel
            </button>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
    <?php Pjax::end(); ?>
</div>
