<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $order common\models\backend\Order */
/* @var $package common\models\backend\Package */
/* @var $subscription common\models\backend\Subscription */
/* @var $customer \Stripe\Customer */

$this->title = 'My Services and Subscriptions';
?>
<?php if ($customer->deleted) { ?>
    <div class="row">
        <div class="col-2">
            Customer deleted
        </div>
    </div>
<?php } else { ?>
    <div class="row" >
        <div class="col-md-2 text-right">
            Package:
        </div>
        <div class="col-md-2 ">
            <?= Html::encode($package->name) ?>
        </div>
    </div>
    <div class="row" style="padding-bottom:20px;">
        <div class="col-md-2 text-right">
            Subscription:
        </div>
        <div class="col-md-2 ">
            <?= Html::encode($subscription->name) ?>
        </div>
    </div>
    <div class="row" >
        <div class="col-md-4 text-center" >
            <hr style="margin: 0px;">
        </div>
    </div>
    <div class="row">
        <div class="col-md-4 text-center" >
            <h4>Services</h4>
        </div>
    </div>
    <div class="row" >
        <div class="col-md-4 text-center" >
        <hr style="margin-top: 0px;">
        </div>
    </div>
    <div class="row" style="padding-bottom:10px;">
        <div class="col-md-4 text-center">
            <?php foreach ($package->getServices()
                ->all() as $service
            ) { ?>
                        <?= Html::encode($service->name) ?>
                <br>
            <?php } ?>
        </div>
    </div>

    <?php if (count($customer->subscriptions->all())) { ?>
        <div class="row">
            <div class="col-md-4 text-center">
                <?= Html::beginForm(['cancel-subscription']) . Html::submitButton('Cancel Subscription', [
                    'class' => 'btn btn-danger',
                    'data-confirm' => 'Are you sure you want to cancel active subscription'
                ]) . Html::endForm() ?>
            </div>
        </div>
    <?php } ?>
<?php } ?>
