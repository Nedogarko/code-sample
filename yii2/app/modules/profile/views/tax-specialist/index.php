<?php
    use yii\helpers\Html;
    if (($auth_accountant!='') || ($not_auth_accountant!='')) {
        ?>
        <h4>Specialist Requests Authorization</h4>
        <?php
        if ($auth_accountant!= ''){
            ?>
            <p>The following specialist is authorized to view your account. Now your tax specialist can help you more by
                having direct access to your account - get help with your questions without leaving the comforts of your
                office and receive proactive tax planning advice by keeping your specialist current on your business
                activity.</p>
            <p>You are in control here - you can always grant and remove access as necessary.</p>
        <?php
        }
        else
        {
    ?>
            <p>The following specialist has requested access to view your account.  Don't miss out on the time saving benefits of working with your tax specialist through the power of the internet - get help with your questions without leaving the comforts of your office and receive proactive tax planning advice by keeping your tax specialist current on your business activity.</p>
            <p>You are in control here - you can always grant and remove access as necessary.</p>
    <?php
        }
    ?>

    <table class="table-padding" border="0">
        <tr>
            <td style="width:50%"><label>Name:</label></td>
            <td><?=$specialist["first_name"]; ?> <?=$specialist["last_name"]; ?></td>
        </tr>
        <tr>
            <td><label>Company:</label></td>
            <td><?=$specialist["company"]; ?></td>
        </tr>
        <tr>
            <td><label>Address:</label></td>
            <td><?=$specialist["addr1"]; ?></td>
        </tr>
        <tr>
            <td><label>City, State, ZIP:</label></td>
            <td><?=$specialist["city"];?>
                <?php
                if ($specialist["state"]) echo ", ".$specialist["state"];
                if ($specialist["zip"]) echo ", ".$specialist["zip"];
                ;?>
            </td>
        </tr>
        <tr>
            <td><label>Phone:</label></td>
            <td><?=$specialist["phone"]; ?></td>
        </tr>
        <tr>
            <td><label>Email:</label></td>
            <td><?=$specialist["email"]; ?></td>
        </tr>
        <tr>

            <td  align="center">
            <?php
                if ($auth_accountant!= ''){
                    echo Html::beginForm(['remove']) . Html::submitButton('Remove Access', [
                        'class' => 'btn btn-success',
                        'data-confirm' => 'Are you sure?'
                    ]) ;
                    echo Html::endForm()."</td><td>";

                    echo Html::beginForm(['unload']) . Html::submitButton('Unload Specialist', [
                            'class' => 'btn btn-success',
                            'data-confirm' => 'Are you sure?'
                        ]) ;

                    echo Html::endForm();
                }
                else {
                    echo Html::beginForm(['grant']) . Html::submitButton('Grant Access', [
                            'class' => 'btn btn-success',
                            'data-confirm' => 'Are you sure?'
                        ]) . Html::endForm();

                }
                ?>
            </td></tr>
    </table>
<?php } else {?>
        <p>No specialist is authorized to access to view your account.  Don't miss out on the time saving benefits of working with your tax specialist through the power of the internet - get help with your questions without leaving the comforts of your office and receive proactive tax planning advice by keeping your tax specialist current on your business activity.</p>
        <p>It's easy to get started - contact your tax specialist directly, let them know about <? echo $COMPANY ?> and direct them to our website at www.sagefire.com.</p>
        <p>When you want to authorize a tax specialist, enter their Reference ID number in the space below.  Your specialist's Reference ID can be found on their <? echo $COMPANY ?>' Accountant Homepage.</p>
<?php }
    if ((($not_auth_accountant!= '') && ($auth_accountant== '')) ||  (($not_auth_accountant== '') && ($auth_accountant== ''))) {
        if (($not_auth_accountant!= '') && ($auth_accountant== '')){
        echo "<p>Or, if you want to authorize a different tax specialist, enter their Reference ID number in the space below. Your specialist's Reference ID can be found on their KeepMore.net Accountant Homepage.</p>";
        }

    ?>

        <?= Html::beginForm(['load']); ?>
        <table class="table-padding">
            <tr>
                <td><label>Specialist <br>Reference ID:</label></td>
                <td><input type="text" name="specialist_code"></td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <button class="btn btn-success">Load Specialist</button>
                </td>
            </tr>
        </table>
        <?php echo Html::endForm();
    }
?>
