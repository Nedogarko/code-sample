<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model \common\models\backend\Order */

?>
<div class="row">
    <div class="col-md-10">

    <?php $form = ActiveForm::begin(['id' => 'paymentCard',
        'options' => [
            'class' => 'form-inline',
        ],

    ]); ?>
        <?= $this->render('@common/views/card.php', [
            'form' => $form,
            'model' => $model
        ]); ?>
    <div class="row text-center">
        <div class="form-group">
            <button class="btn btn-success plus" ><i
                    class="fa fa-plus" aria-hidden="true"> </i> Add Card
            </button>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
    </div>

</div>
