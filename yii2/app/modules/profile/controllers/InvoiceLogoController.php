<?php
namespace app\modules\profile\controllers;

use common\models\frontend\File;
use common\models\frontend\FileData;
use Yii;
use app\modules\profile\components\Controller;
use app\models\UploadForm;
use yii\web\UploadedFile;
use yii\helpers\Url;

/**
 * InvoiceLogoController
 */
class InvoiceLogoController extends Controller
{
    public function actionIndex()
    {
        $model = new UploadForm();
        $logo = new File();
        $logo = $logo->find()->where(['purpose' => FILE::LOGO_PURPOSE])->one();

        if (Yii::$app->request->isPost) {
            $model->imageFile = UploadedFile::getInstance($model, 'imageFile');
            if ($logo) {
                $logo->deleteFile();
            }
            $file = new File();
            $file->datatype = $model->imageFile->type;
            $file->name = $model->imageFile->name;
            $file->size = $model->imageFile->size;
            $file->purpose = FILE::LOGO_PURPOSE;
            $file->filedate = (new \DateTime())->format(Yii::$app->helper->getStorageDateTimeFormat());
            if ($file->save()) {
                $fp = fopen($model->imageFile->tempName, "rb");
                $fileData = new FileData();
                $fileData->masterid = $file->id;
                $fileData->filedata = addslashes(file_get_contents($model->imageFile->tempName));
                $fileData->save();
                fclose($fp);
            }
            $logo = $file;
        }


        return $this->render('index', [
            'model' => $model,
            'logo' => $logo
        ]);
    }

    public function actionRemove()
    {
        $logo = new File();
        $logo = $logo->find()->where(['purpose' => FILE::LOGO_PURPOSE])->one();
        if (!$logo->deleteFile()) {
            Yii::$app->getSession()
                ->setFlash('error', "Can't delete logo!");
        }
        $this->redirect(Url::to('/profile/invoice-logo'));
    }
}
