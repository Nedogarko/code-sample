<?php
namespace app\modules\profile\controllers;

use Yii;
use app\modules\profile\components\Controller;
use common\models\backend\Order;

/**
 * CompanyInfoController
 */
class CompanyInfoController extends Controller
{
    public function actionIndex()
    {
        $order = $this->findOrder();
        $order->setScenario(Order::SCENARIO_COMPANY_UPDATE);
        if ($order->load(Yii::$app->request->post())) {
            $order->save();
        }

        return $this->render('index', [
            'order' => $order,
        ]);
    }

    public function actionUpdate()
    {
        $order = $this->findOrder();
        $order->setScenario(Order::SCENARIO_COMPANY_UPDATE);
        if ($order->load(Yii::$app->request->post()) && $order->save()) {
        } else {
            return $this->renderAjax('_form', [
                'order' => $order,
            ]);
        }
    }
}
